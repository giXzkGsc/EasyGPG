#!/bin/sh

# EasyGPG 4.58
# Copyright (C) 2015 -- 2024 the author indicated below
# The author of EasyGPG made an OpenPGP,
# RSA key pair. The fingerprint of this key pair is
# BA34F30AC917CB0714884A3DA6BDBF5757B731E9
# The three PGP messages below were signed with the secret key
# of this pair. The public key of this pair is included below.
# EasyGPG is distributed under the terms of the GNU General
# Public License, version 3 (https://www.gnu.org/licenses/gpl.html).
# EasyGPG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY--without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

thisFileName="easygpg.sh"
theversion="EasyGPG 4.58"
thisyear="2024"
egpgvf="version.txt"
egpgdf="easygpg.asc"
rdname="ramdisk"

thisFile=`readlink -e "${0}"` # gets the absolute path name of this file
if test "$(ps -fC "${thisFileName}" | grep -c "${thisFile}")" -gt 30
then
  exit 1
fi

if printf "%s\n" "${thisFile}" | grep "^${HOME}" > /dev/null 2> /dev/null
then
  thisrootdir="${HOME}"
else
  thisrootdir=`printf "%s\n" "${thisFile}" | grep -o "^/[^/]*"`
fi
thisdir=`dirname "${thisFile}"`
thisdirbase=`basename "${thisdir}"`

rebuildpath="${thisdir}/Rebuild EasyGPG.desktop"
if ! test -f "${rebuildpath}"
then
  rm -f "${thisdir}/start-easygpg.desktop" # old start desktop file
  rm -f "${thisdir}/LXLE-Set-Up.sh" # old set up script
  rm -f "${thisdir}/LXLE-Rebuild.sh" # old rebuild script
  rm -f "${thisdir}/LXLE-README" # old README
  touch "${rebuildpath}"
  chmod 700 "${rebuildpath}"
  printf "%s\n" "#!/usr/bin/env ./${thisFileName} --su" > "${rebuildpath}"
  printf "\n" >> "${rebuildpath}"
  printf "%s\n" "[Desktop Entry]" >> "${rebuildpath}"
  printf "%s\n" "Type=Application" >> "${rebuildpath}"
  printf "%s\n" "Categories=Utility;Security;TextTools;" >> "${rebuildpath}"
  printf "%s\n" "Name=Rebuild EasyGPG" >> "${rebuildpath}"
  printf "%s\n" "Comment=Rebuild EasyGPG" >> "${rebuildpath}"
  printf "%s\n" "Exec=sh -c '\"\$(dirname \"\$*\")\"/${thisFileName} --su || ([ ! -x \"\$(dirname \"\$*\")\"/${thisFileName} ] && \"\$*\"/${thisFileName} --su)' dummy %k" >> "${rebuildpath}"
  printf "%s\n" "Icon=application-x-executable" >> "${rebuildpath}"
  touch -r "${thisFile}" "${rebuildpath}"
  if which "gio" > /dev/null
  then
    gio info "${rebuildpath}" > /dev/null
    gio set "${rebuildpath}" metadata::caja-trusted-launcher true
  fi
fi
tailstest="/usr/local/bin/tails-version" # if test -x "${tailstest}" then this is Tails.
tailsREADME="${thisdir}/Tails-README"
if test -x "${tailstest}" || test "${thisrootdir}" = "/mnt" || test "${thisrootdir}" = "/media"
then
  printf "If this is Tails, install nemo and mark it for installation every time you start Tails. Nemo will be the second item labeled Files in the Accessories sub-menu of the Applications menu. Select it, open your EasyGPG folder, and double-click the EasyGPG icon." > "${tailsREADME}"
  chmod 644 "${tailsREADME}"
fi

missing=""
if ! which gpg > /dev/null
then
  missing="${missing}\ngpg (gnupg)"
fi
if ! which gpg-agent > /dev/null
then
  missing="${missing}\ngpg-agent (gnupg-agent)"
fi
if ! which zenity > /dev/null
then
  missing="${missing}\nzenity"
fi
if ! which xclip > /dev/null
then
  missing="${missing}\nxclip"
fi
if test -n "${missing}"
then
  errormsg="EasyGPG depends on the following software that is not installed.\n${missing}\n\nPlease install the missing software and then rebuild EasyGPG."
  if which zenity > /dev/null
  then
    zenity --error --width=400 --title "Missing Software" --text="${errormsg}" &
  elif which kdialog > /dev/null
  then
    kdialog --error "${errormsg}" --title "Missing Software" &
  elif which xmessage > /dev/null
  then
    printf "${errormsg}" | xmessage -center -buttons OK -default OK -xrm '*message.scrollVertical: Never' -file - &
  else
    printf "%s\n" "${errormsg}" 1>&2
  fi
  exit 1
fi

gpgvers=`gpg --version | head -n 1`
gpgmvers=`printf "${gpgvers}" | grep -o "[0-9\.]*$" | grep -o "^[^\.]*"`

if test "${gpgmvers}" = "1" && grep " \-\-daemon$" "/etc/X11/Xsession.d/90gpg-agent" > /dev/null 2> /dev/null
then
  if ! pgrep gpg-agent > /dev/null
  then
    gpg-agent --daemon # For gpg 1.x, gpg-agent must run as daemon. It creates $HOME/.gnupg/S.gpg-agent, and it must have GPG_AGENT_INFO set to it.
  fi
  if test -z "${GPG_AGENT_INFO}"
  then
    GPG_AGENT_INFO="${HOME}/.gnupg/S.gpg-agent:0:1"
    export GPG_AGENT_INFO
  fi
fi

gotinet=`ip addr | grep "inet6*[ \t]\+" | grep "scope[ \t]\+global"`

if lsof -ni | grep "^tor .*127.0.0.1:9150 (LISTEN)$" > /dev/null
then
  torproxy="127.0.0.1:9150"
elif lsof -ni | grep "^tor .*127.0.0.1:9050 (LISTEN)$" > /dev/null || ss -ltn4 | grep "127.0.0.1:9050" > /dev/null
then
  torproxy="127.0.0.1:9050"
else
  torproxy=""
fi

ramdiskpath="${thisdir}/${rdname}"
if test -d "${ramdiskpath}" && df --output=fstype "${ramdiskpath}" | grep "^tmpfs" > /dev/null
then
  gotramdisk="yes"
else
  gotramdisk=""
fi
thisisram=`df --output=fstype "${thisdir}" | grep "^tmpfs"`
if test -n "${thisisram}"
then
  rammountpoint=`df --output=target "${thisdir}" | grep "^/"`
  defaultdir="${rammountpoint}"
elif test -n "${gotramdisk}"
then
  defaultdir="${ramdiskpath}"
else
  defaultdir="${thisdir}"
fi

if test -n "${thisisram}"
then
  tempfiledir="${rammountpoint}"
elif test -n "${gotramdisk}"
then
  tempfiledir="${ramdiskpath}"
elif test -n "${XDG_RUNTIME_DIR}" && test -d "${XDG_RUNTIME_DIR}" && df --output=fstype "${XDG_RUNTIME_DIR}" | grep "^tmpfs" > /dev/null
then
  tempfiledir="${XDG_RUNTIME_DIR}"
elif test -d "/run/user/$(id -u)" && df --output=fstype "/run/user/$(id -u)" | grep "^tmpfs" > /dev/null
then
  tempfiledir="/run/user/$(id -u)"
elif test -d "/run/user/$(id -un)" && df --output=fstype "/run/user/$(id -un)" | grep "^tmpfs" > /dev/null
then
  tempfiledir="/run/user/$(id -un)"
elif test -d "/tmp" && df --output=fstype "/tmp" | grep "^tmpfs" > /dev/null
then
  tempfiledir="/tmp"
else
  tempfiledir="${thisdir}"
fi
if test -n "${XDG_DATA_HOME}" && test -d "${XDG_DATA_HOME}"
then
  menudir="${XDG_DATA_HOME}/applications"
else
  menudir="${HOME}/.local/share/applications"
fi

if test -z "${gotramdisk}" && test -f "${ramdiskpath}"
then
  mv "${ramdiskpath}" "${thisdir}/ramdisk-file"
  zenity --info --width=400 --text="The EasyGPG folder contained a file called \"${rdname}.\" This name is reserved for the RAM disk created by EasyGPG. This \"${rdname}\" file has been renamed \"ramdisk-file.\""
fi
if test -z "${gotramdisk}" && test -d "${ramdiskpath}"
then
  if test -n "$(ls -A "${ramdiskpath}")"
  then
    mv "${ramdiskpath}" "${thisdir}/bogus-ramdisk"
    zenity --info --width=400 --text="The EasyGPG folder contained a folder called \"${rdname},\"\nbut it was not really a RAM disk created by EasyGPG.\nYou may not have removed the RAM disk before shutting down your computer.\nThis bogus \"${rdname}\" folder has been renamed \"bogus-ramdisk.\""
  else
    rm -rf "${ramdiskpath}"
    if test "${1}" != "--mr" # Create the RAM disk
    then
      zenity --info --width=400 --text="The EasyGPG folder contained a folder called \"${rdname},\"\nbut it was not really a RAM disk created by EasyGPG.\nYou may not have removed the RAM disk before shutting down your computer.\nThis bogus \"${rdname}\" folder has been removed."
    fi
  fi
fi

oldkeydir="${thisdir}/easysgpgkeyrings" # misspelled name from old versions
keydir="${thisdir}/easygpgkeyrings"
if test -d "${oldkeydir}"
then
  mv -f "${oldkeydir}" "${keydir}"
fi
pubkeybu="${keydir}/egpgpub.gpg"
seckeybu="${keydir}/egpgsec.gpg"
custom="${keydir}/egpgdesk"
egpgfile="${keydir}/egpgfile"
egpgv="${keydir}/egpgv"
iconpath="${keydir}/easygpg.png"
actionsname="EasyGPG-Actions-${thisdirbase}"
actionsdir="${thisdir}/${actionsname}"
dname="EasyGPG-${thisdirbase}"
clearaction="Clear copied text"
putramdiskaction="Drag and drop an Action here to put it on the RAM disk"
putramdiskpath="${actionsdir}/${putramdiskaction}"
putmenuaction="Drag and drop an Action here to put it in the menu"
putmenupath="${actionsdir}/${putmenuaction}"
putondesktopaction="Drag and drop an Action here to put it on the Desktop"
putondesktoppath="${actionsdir}/${putondesktopaction}"
removeaction="Drag and drop a custom Action here to remove it"
removepath="${actionsdir}/${removeaction}"
createramdiskaction="RAM disk create"
createramdiskpath="${thisdir}/${createramdiskaction}"
removeramdiskaction="RAM disk remove"
removeramdiskpath="${thisdir}/${removeramdiskaction}"
backupramdiskaction="RAM disk backup"
backupramdiskpath="${thisdir}/${backupramdiskaction}"
makeegpgaction="Make a new EasyGPG folder on the RAM disk"
makeegpgpath="${thisdir}/${makeegpgaction}"

if (test "${1}" = "--nn" ||  test "${1}" = "--ni" ||  test "${1}" = "--nv" || test "${1}" = "--rm" || test "${1}" = "--cc" || test "${1}" = "--mk" || test "${1}" = "--ck" || test "${1}" = "--sk" || test "${1}" = "--pk" || test "${1}" = "--ip" || test "${1}" = "--is" || test "${1}" = "--ep" || test "${1}" = "--es" || test "${1}" = "--ca" || test "${1}" = "--lk" || test "${1}" = "--dk" || test "${1}" = "--dm" || test "${1}" = "--md" || test "${1}" = "--pm" || test "${1}" = "--fs" || test "${1}" = "--cl" || test "${1}" = "--sh" || test "${1}" = "--rr" || test "${1}" = "--emc" || test "${1}" = "--sfc" || test "${1}" = "--help" || test "${1}" = "--version") && test "$#" -gt 1
then
  printf "No other arguments can be used with the ${1} option.\n" 1>&2
  exit 1
fi

if test "${1}" = "--su" || test "${1}" = "--si" # Rebuild EasyGPG
then
  rm -f "${thisdir}/"*".desktop"
  rm -rf "${thisdir}/EasyGPG-Actions-"*
  rm -f "${keydir}/egpgu" # created by old version
  rm -f "${iconpath}"
  rm -f "${tailsREADME}"
  rm -rf "${thisdir}/bin" # created by old version
  if test -d "${keydir}"
  then
    ("${thisFile}" --nn) | zenity --progress --text="Rebuilding \"${thisdirbase}\"..." --pulsate --auto-close --no-cancel
  else
    "${thisFile}" --nn
  fi
  if test -n "${gotramdisk}" && (test -f "${ramdiskpath}/${thisFileName}" || test -n "$(ls "${ramdiskpath}"/*/"${thisFileName}" 2> /dev/null)")
  then
    if zenity --question --width=400 --title="Rebuild RAM disk EasyGPG?" --text="Also rebuild all copies of EasyGPG on the RAM disk, making sure they are also ${theversion}?" --ok-label="Rebuild" --cancel-label="Cancel"
    then
      if test -f "${ramdiskpath}/${thisFileName}"
      then
        cp -af "${thisFile}" "${ramdiskpath}"
        "${ramdiskpath}/${thisFileName}" --su
      fi
      if test -n "$(ls "${ramdiskpath}"/*/"${thisFileName}" 2> /dev/null)"
      then
        for item in "${ramdiskpath}"/*/"${thisFileName}"
        do
          cp -af "${thisFile}" "$(dirname "${item}")"
          "${item}" --su
        done
      fi
      if zenity --question --width=400 --title="Backup RAM Disk?" --text="The contents of the RAM disk have changed. Backup the RAM disk?" --ok-label="Backup" --cancel-label="Cancel"
      then
        "${thisFile}" --br
      fi
    fi
  fi
  exit 0
fi

if test "${thisrootdir}" = "/mnt" || test "${thisrootdir}" = "/media" || test "${thisrootdir}" = "${HOME}"
then
  if ! test -d "${keydir}" && test -w "${thisdir}"
  then
    rm -f "${thisdir}/"*".desktop"
    mkdir -m 700 "${keydir}" # must be available only to you or won't be used
    if test "${gpgmvers}" != "1"
    then
      mkdir -m 700 "${keydir}/private-keys-v1.d" # must be available only to you or won't be used
    fi
    gpg --homedir "${keydir}" -K > /dev/null 2> /dev/null # initializes files in ${keydir}
    if test "${1}" = "--nn" # assume EasyGPG is running for the first time
    then
      ("${thisFile}" --ni) | zenity --progress --text="Building \"${thisdirbase}\"..." --pulsate --auto-close --no-cancel
      "${thisFile}" --sh
      sleep 2
      zenity --info --width=400 --text="${theversion}\n\nDouble-click \"${dname}\" (just created) to read copied text or open the \"${actionsname}\" folder.\nDrop files and folders onto it to encrypt, decrypt, and to import keys. Read more in the EasyGPG Help.\n\nEverything you can do with EasyGPG is in the \"${actionsname}\" folder. Just double-click on what\nyou want to do. You can also drag and drop files onto some of the Actions.\n\nMost of the things you'll want to do with EasyGPG require that you have a personal key pair.\nYou can make one with \"Make a new personal key pair\" or import one or more from the main GPG keyring\nwith \"Import all the personal key pairs from the main GPG keyring.\" Read more in the EasyGPG Help.\n\n\"Rebuild EasyGPG\" must be in the same folder as \"${thisFileName}\".\n\nIf you move or rename this folder, you will need to double-click \"Rebuild EasyGPG\" to rebuild EasyGPG." &
    fi
  fi
fi
if test -d "${keydir}"
then
  if ! test -O "${keydir}"
  then
    zenity --error --width=400 --text="${keydir} can't be used because you don't own it." &
    exit 1
  fi
else
  zenity --error --width=400 --text="EasyGPG can not be run from ${thisdir} because you can't create and delete files and folders here." &
  exit 1
fi

if ! test -f "${egpgv}"
then
  printf "%s\n" "${gpgvers}" > "${egpgv}"
  chmod 600 "${egpgv}"
  if test "${gpgmvers}" != "1"
  then
    mkdir -m 700 "${keydir}/private-keys-v1.d" # must be available only to you or won't be used
  fi
  gpg --homedir "${keydir}" -K > /dev/null 2> /dev/null # initializes files in ${keydir}
elif test "$(cat "${egpgv}")" != "${gpgvers}"
then # if version of gpg changes, restore keys from backups
  printf "%s\n" "${gpgvers}" > "${egpgv}"
  chmod 600 "${egpgv}"
  for item in "${keydir}/"*
  do
    if test "${item}" != "${egpgfile}" && test "${item}" != "${egpgv}" && test "${item}" != "${custom}" && test "${item}" != "${pubkeybu}" && test "${item}" != "${seckeybu}" && test "${item}" != "${iconpath}"
    then
      rm -rf "${item}"
    fi
  done
  if test "${gpgmvers}" != "1"
  then
    mkdir -m 700 "${keydir}/private-keys-v1.d" # must be available only to you or won't be used
  fi
  gpg --homedir "${keydir}" -K > /dev/null 2> /dev/null # initializes files in ${keydir}
  "${thisFile}" --rk
fi
if ! test -f "${iconpath}"
then
  messageStart=`cat "${thisFile}" | grep -n "^\-\-\-\-\-BEGIN PGP MESSAGE\-\-\-\-\-$" | grep -o "^[^:]*" | tail -n +3 | head -n 1`
  tail -n +${messageStart} "${thisFile}" | gpg --homedir "${keydir}" -d - 2> /dev/null > "${iconpath}"
  chmod 600 "${iconpath}"
fi
if ! test -d "${custom}"
then
  mkdir -m 700 "${custom}"
fi
if ! test -f "${egpgfile}"
then
  printf "%s\n" "EasyGPG-$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10)" > "${egpgfile}"
  chmod 600 "${egpgfile}"
fi
if ! test -d "${actionsdir}"
then
  mkdir -m 700 "${actionsdir}"
  "${thisFile}" --wd "${actionsdir}/01-Encrypt a message and copy it" "--em %f"
  "${thisFile}" --wd "${actionsdir}/02-Encrypt a message with deniable authentication and copy it" "--da %f"
  "${thisFile}" --wd "${actionsdir}/03-Sign a message and copy it" "--sm %f"
  "${thisFile}" --wd "${actionsdir}/04-Read copied text" "--rm %f"
  "${thisFile}" --wd "${actionsdir}/05-Encrypt a message and save it" "--ex %f"
  "${thisFile}" --wd "${actionsdir}/06-Sign a message and save it" "--ms %f"
  "${thisFile}" --wd "${actionsdir}/07-Encrypt a message to me and save it" "--wf %f"
  "${thisFile}" --wd "${actionsdir}/08-Save a file or folder as a signed, encrypted tar archive" "--sx %f"
  "${thisFile}" --wd "${actionsdir}/09-Save a file or folder as an unsigned, encrypted tar archive" "--uf %f"
  "${thisFile}" --wd "${actionsdir}/10-Save a file or folder as a signed tar archive" "--xs %f"
  "${thisFile}" --wd "${actionsdir}/11-Save a file or folder as a tar archive encrypted for me" "--ef %f"
  "${thisFile}" --wd "${actionsdir}/12-Read files" "--rf %F"
  "${thisFile}" --wd "${actionsdir}/13-Read a file from the Internet" "--ru"
  "${thisFile}" --wd "${actionsdir}/14-Make a new personal key pair" "--mk"
  "${thisFile}" --wd "${actionsdir}/15-List and copy keys" "--lk"
  "${thisFile}" --wd "${actionsdir}/16-Copy all the public keys" "--ca"
  "${thisFile}" --wd "${actionsdir}/17-Copy my public key" "--ck"
  "${thisFile}" --wd "${actionsdir}/18-Save my public key as a text file" "--sk"
  "${thisFile}" --wd "${actionsdir}/19-Import all the public keys from the main GPG keyring" "--ip"
  "${thisFile}" --wd "${actionsdir}/20-Import all the personal key pairs from the main GPG keyring" "--is"
  "${thisFile}" --wd "${actionsdir}/21-Export all the public keys to the main GPG keyring" "--ep"
  "${thisFile}" --wd "${actionsdir}/22-Export all the personal key pairs to the main GPG keyring" "--es"
  "${thisFile}" --wd "${actionsdir}/23-Delete a key" "--dk"
  "${thisFile}" --wd "${actionsdir}/24-Put EasyGPG on the Desktop" "--md"
  "${thisFile}" --wd "${actionsdir}/25-Put EasyGPG in the Applications menu" "--pm"
  "${thisFile}" --wd "${actionsdir}/26-Remove EasyGPG from the Applications menu" "--dm"
  "${thisFile}" --wd "${actionsdir}/27-Check for a new version of EasyGPG" "--nv"
  "${thisFile}" --wd "${actionsdir}/28-EasyGPG Help" "--sh"
  "${thisFile}" --wd "${actionsdir}/29-Full source code" "--fs"
  "${thisFile}" --wd "${actionsdir}/30-About EasyGPG" "--cl"
  "${thisFile}" --wd "${actionsdir}/31-Create a custom encrypt and copy message Action" "--emc"
  "${thisFile}" --wd "${actionsdir}/32-Create a custom encrypt file or folder Action" "--sfc"
  "${thisFile}" --wd "${actionsdir}/${clearaction}" "--cc"
  if test "$(dirname "${thisdir}")" = "${rammountpoint}"
  then
    "${thisFile}" --wd "${putramdiskpath}" "--cr %f"
  fi
  "${thisFile}" --wd "${removepath}" "--ra %f"
  "${thisFile}" --wd "${putmenupath}" "--am %f"
  "${thisFile}" --wd "${putondesktoppath}" "--cd %f"
fi
dfilename="$(cat "${egpgfile}").desktop"
dfilepath="${thisdir}/${dfilename}"
if ! test -f "${dfilepath}"
then
  touch "${dfilepath}" # to avoid fork bomb
  "${thisFile}" --wd "${thisdir}/$(cat "${egpgfile}")" "%F" "${dname}"
  if test "$(dirname "${thisdir}")" = "${rammountpoint}"
  then
    cp -af "${dfilepath}" "${rammountpoint}"
    if which "gio" > /dev/null
    then
      gio info "${rammountpoint}/${dfilename}" > /dev/null
      gio set "${rammountpoint}/${dfilename}" metadata::caja-trusted-launcher true
    fi
  fi
  if test -f "${HOME}/Desktop/${dname}.desktop"
  then
    rm -f "${HOME}/Desktop/${dname}.desktop"
    cp -af "${dfilepath}" "${HOME}/Desktop"
  fi
  if test -f "${menudir}/${dname}.desktop"
  then
    rm -f "${menudir}/${dname}.desktop"
    "${thisFile}" --mf "${dfilepath}"
  fi
fi
if test -z "${thisisram}"
then
  if ! test -f "${createramdiskpath}.desktop"
  then
    touch "${createramdiskpath}.desktop" # to avoid fork bomb
    "${thisFile}" --wd "${createramdiskpath}" "--mr %f"
  fi
  if ! test -f "${removeramdiskpath}.desktop"
  then
    touch "${removeramdiskpath}.desktop" # to avoid fork bomb
    "${thisFile}" --wd "${removeramdiskpath}" "--rr"
  fi
  if ! test -f "${backupramdiskpath}.desktop"
  then
    touch "${backupramdiskpath}.desktop" # to avoid fork bomb
    "${thisFile}" --wd "${backupramdiskpath}" "--br %f"
  fi
  if ! test -f "${makeegpgpath}.desktop"
  then
    touch "${makeegpgpath}.desktop" # to avoid fork bomb
    "${thisFile}" --wd "${makeegpgpath}" "--ce %f"
  fi
fi
if test -n "$(ls -A "${custom}/"* 2> /dev/null)"
then
  for item in "${custom}/"*
  do
    if test -f "${item}" && file -b -i "${item}" | grep -v "charset=binary$" > /dev/null
    then
      itembase=`basename "${item}"`
      if ! test -f "${actionsdir}/${itembase}.desktop"
      then
        cname=`head -n 1 "${item}"`
        arguments=`tail -n +2 "${item}" | head -n 1`
        touch "${actionsdir}/${itembase}.desktop" # to avoid fork bomb
        "${thisFile}" --wd "${actionsdir}/${itembase}" "${arguments}" "${cname}-${thisdirbase}"
      fi
    fi
  done
fi
if test "${1}" = "--nn" || test "${1}" = "--ni"
then
  exit 0
fi


if test "${1}" = "--nv" # Check for a new version of EasyGPG
then
  if ! test -w "${thisdir}"
  then
    zenity --info --width=400 --text="You can't update this copy of EasyGPG because you can't create and delete files in ${thisdir}." &
    exit 0
  fi
  torurl="gopher://7hinc6ucgvwbcjjoe44lhzzxyjptb3da6tzl33oe7ezl2qgwlrkfe6yd.onion:1990/9"
  i2purl="http://bwxry5alzx5ihgrd3glah4eotddblzhalvpheppnw4zcajzqoora.b32.i2p"
  archiveurl="https://archive.org/download/easygpg"
  codebergurl="https://codeberg.org/giXzkGsc/EasyGPG/raw/branch/main"
  cd "${keydir}"
  downloadcommand=""
  if test -z "${gotinet}"
  then # do not try Tor or I2P if no network connection--will take too long to fail
    zenity --info --width=400 --text="There is no Internet connection." &
    exit 0
  else
    if test -n "${torproxy}"
    then
      downloadcommand="curl -s -L -O --socks5-hostname ${torproxy}"
      if ! which "curl" > /dev/null
      then
        if zenity --question --width=400 --title="Proceed without Tor?" --text="Tor is running, but it can not be used to update because curl is not installed. Proceed without using Tor?" --ok-label="Proceed without Tor" --cancel-label="Cancel"
        then
          downloadcommand=""
        else
          exit 0
        fi
      fi
    fi
    if test -n "${downloadcommand}" # use Tor
    then # use Tor
      (${downloadcommand} ${torurl}/${egpgvf}) | zenity --progress --text="checking for new version of EasyGPG on the EasyGPG Tor Onion Minus server..." --pulsate --auto-close --no-cancel
      if test -f "${egpgvf}"
      then
        thesource="onion"
      else
        if ! zenity --question --width=400 --title="Try codeberg.org?" --text="Couldn't read the latest version number from the EasyGPG Tor Onion Minus server.\nTry codeberg.org instead?" --ok-label="Try codeberg.org" --cancel-label="Cancel"
        then
          exit 0
        fi
        via=" via Tor..."
      fi
    elif lsof -ni | grep "TCP 127.0.0.1:4444 (LISTEN)" > /dev/null # use I2P
    then # use I2P
      downloadcommand="wget -q -T 20 -t 4 -e use_proxy=yes -e http_proxy=127.0.0.1:4444"
      (${downloadcommand}  ${i2purl}/${egpgvf}) | zenity --progress --text="checking for new version of EasyGPG on I2P..." --pulsate --auto-close --no-cancel
      if test -f "${egpgvf}"
      then
        thesource="I2P"
      else
        if ! zenity --question --width=400 --title="Try codeberg.org?" --text="Couldn't read the latest version number from I2P.\nTry codeberg.org instead?" --ok-label="Try codeberg.org" --cancel-label="Cancel"
        then
          exit 0
        fi
        downloadcommand=""
      fi
    fi
  fi
  if test -f "${egpgvf}" && test "$(grep "^EasyGPG [0-9\.]\+$" "${egpgvf}")" != "$(cat "${egpgvf}")"
  then
    rm "${egpgvf}"
  fi
  if ! test -f "${egpgvf}" # use clearnet
  then
    if test -z "${downloadcommand}"
    then # use clearnet
      downloadcommand="wget -q -T 20 -t 4"
      via="..."
    fi
    (${downloadcommand} ${codebergurl}/${egpgvf}) | zenity --progress --text="checking for new version of EasyGPG on codeberg.org${via}" --pulsate --auto-close --no-cancel
    if test -f "${egpgvf}"
    then
      thesource="codeberg"
    else
      (${downloadcommand} ${archiveurl}/${egpgvf}) | zenity --progress --text="couldn't read the latest version from codeberg.org;\nchecking for new version of EasyGPG on archive.org${via}" --pulsate --auto-close --no-cancel
      if test -f "${egpgvf}"
      then
        thesource="archive"
      fi
    fi
  fi
  if ! test -f "${egpgvf}"
  then
    zenity --info --width=400 --text="Couldn't read the latest version number" &
  else
    networkversion=`cat "${egpgvf}" 2> /dev/null`
    rm -f "${egpgvf}"
    if test "${theversion}" = "${networkversion}"
    then
      zenity --info --width=400 --text="This copy of EasyGPG (${networkversion}) is up to date." &
    else
      if zenity --question --width=400 --title="Update to ${networkversion}?" --text="Your present version is ${theversion},\nand the new version is ${networkversion}.\nUpdate to ${networkversion}?" --ok-label="Update" --cancel-label="Cancel"
      then
        if test "${thesource}" = "onion"
        then
          (${downloadcommand} ${torurl}/${egpgdf}) | zenity --progress --text="downloading ${networkversion} from the EasyGPG Tor Onion Minus server..." --pulsate --auto-close --no-cancel
        elif test "${thesource}" = "I2P"
        then
          (${downloadcommand} ${i2purl}/${egpgdf}) | zenity --progress --text="downloading ${networkversion} from I2P..." --pulsate --auto-close --no-cancel
        elif test "${thesource}" = "archive"
        then
          (${downloadcommand} ${archiveurl}/${egpgdf}) | zenity --progress --text="downloading ${networkversion} from archive.org${via}" --pulsate --auto-close --no-cancel
        elif test "${thesource}" = "codeberg"
        then
          (${downloadcommand} ${codebergurl}/${egpgdf}) | zenity --progress --text="downloading ${networkversion} from codeberg.org${via}" --pulsate --auto-close --no-cancel
        fi
        if test -f "${egpgdf}" && test "$(tail -n 1 "${egpgdf}")" != "-----END PGP MESSAGE-----" # have to get whole file
        then
          rm "${egpgdf}"
        fi
        if ! test -f "${egpgdf}"
        then
          zenity --info --width=400 --text="Download of ${networkversion} failed." &
          exit 0
        fi
        tempdir=".egpgsk" # ensure that the signing key is in the keyring
        mkdir -m 700 "${tempdir}" # must be available only to you or won't be used
        gpg --homedir "${tempdir}" -k > /dev/null 2> /dev/null # initializes keyrings
        keyStart=`cat "${thisFile}" | grep -n "^\-\-\-\-\-BEGIN PGP PUBLIC KEY BLOCK\-\-\-\-\-$" | grep -o "^[^:]*" | tail -n +1 | head -n 1`
        signingKey=`tail -n +${keyStart} "${thisFile}"`
        keyLength=`printf "%s\n" "${signingKey}" | grep -n "^\-\-\-\-\-END PGP PUBLIC KEY BLOCK\-\-\-\-\-$" | grep -o "^[^:]*" | head -n 1`
        printf "%s\n" "${signingKey}" | head -n ${keyLength} | gpg --homedir "${tempdir}" --import - 2> /dev/null
        temp="${tempfiledir}/.$(cat /dev/urandom | tr -dc "0-9a-z" | head -c 10)"
        gpg --homedir "${tempdir}" --verify --status-file "${temp}" "${egpgdf}" 2> /dev/null
        rm -rf "${tempdir}"
        if cat "${temp}" | grep "^\[GNUPG:\][\t ]*VALIDSIG" | grep "BA34F30AC917CB0714884A3DA6BDBF5757B731E9$" > /dev/null
        then
          gpg --homedir "${keydir}" -d "${egpgdf}" 2> /dev/null | tar -x
          oldname="${thisdir}/"`cat /dev/urandom | tr -dc "a-zA-Z0-9" | head -c 16`
          mv "${thisFile}" "${oldname}"
          mv "${thisFileName}" "${thisdir}"
          zenity --info --width=400 --text="Signature of download verfied.\nSuccessfully updated to ${networkversion}" &
          rm  "${oldname}"
          rm  "${egpgdf}"
          rm -f  "${temp}"
          "${thisFile}" --su
          exit 0
        else
          zenity --info --width=400 --text="Verification of the signature on the download failed." &
        fi
        rm  "${egpgdf}"
        rm -f  "${temp}"
      fi
    fi
  fi
  exit 0
fi

if test "$#" -gt 1
then
  if test "${1}" = "--em" || test "${1}" = "--eo" || test "${1}" = "--eg" || test "${1}" = "--et" || test "${1}" = "--ex" || test "${1}" = "--wf" || test "${1}" = "--da" || test "${1}" = "--sm" || test "${1}" = "--ms" || printf "%s\n" "${1}" | grep "^\-\-emc[a-zA-Z0-9]\{10\}$" > /dev/null
  then
    if test -f "${2}" && file -b -i "${2}" | grep -v "charset=binary$" > /dev/null
    then
      cat "${2}" | "${thisFile}" "${1}" &
      exit 0
    elif test -n "${2}"
    then
      printf "Only a text file can be used as an argument with the %s option.\n" "${1}" 1>&2
      exit 1
    fi
  fi
fi

if test $# -eq 0
then # no arguments at all
  if test -n "$(xclip -o -selection clipboard 2> /dev/null)"
  then
    if zenity --question --width=400 --title="Read Copied Text?" --text="Text has been copied. Do you want to read it or open the EasyGPG-Actions folder?" --ok-label="Read the Copied Text" --cancel-label="Open the EasyGPG-Actions Folder"
    then
      "${thisFile}" "--rm" &
      exit 0
    fi
  fi
  if which "caja" > /dev/null
  then
    caja "${actionsdir}" &
  elif which "nemo" > /dev/null
  then
    nemo "${actionsdir}" &
  elif which "pcmanfm" > /dev/null
  then
    pcmanfm "${actionsdir}" &
  elif which "thunar" > /dev/null
  then
    thunar "${actionsdir}" &
  elif which "dolphin" > /dev/null
  then
    dolphin "${actionsdir}" &
  else
    xdg-open "${actionsdir}" &
  fi
  exit 0
fi
# at least one argument
if test "${1}" = "--fs" # Full source code
then
  zenity --text-info --filename="${thisFile}" --font="monospace" --ok-label="Close" --cancel-label="Close" --title="Source code" --width=1000 --height=620 &
# ==================
elif test "${1}" = "--version" # Command-line version information
then
  printf "${theversion}\nCopyright (C) 2015 - ${thisyear} the author indicated below\nThe author of EasyGPG made an OpenPGP, RSA key pair.\nThe fingerprint of this key pair is\nBA34F30AC917CB0714884A3DA6BDBF5757B731E9\n${thisFileName} contains the public key of this key pair,\nas well as 3 PGP messages signed with it.\n\nEasyGPG is distributed under the terms of the GNU General\nPublic License, version 3 (https://www.gnu.org/licenses/gpl.html).\nEasyGPG is distributed in the hope that it will be useful,\nbut WITHOUT ANY WARRANTY--without even the implied warranty of\nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\nGNU General Public License for more details.\n"
# ==================
elif test "${1}" = "--help" # Command-line help
then
  printf "usage: ${thisFileName} [option]\n       ${thisFileName} --sf [<pathname to encrypt>]\n       ${thisFileName} --st [<pathname to encrypt>]\n       ${thisFileName} --uf [<pathname to encrypt>]\n       ${thisFileName} --ef [<pathname to encrypt>] [<output file pathname>]\n       ${thisFileName} --sg [<pathname to sign>] [<output file pathname>]\n       ${thisFileName} --ss [<pathname to sign>] [<output file pathname>]\n       ${thisFileName} --rf [<pathname to read>] [<pathname to read> ...]\n       ${thisFileName} --ru [<URL to read>]\n       ${thisFileName} --mr [<pathname to copy into RAM disk>]\n       ${thisFileName} --br [<pathname of RAM disk backup>]\n       ${thisFileName} --ce [<pathname to clone>]\n"
  if test "$(dirname "${thisdir}")" = "${rammountpoint}"
  then
    printf "       ${thisFileName} --cr <pathname to put on RAM disk>\n"
  fi
  printf "       ${thisFileName} --am <pathname to put in menu>\n       ${thisFileName} --cd <pathname to copy to Desktop>\n       ${thisFileName} --ra <pathname to remove>\n       ${thisFileName} <pathname to read> [<pathname to read> ...]\nWith no option, all arguments are pathnames of files to read.\nWith no arguments, the EasyGPG-Actions folder will be opened or copied text will be read.\n\noptions:\n--em       Encrypt a message and copy it\n--eo       Encrypt a message and print it to stdout\n--eg       Encrypt a message and save it\n--et       Encrypt a message and save it as a text file\n--wf       Encrypt a message to me and save it as a text file\n--da       Encrypt a message with deniable authentication and copy it\n--sm       Sign a message and copy it\n--ms       Sign a message and save it as a text file\n--rm       Read copied text\n--cc       ${clearaction}\n--rs       Read text from stdin\n--ru       Read a file from the Internet\n--rf       Read files\n--sf       Save a file or folder as a signed, encrypted tar archive\n--st       Save a file or folder as a signed, encrypted tar archive in text form\n--uf       Save a file or folder as an unsigned, encrypted tar archive\n--ef       Save a file or folder as a tar archive encrypted for me\n--sg       Save a file or folder as a signed tar archive\n--ss       Save a file or folder as a signed tar archive in text form\n--mk       Make a new personal key pair\n--lk       List and copy keys\n--ca       Copy all the public keys\n--ck       Copy my public key\n--sk       Save my public key as a text file\n--pk       Print my public key to stdout\n--ip       Import all the public keys from the main GPG keyring\n--is       Import all the personal key pairs from the main GPG keyring\n--ep       Export all the public keys to the main GPG keyring\n--es       Export all the personal key pairs to the main GPG keyring\n--dk       Delete a key\n--md       Put EasyGPG on the Desktop\n--pm       Put EasyGPG in the Applications menu\n--dm       Remove EasyGPG from the Applications menu\n--nv       Check for a new version of EasyGPG\n--mr       ${createramdiskaction}\n--rr       ${removeramdiskaction}\n--br       ${backupramdiskaction}\n--ce       ${makeegpgaction}\n--emc      Create a custom encrypt and copy message Action\n--sfc      Create a custom encrypt file or folder Action"
  if test "$(dirname "${thisdir}")" = "${rammountpoint}"
  then
    printf "\n--cr       Put an Action on the RAM disk"
  fi
  printf "\n--am       Put an Action in the Applications menu\n--cd       Put an Action on the Desktop\n--ra       Remove a custom Action\n--su       Rebuild EasyGPG\n--fs       Full source code\n--cl       About EasyGPG\n--sh       GUI help\n--version  Version information\n--help     This help"
  if test -n "$(ls -A "${custom}/"* 2> /dev/null)"
  then
    first=""
    for item in "${custom}/"*
    do
      if test -f "${item}" && file -b -i "${item}" | grep -v "charset=binary$" > /dev/null
      then
        arguments=`tail -n +2 "${item}" | head -n 1`
        if test "${arguments}" = "--$(basename "${item}")"
        then
          if test -z "${first}"
          then
            printf "\n"
            first="${arguments}"
          fi
          printf "\n%s  %s" "${arguments}" "$(head -n 1 "${item}")"
        fi
      fi
    done
  fi
  printf "\n\nTo add the contents of a text file to a message, put the path name\nof the text file after any of the first 8 options above.\n"
# ==================
elif test "${1}" = "--cl" # About EasyGPG
then
  messageStart=`cat "${thisFile}" | grep -n "^\-\-\-\-\-BEGIN PGP MESSAGE\-\-\-\-\-$" | grep -o "^[^:]*" | tail -n +1 | head -n 1`
  signedText=`tail -n +${messageStart} "${thisFile}"`
  messageLength=`printf "%s\n" "${signedText}" | grep -n "^\-\-\-\-\-END PGP MESSAGE\-\-\-\-\-$" | grep -o "^[^:]*" | head -n 1`
  signedText=`printf "%s\n" "${signedText}" | head -n ${messageLength}`
  printf "%s\n" "${signedText}" | gpg --homedir "${keydir}" -d - 2> /dev/null | sed "s/EASYGPGVERSION/${theversion}/" | sed "s/COPRYEARS/2015 \&ndash; ${thisyear}/" | sed "s/GPGVERS/${gpgvers}/" | sed "s/GPGAGENTVERS/$(gpg-agent --version | head -n 1)/" | sed "s/ZENITYVERS/$(zenity --version)/" | sed "s/XCLIPVERS/$(xclip -version 2>&1 | head -n 1)/" | sed "s/xclip version/xclip/" | zenity --text-info --filename=/dev/stdin --html --ok-label="Close" --cancel-label="Close" --title="About EasyGPG" --width=677 --height=620 2> /dev/null &
# ==================
elif test "${1}" = "--sh" # Show GUI help
then
  messageStart=`cat "${thisFile}" | grep -n "^\-\-\-\-\-BEGIN PGP MESSAGE\-\-\-\-\-$" | grep -o "^[^:]*" | tail -n +2 | head -n 1`
  signedText=`tail -n +${messageStart} "${thisFile}"`
  messageLength=`printf "%s\n" "${signedText}" | grep -n "^\-\-\-\-\-END PGP MESSAGE\-\-\-\-\-$" | grep -o "^[^:]*" | head -n 1`
  signedText=`printf "%s\n" "${signedText}" | head -n ${messageLength}`
  printf "%s\n" "${signedText}" | gpg --homedir "${keydir}" -d - 2> /dev/null | zenity --text-info --filename=/dev/stdin --html --ok-label="Close" --cancel-label="Close" --title="EasyGPG Help" --width=620 --height=620 2> /dev/null &
# ==================
elif test "${1}" = "--mr" # Create the RAM disk
then
  if test -e "${ramdiskpath}"
  then
    if test -n "${gotramdisk}"
    then
      zenity --info --width=400 --text="The RAM disk already exists." &
      exit 0
    fi
  fi
  if test -n "${thisisram}"
  then
    zenity --info --width=400 --text="You can not create a RAM disk inside a RAM disk." &
    exit 0
  fi
  if ! zenity --question --width=400 --ok-label="Create" --cancel-label="Cancel" --text="Creating a RAM disk requires root (superuser) privilege. Click Create, and then type your admin. password."
  then
    exit 0
  fi
  userid="$(id -u)"
  groupid="$(id -g)"
  mkdir -p -m 700 "${ramdiskpath}"
  pkexec env DISPLAY="${DISPLAY}" XAUTHORITY="${XAUTHORITY}" mount -t tmpfs -o size=$(expr $(free -b | grep "^Mem" | grep -o "[0-9]\+$") / 10) -o mode=700 -o uid="${userid}" -o gid="${groupid}" tmpfs "${ramdiskpath}"
  if test -d "${ramdiskpath}" && ! df --output=fstype "${ramdiskpath}" | grep "^tmpfs" > /dev/null
  then
    rm -rf "${ramdiskpath}"
  fi
  if test -d "${ramdiskpath}"
  then
    filename="${2}"
    if test -n "${filename}"
    then
      filename=`readlink -e "${filename}"`
      if test -z "${filename}"
      then
        zenity --info --width=400 --text="\"${2}\" is not a file or folder. Nothing was copied to the new RAM disk.\nIt has $(df -H --output=avail "${ramdiskpath}" | tail -n 1 | grep -o "[0-9A-Za-z]*") of free space." &
        exit 0
      fi
    fi
    if test -z "${filename}" && test "$(ls "${thisdir}"/rdbackup-*.gpg | wc -l)" = "1"
    then
      filename=`ls "${thisdir}"/rdbackup-*.gpg`
      if ! zenity --question --width=400 --title="Restore RAM Disk from Backup?" --text="Do want to restore the RAM disk from $(basename "${filename}")?" --ok-label="Restore It" --cancel-label="Do Not Restore"
      then
        filename=""
      fi
    fi
    if test -n "${filename}"
    then
      cd "${ramdiskpath}"
      cp -a "${filename}" .
      filenamebase="$(basename "${filename}")"
      (gpg --homedir "${keydir}" --use-agent --no-tty "${filenamebase}" > /dev/null 2> /dev/null) | zenity --progress --text="Decrypting..." --pulsate --auto-close --no-cancel
      if test "$(ls -A)" != "${filenamebase}"
      then
        rm "${filenamebase}"
      fi
      decryptedFile="$(basename -s .gpg "${filenamebase}")"
      if test "$(ls -A)" = "${decryptedFile}" && test "$(file -b -i ${decryptedFile} | grep -o "^[^;]*" | grep -o "[^/]*$" | sed "s/x-//")" = "tar"
      then
        tar -xf "${decryptedFile}" 2> /dev/null
        rm "${decryptedFile}"
      fi
      if test "$(ls -A)" != "${filenamebase}"
      then
        zenity --info --width=400 --text="The contents of \"${filenamebase}\" are now in the new RAM disk.\nThe RAM disk has $(df -H --output=avail "${ramdiskpath}" | tail -n 1 | grep -o "[0-9A-Za-z]*") of free space." &
      else
        zenity --info --width=400 --text="\"${filenamebase}\" is now in the new RAM disk.\nThe RAM disk has $(df -H --output=avail "${ramdiskpath}" | tail -n 1 | grep -o "[0-9A-Za-z]*") of free space." &
      fi
    elif zenity --question --width=400 --title="Make the RAM disk an EasyGPG folder?" --text="Do want to turn the RAM disk into another EasyGPG folder where the keyrings are never saved unencrypted to a physical medium, or do you want to leave it empty?" --ok-label="Make it an EasyGPG Folder" --cancel-label="Leave It Empty"
    then
      cp -a "${thisFile}" "${ramdiskpath}"
      ("${ramdiskpath}/${thisFileName}" --ni) | zenity --progress --text="Building \"$(basename "${ramdiskpath}")\"..." --pulsate --auto-close --no-cancel
      zenity --info --width=400 --text="The new RAM disk is an EasyGPG folder.\nIt has $(df -H --output=avail "${ramdiskpath}" | tail -n 1 | grep -o "[0-9A-Za-z]*") of free space." &
    else
      zenity --info --width=400 --text="The new RAM disk has $(df -H --output=avail "${ramdiskpath}" | tail -n 1 | grep -o "[0-9A-Za-z]*") of free space." &
    fi
  else
    zenity --info --width=400 --text="No RAM disk was created." &
  fi
# ==================
elif test "${1}" = "--rr" # Remove RAM disk
then
  if test -z "${gotramdisk}"
  then
    zenity --info --width=400 --text="There is no RAM disk." &
    exit 0
  fi
  if test -n "$(ls -A "${ramdiskpath}")"
  then
    if zenity --question --width=400 --title="Backup RAM Disk?" --icon-name="dialog-warning" --text="The RAM disk is not empty. Back it up before removing it?" --ok-label="Backup" --cancel-label="Do not backup"
    then
      "${thisFile}" --br
    fi
  fi
  if ! zenity --question --width=400 --title="Remove the RAM disk?" --text="To remove the RAM disk, click \"Remove\" and then type your admin. password." --ok-label="Remove" --cancel-label="Cancel"
  then
    exit 0
  fi
  pkexec env DISPLAY="${DISPLAY}" XAUTHORITY="${XAUTHORITY}" umount "${ramdiskpath}"
  if test -d "${ramdiskpath}" && ! df --output=fstype "${ramdiskpath}" | grep "^tmpfs" > /dev/null
  then
    rm -rf "${ramdiskpath}"
  fi
  if test -d "${ramdiskpath}"
  then
    zenity --info --width=400 --text="The RAM disk was not removed.\nYou canceled the operation or something is keeping\nthe RAM disk busy (such as a terminal open to it, or an open file)." &
  fi
# ==================
elif test "${1}" = "--br" # Backup the RAM disk
then
  if test -z "${gotramdisk}"
  then
    zenity --info --width=400 --text="There is no RAM disk." &
    exit 0
  fi
  if test -z "$(ls -A "${ramdiskpath}")"
  then
    zenity --info --width=400 --text="The RAM disk is empty." &
    exit 0
  fi
  savepath=""
  if test -n "${2}"
  then
    filename=`readlink -e "${2}"`
    if test -n "${filename}" && test -f "${filename}" && test -w "${filename}" && printf "%s\n" "$(basename "${filename}")" | grep "^rdbackup-.\+\.gpg$" > /dev/null
    then
      savepath="${filename}"
    else
      zenity --info --width=400 --text="The file name of a RAM disk backup file must begin with \"rdbackup-\" and end with \".gpg\"" &
      exit 0
    fi
  fi
  if test -z "${savepath}" && test "$(ls "${thisdir}"/rdbackup-*.gpg | wc -l)" = "1"
  then
    savepath=`ls "${thisdir}"/rdbackup-*.gpg`
    if ! zenity --question --width=400 --title="Back Up to Existing Backup?" --text="Do want to back up the RAM disk to $(basename "${savepath}") or to a new backup file?" --ok-label="Existing Backup" --cancel-label="New Backup"
    then
      savepath=""
    fi
  fi
  if test -z "${savepath}"
  then
    savepath="${thisdir}/rdbackup-$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10).gpg"
  fi
  cd "${ramdiskpath}"
  if test -z "$("${thisFile}" --ps | tr -d "\n")"
  then
    if zenity --question --width=400 --title="Encrypt with a Passphrase?" --text="You have no personal key pair. Do you want to encrypt the backup with a passphrase, instead?" --ok-label="Use a Passphrase" --cancel-label="Cancel"
    then
      (tar --numeric-owner -c * .[^.]* 2> /dev/null | gpg --homedir "${keydir}" --trust-model always -c --no-emit-version --no-encrypt-to --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty --yes -o "${savepath}" -) | zenity --progress --text="Encrypting..." --pulsate --auto-close --no-cancel
    fi
  else
    senderID=`"${thisFile}" --ps | sed -n "p;n" | head -n 1`
    recipients=`"${thisFile}" --ps | sed -n "p;N;" | sed "s/^/-R /" | tr "\n" " " | sed "s/ $//"`
    (tar --numeric-owner -c * .[^.]* 2> /dev/null | gpg --homedir "${keydir}" --trust-model always -s -u "${senderID}" -e ${recipients} --no-emit-version --no-encrypt-to --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty --yes -o "${savepath}" -) | zenity --progress --text="Encrypting..." --pulsate --auto-close --no-cancel
  fi
  chmod 600 "${savepath}"
# ==================
elif test "${1}" = "--ce" # Create a new EasyGPG folder in your RAM disk; ${2}, if present, is folder to clone
then
  if test -z "${gotramdisk}"
  then
    zenity --info --width=400 --text="There is no RAM disk." &
    exit 0
  fi
  if test -n "${2}"
  then
    clonekeyrings="$(readlink -e "${2}")"
  elif ! zenity --question --width=400 --title="Make new or clone?" --text="Make a new EasyGPG folder with no keys or Custom Actions or clone an existing EasyGPG folder, copying its keyrings and Custom Actions?" --ok-label="New" --cancel-label="Clone"
  then
    clonekeyrings=`zenity --file-selection --directory --filename="${HOME}/" --title "Select an EasyGPG folder to clone"`
    if test -z "${clonekeyrings}"
    then
      exit 0
    fi
  else
    clonekeyrings=""
  fi
  if test -n "${clonekeyrings}" && (! test -d "${clonekeyrings}" || ! test -x "${clonekeyrings}/${thisFileName}" || ! test -d "${clonekeyrings}/$(basename "${keydir}")")
  then
    zenity --info --width=400 --text="\"$(basename "${clonekeyrings}")\" is not an EasyGPG folder." &
    exit 0
  fi
  if test -n "${clonekeyrings}"
  then
    newfolderpath="${ramdiskpath}/$(basename "${clonekeyrings}")"
    if test -e "${newfolderpath}"
    then
      zenity --info --width=400 --text="\"$(basename "${newfolderpath}")\" is already on the RAM disk." &
      exit 0
    fi
  else
    newfolderpath=`zenity --entry --title="Name of new EasyGPG folder" --text="Enter the name of the new EasyGPG folder."`
    if test -n "${newfolderpath}"
    then
      newfolderpath="${ramdiskpath}/${newfolderpath}"
    else
      exit 0
    fi
    while test -e "${newfolderpath}"
    do
      newfolderpath=`zenity --entry --title="Name of new EasyGPG folder" --text="That name is already used.\nEnter a different name."`
      if test -n "${newfolderpath}"
      then
        newfolderpath="${ramdiskpath}/${newfolderpath}"
      else
        exit 0
      fi
    done
  fi
  mkdir -m 700 "${newfolderpath}"
  cp -a "${thisFile}" "${newfolderpath}/"
  ("${newfolderpath}/${thisFileName}" --ni) | zenity --progress --text="Building \"$(basename "${newfolderpath}")\"..." --pulsate --auto-close --no-cancel
  if test -n "${clonekeyrings}"
  then
    clonekeyrings="${clonekeyrings}/$(basename "${keydir}")"
    newkeyrings="${newfolderpath}/$(basename "${keydir}")"
    cp -af "${clonekeyrings}/$(basename "${pubkeybu}")" "${newkeyrings}" 2> /dev/null
    cp -af "${clonekeyrings}/$(basename "${seckeybu}")" "${newkeyrings}" 2> /dev/null
    if test -n "$(ls "${clonekeyrings}/$(basename "${custom}")/"* 2> /dev/null)"
    then
      for item in "${clonekeyrings}/$(basename "${custom}")/"*
      do
        if test -f "${item}" && file -b -i "${item}" | grep -v "charset=binary$" > /dev/null
        then
          itembase=`basename "${item}"`
          arguments=`tail -n +2 "${item}" | head -n 1`
          if test "${arguments}" = "--${itembase}"
          then
            newitembase="$(printf "%s\n" "${itembase}" | grep -o "^[a-z]\{3\}")$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10)"
            newitem="${newkeyrings}/$(basename "${custom}")/${newitembase}"
            head -n 1 "${item}" > "${newitem}"
            printf "%s\n" "--${newitembase}" >> "${newitem}"
            tail -n +3 "${item}" >> "${newitem}"
          else
            newitem="${newkeyrings}/$(basename "${custom}")/$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10)"
            cat "${item}" > "${newitem}"
          fi
          chmod 600 "${newitem}"
        fi
      done
    fi
    ("${newfolderpath}/${thisFileName}" --ni) | zenity --progress --text="Building \"$(basename "${newfolderpath}")\"..." --pulsate --auto-close --no-cancel
    "${newfolderpath}/${thisFileName}" --rk
  fi
  if zenity --question --width=400 --title="Backup RAM Disk?" --text="The contents of the RAM disk have changed. Backup the RAM disk?" --ok-label="Backup" --cancel-label="Cancel"
  then
    "${thisFile}" --br
  fi
# ==================
elif test "${1}" = "--md" # Put EasyGPG on the Desktop
then
  if test "${thisrootdir}" = "/mnt" || test "${thisrootdir}" = "/media" || test -n "${thisisram}"
  then
    if ! zenity --question --width=400 --title="Put EasyGPG on the Desktop?" --icon-name="dialog-warning" --text="One reason to run EasyGPG from a flash drive or RAM disk is not to leave evidence of its use behind.\nThis Desktop file will be evidence.\nCopy it anyway?" --ok-label="Proceed" --cancel-label="Cancel"
    then
      exit 0
    fi
  fi
  rm -f "${HOME}/Desktop/${dname}.desktop"
  if test -f "${HOME}/Desktop/${dfilename}"
  then
    zenity --info --width=400 --text="${dname} is already on the Desktop." &
    exit 0
  fi
  cp -af "${dfilepath}" "${HOME}/Desktop"
# ==================
elif test "${1}" = "--wd" # Write a Desktop file -- ${2} is save path without suffix and ${3} is arguments. ${4}, if present, is name.
  then
  savepath="${2}.desktop"
  execargs="${3}"
  if test -n "${4}"
  then
    name="${4}"
  else
    name="$(basename "${2}")"
  fi
  touch "${savepath}"
  chmod 700 "${savepath}"
  printf "%s\n" "#!/usr/bin/env xdg-open" > "${savepath}"
  printf "\n" >> "${savepath}"
  printf "%s\n" "[Desktop Entry]" >> "${savepath}"
  printf "%s\n" "Type=Application" >> "${savepath}"
  printf "%s\n" "Categories=Utility;Security;TextTools;" >> "${savepath}"
  printf "%s\n" "Name=${name}" >> "${savepath}"
  if test "${name}" = "${dname}"
  then
    printf "%s\n" "Comment=Use GPG with a graphical user interface" >> "${savepath}"
  else
    printf "%s\n" "Comment=$(printf "${name}" | sed "s/^[0-9]*\-//")" >> "${savepath}"
  fi
  printf "%s\n" "Exec=${thisFile} ${execargs}" >> "${savepath}"
  if test "${name}" = "${putramdiskaction}"
  then
    printf "%s\n" "Icon=folder" >> "${savepath}"
  elif test "${name}" = "${removeaction}"
  then
    printf "%s\n" "Icon=edit-delete" >> "${savepath}"
  elif test "${name}" = "${putmenuaction}"
  then
    printf "%s\n" "Icon=open-menu" >> "${savepath}"
  elif test "${name}" = "${putondesktopaction}"
  then
    printf "%s\n" "Icon=desktop" >> "${savepath}"
  elif test "${name}" = "${clearaction}" || test "${name}" = "${clearaction}-${thisdirbase}"
  then
    printf "%s\n" "Icon=edit-clear" >> "${savepath}"
  elif test "${name}" = "${createramdiskaction}"
  then
    printf "%s\n" "Icon=add" >> "${savepath}"
  elif test "${name}" = "${removeramdiskaction}"
  then
    printf "%s\n" "Icon=remove" >> "${savepath}"
  elif test "${name}" = "${backupramdiskaction}"
  then
    printf "%s\n" "Icon=filesave" >> "${savepath}"
  elif test "${name}" = "${makeegpgaction}"
  then
    printf "%s\n" "Icon=folder-new" >> "${savepath}"
  else
    printf "%s\n" "Icon=${iconpath}" >> "${savepath}"
  fi
  if printf "%s\n" "${execargs}" | grep "%[fF]$" > /dev/null
  then
    printf "%s\n" "MimeType=text/PGP;application/pgp;application/pgp-keys;application/pgp-encrypted;application/pgp-signature;text/plain;inode/directory;" >> "${savepath}"
  fi
  if which "gio" > /dev/null
  then
    gio info "${savepath}" > /dev/null
    gio set "${savepath}" metadata::caja-trusted-launcher true
  fi
  if test -f "${HOME}/Desktop/$(basename "${savepath}")"
  then
    cp -af "${savepath}" "${HOME}/Desktop/"
  fi
  if test -f "$(dirname "${thisdir}")/$(basename "${savepath}")"
  then
    cp -af "${savepath}" "$(dirname "${thisdir}")/"
  fi
  if test -f "${ramdiskpath}/$(basename "${savepath}")"
  then
    cp -af "${savepath}" "${ramdiskpath}"
  fi
  if test -f "${menudir}/${pathbase}.desktop"
  then
    "${thisFile}" --mf "${savepath}"
  fi
# ==================
elif test "${1}" = "--pm" # Put EasyGPG in the Applications menu
then
  rm -f "${menudir}/${dname}.desktop"
  if test -f "${menudir}/${dfilename}"
  then
    zenity --info --width=400 --text="${dname} is already in the menu." &
    exit 0
  fi
  "${thisFile}" --mm "${dfilepath}"
# ==================
elif test "${1}" = "--dm" # Remove EasyGPG from the Applications menu
then
  if ! test -f "${menudir}/${dfilename}" &&  ! test -f "${menudir}/${dname}.desktop"
  then
    zenity --info --width=400 --text="${dname} is not in the Applications menu." &
    exit 0
  fi
  rm -f "${menudir}/${dname}.desktop"
  rm -f "${menudir}/${dfilename}"
  zenity --info --width=400 --text="The Applications menu item (${dname}) was removed." &
# ==================
elif test "${1}" = "--mm" || test "${1}" = "--mf" # Make Applications menu item -- ${2} is pathname of Action
then
  if (test "${thisrootdir}" = "/mnt" || test "${thisrootdir}" = "/media" || test -n "${thisisram}") && test "${1}" = "--mm"
  then
    if ! zenity --question --width=400 --title="Make a menu entry?" --icon-name="dialog-warning" --text="One reason to run EasyGPG from a flash drive or RAM disk is not to leave evidence of its use behind.\nThis menu item will be evidence.\nMake it anyway?" --ok-label="Proceed" --cancel-label="Cancel"
    then
      exit 0
    fi
  fi
  mkdir -p -m 664 "${menudir}"
  cp -af "${2}" "${menudir}"
# ==================
elif test "${1}" = "--cd" # Put an Action on the Desktop; ${2} is the Action
then
  if test -z "${2}"
  then
    printf "%s\n" "No file was specified." 1>&2
    exit 1
  fi
  theaction=`readlink -e "${2}"`
  if ! test -f "${custom}/$(basename -s .desktop "${theaction}")"
  then
    if test "$(printf "%s\n" "${theaction}" | grep -o "[^\.]*$")" = "desktop"
    then
      "${thisFile}" --mc "${theaction}" --cd
    else
      zenity --info --width=400 --text="\"$(basename "${theaction}")\" is not an Action." &
    fi
    exit 0
  fi
  if test -f "${HOME}/Desktop/$(basename "${theaction}")"
  then
    zenity --info --width=400 --text="\"$(grep "^Name=" "${theaction}" | sed "s/^Name=//" | sed "s/^[0-9]*\-//")\" is already on the Desktop." &
  else
    if test "${thisrootdir}" = "/mnt" || test "${thisrootdir}" = "/media" || test -n "${thisisram}"
    then
      if ! zenity --question --width=400 --title="Put the Action on the Desktop?" --icon-name="dialog-warning" --text="One reason to run EasyGPG from a flash drive or RAM disk is not to leave evidence of its use behind.\nPutting this Action on the Desktop will create evidence.\nDo it anyway?" --ok-label="Proceed" --cancel-label="Cancel"
      then
        exit 0
      fi
    fi
    cp -af "${theaction}" "${HOME}/Desktop"
  fi
# ==================
elif test "${1}" = "--am" # Put an action in the Applications menu; ${2} is the Desktop file in ${actionsdir}
then
  if test -z "${2}"
  then
    printf "%s\n" "No file was specified." 1>&2
    exit 1
  fi
  theaction=`readlink -e "${2}"`
  if test -f "${theaction}" && test -f "${custom}/$(basename -s .desktop "${theaction}")"
  then
    if ! test -f "${menudir}/$(basename "${theaction}")"
    then
      "${thisFile}" --mm "${theaction}"
    else
      zenity --info --width=400 --text="\"$(grep "^Name=" "${theaction}" | sed "s/^Name=//")\" is already in the menu." &
    fi
  elif test "$(printf "%s\n" "${theaction}" | grep -o "[^\.]*$")" = "desktop"
  then
    "${thisFile}" --mc "${theaction}" --am
  else
    zenity --info --width=400 --text="\"$(basename "${theaction}")\" is not an Action." &
  fi
# ==================
elif test "${1}" = "--cr" # Copy an Action to the RAM disk; ${2} is the Action
then
  if test -z "${2}"
  then
    printf "%s\n" "No file was specified." 1>&2
    exit 1
  fi
  theaction=`readlink -e "${2}"`
  actionbase=`basename "${theaction}"`
  if ! test -f "${custom}/$(basename -s .desktop "${theaction}")"
  then
    if test "$(printf "%s\n" "${theaction}" | grep -o "[^\.]*$")" = "desktop"
    then
      "${thisFile}" --mc "${theaction}" --cr
    else
      zenity --info --width=400 --text="\"$(basename "${theaction}")\" is not an Action." &
    fi
    exit 0
  fi
  if test -f "${rammountpoint}/${actionbase}"
  then
    zenity --info --width=400 --text="\"$(grep "^Name=" "${theaction}" | sed "s/^Name=//" | sed "s/^[0-9]*\-//")\" is already on the RAM disk." &
  else
    cp -af "${theaction}" "${rammountpoint}"
    if which "gio" > /dev/null
    then
      gio info "${rammountpoint}/${actionbase}" > /dev/null
      gio set "${rammountpoint}/${actionbase}" metadata::caja-trusted-launcher true
    fi
  fi
# ==================
elif test "${1}" = "--mc" # Copy an action as a custom action -- ${2} is the action -- ${3} should be either --cd, --am, or --cr
then
  if test -z "${2}" || test -z "${3}"
  then
    exit 1
  fi
  theaction=`readlink -e "${2}"`
  if test "$(printf "%s\n" "${theaction}" | grep -o "[^\.]*$")" = "desktop"
  then
    cname=`grep "^Name=" "${theaction}" | sed "s/^Name=//" | sed "s/^[0-9]*\-//"`
    actionbase=`basename -s .desktop "${theaction}"`
    if test -f "${custom}/${actionbase}"
    then
      exit 0
    fi
    execargs=`grep "^Exec=" "${theaction}" | grep -o "\-\-.*$" | sed "s/^\-\-//"`
    if test -f "${theaction}" && grep "^Exec=${thisFile}" "${theaction}" > /dev/null && test -n "${execargs}" && test "${execargs}" != "ra" && test "${execargs}" != "am" && test "${execargs}" != "cd"
    then
      if test -n "$(ls -A "${custom}")" && cat "${custom}/"* | grep "${execargs}$" > /dev/null
      then
        zenity --info --width=400 --text="Drag and drop \"${cname}-${thisdirbase}\" instead of \"${actionbase}.\""
        exit 0
      fi
      if test "${3}" = "--ra"
      then
        zenity --info --width=400 --text="\"${cname}\" is not a custom Action." &
        exit 0
      fi
      custname="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10)"
      custpath="${custom}/${custname}"
      printf "%s\n" "${cname}" > "${custpath}"
      printf "%s\n" "--${execargs}" >> "${custpath}"
      chmod 600 "${custpath}"
      "${thisFile}" --nn
      "${thisFile}" "${3}" "${actionsdir}/${custname}.desktop"
    else
      zenity --info --width=400 --text="\"${cname}\" is not an Action." &
    fi
  fi
# ==================
elif test "${1}" = "--ra" # Remove a custom action; ${2} is the Desktop file
then
  if test -z "${2}"
  then
    printf "%s\n" "No file was specified." 1>&2
    exit 1
  fi
  theaction=`readlink -e "${2}"`
  actionbase=`basename "${theaction}"`
  if test -f "${theaction}" && test -f "${custom}/$(basename -s .desktop "${theaction}")"
  then
    rm -f "${custom}/$(basename -s .desktop "${theaction}")"
    rm -f "${actionsdir}/${actionbase}"
    rm -f "${menudir}/${actionbase}"
    rm -f "${HOME}/Desktop/${actionbase}"
    rm -f "$(dirname "${thisdir}")/${actionbase}"
    rm "${theaction}"
  elif test "$(printf "%s\n" "${theaction}" | grep -o "[^\.]*$")" = "desktop"
  then
    "${thisFile}" --mc "${theaction}" --ra
  else
    zenity --info --width=400 --text="\"${actionbase}\" is not a custom Action." &
  fi
# ==================
elif test "${1}" = "--cc" # clear copied text
then
  printf "" | xclip -i -selection clipboard
  pkill xclip
  zenity --info --width=400 --text="Copied text is cleared." &
# ==================
elif test "${1}" = "--ps" # print secret keys to stdout
then
  secretkeyids=`gpg --homedir "${keydir}" -K --with-colons --fixed-list-mode --with-fingerprint | tr -d "\n" | sed "s/$/\n/" | sed "s/:sec:/:\nsec:/g" | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*"`
  senderKeys=""
  for akey in ${secretkeyids}
  do
    senderKeys="${senderKeys}"`gpg --homedir "${keydir}" -k --with-colons --fixed-list-mode --with-fingerprint ${akey} | tr -d "\n" | grep -o "pub.*$" | grep "pub:[fumq-]:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*S[^:]" | sed "s/uid/\nuid/g" | grep -o "^uid:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*" | grep -o "[^:]*$" | sed "s/^/${akey}:/" | head -n 1`"\n"
  done
  senderKeys=`printf "${senderKeys}" | grep -v "^$" | sort -t : -k 2 | sed "s/:/\n/" | sed "s/[\]x3a/:/ig"`
  printf "%s\n" "${senderKeys}"
# ==================
elif test "${1}" = "--pp" # print public keys to stdout
then
  if test "${2}" = "all"
  then
    recipientKeyIDs=`gpg --homedir "${keydir}" -k --with-colons --fixed-list-mode --with-fingerprint | grep "^[^:]*:[^:]" | tr -d "\n" | sed "s/$/\n/" | sed "s/:pub:/:\npub:/g" | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*" | grep -o "^[^:]*"`
  else
    recipientKeyIDs=`gpg --homedir "${keydir}" -k --with-colons --fixed-list-mode --with-fingerprint | grep "^[^:]*:[fumq-]" | tr -d "\n" | sed "s/$/\n/" | sed "s/:pub:/:\npub:/g" | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*" | grep "^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*E[^:]*:" | grep -o "^[^:]*"`
  fi
  recipientKeys=""
  for akey in ${recipientKeyIDs}
  do
    if test "${2}" = "all"
    then
      recipientKeys="${recipientKeys}"`gpg --homedir "${keydir}" -k --with-colons --fixed-list-mode --with-fingerprint ${akey} | grep "^uid" | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:/${akey}:/" | grep -o "^[^:]*:[^:]*" | head -n 1`"\n"
    else
      recipientKeys="${recipientKeys}"`gpg --homedir "${keydir}" -k --with-colons --fixed-list-mode --with-fingerprint ${akey} | grep "^uid" | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:/${akey}:/" | grep -o "^[^:]*:[^:]*"`"\n"
    fi
  done
  recipientKeys=`printf "${recipientKeys}" | grep -v "^$" | sort -t : -k 2 | sed "s/:/\n/" | sed "s/[\]x3a/:/ig"`
  printf "%s\n" "${recipientKeys}"
# ================== Encrypt a message
elif test "${1}" = "--ex" ||  test "${1}" = "--em" ||  test "${1}" = "--eo" ||  test "${1}" = "--eg" ||  test "${1}" = "--et" ||  test "${1}" = "--emc" ||  printf "%s\n" "${1}" | grep "^\-\-emc[a-zA-Z0-9]\{10\}$" > /dev/null
then
  theoption="${1}"
  if printf "%s\n" "${theoption}" | grep "^\-\-emc[a-zA-Z0-9]\{10\}$" > /dev/null
  then
    custpath="${custom}/$(printf "%s\n" "${theoption}" | sed "s/^\-\-//")"
    if ! test -f "${custpath}"
    then
      rm "${actionsdir}/$(basename "${custpath}").desktop"
      rm -f "${menudir}/$(basename "${custpath}").desktop"
      rm -f "${HOME}/Desktop/$(basename "${custpath}").desktop"
      zenity --info --width=400 --text="Because \"$(basename "${custpath}")\" has disappeared from the \"egpgdesk\" folder in \"easygpgkeyrings\" folder, this Action can no longer be used. It has been removed." &
      exit 0
    fi
    senderID=`tail -n +3 "${custpath}" | head -n 1`
    keylist=`tail -n +4 "${custpath}"`
    recipientID=`printf "%s\n%s\n" "${senderID}" "${keylist}" | grep -o "^[A-F0-9]\+" | sort -u`
    for akey in ${recipientID}
    do
      if test -z "$(gpg --homedir "${keydir}" -k "${akey}" 2> /dev/null)"
      then
        zenity --info --width=400 --text="Because the key ${akey} has disappeared from the keyring, this Action can no longer be used." &
        exit 0
      fi
    done
  else
    senderKeys=`"${thisFile}" --ps`
    totalkeys=`printf "%s\n" "${senderKeys}" | wc -l`
    if test "${totalkeys}" -lt 2
    then
      zenity --info --width=400 --text="You have no personal key pair.\nYou'll need to make or import one before\nyou can encrypt and sign messages." &
      exit 0
    fi
    if test "${theoption}" = "--emc"
    then
      actionname=`zenity --entry --text="Enter a name for the new Encrypt a message Action." --title="Name of new custom Action" --entry-text="Encrypt and Copy a Message to "`
      if test -z "${actionname}"
      then
        exit 0
      fi
    fi
    if test "${totalkeys}" -eq 2
    then
      senderID=`printf "%s\n" "${senderKeys}" | head -n 1`
    else
      senderID=`printf "%s\n" "${senderKeys}" | zenity --list --title="Select Sender" --text="Select key to sign message." --width=600 --height=401 --column="Key ID                  " --column="Sender" 2> /dev/null | grep -o "^[^|]*"`
    fi
    if test -z "${senderID}"
    then
      exit 0
    fi
    recipientKeys=`"${thisFile}" --pp`
    totalkeys=`printf "%s\n" "${recipientKeys}" | wc -l`
    if test "${totalkeys}" -eq 2
    then
      keylist=`printf "%s\n" "${recipientKeys}" | tr "\n" " " | sed "s/ $//"`
    else
      keylist=`printf "%s\n" "${recipientKeys}" | zenity --list --multiple --print-column=ALL --title="Select Recipients" --text="Select recipients' keys to encrypt with." --width=600 --height=401 --column="Key ID                  " --column="Recipients" 2> /dev/null | sed "s/[^|]*|[^|]*|/&\n/g" | sed "s/|$//" | sed "s/|/ /"`
    fi
    if test -z "${keylist}"
    then
      exit 0
    fi
    if test "${theoption}" = "--emc"
    then
      custname="emc$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10)"
      custpath="${custom}/${custname}"
      printf "%s\n" "${actionname}" > "${custpath}"
      printf "%s\n" "--${custname} %f" >> "${custpath}"
      printf "%s\n" "${senderID}" >> "${custpath}"
      printf "%s\n" "${keylist}" >> "${custpath}"
      chmod 600 "${custpath}"
      "${thisFile}" --nn
      exit 0
    fi
  fi
  recipientID=`printf "%s\n" "${keylist}" | grep -o "^[A-F0-9]\+" | uniq`
  nameandaddress=`printf "%s\n" "${keylist}" | head -n 1 | sed "s/^[A-F0-9]\+ //"`
  justname=`printf "%s\n" "${nameandaddress}" | sed "s/ <[^>]*>$//"`
  if test $(printf "%s\n" "${recipientID}" | wc -l) -gt 1
  then
    nameandaddress="${nameandaddress}, etc."
    justname="${justname}, etc."
  fi
  theText=`zenity --text-info --editable --font="monospace" --title="Type a message for ${nameandaddress}" --ok-label="Encrypt for ${justname}" --width=620 --height=620`
  if test -z "${theText}"
  then
    exit 0
  fi
  if zenity --question --width=400 --title="Add public key?" --text="Add your public key to the message?\nWhen the recipient reads the message with EasyPGP,\nthis public key will automatically be imported,\nif it is not already in the recipient's keyring."
  then
    theText=`printf "%s\n-----------------\n%s\n" "${theText}" "$(gpg --homedir "${keydir}" --export -a --no-emit-version "${senderID}")"`
  fi
  recipients=`printf "%s\n" "${recipientID}" | sed "s/^/-R /" | tr "\n" " " | sed "s/ $//"`
  if test "${1}" = "--ex"
  then
    if zenity --question --width=400 --title="Save as .gpg or .asc?" --text="Save the message in binary form (.gpg) or text form (.asc)?" --ok-label=".asc" --cancel-label=".gpg"
    then
      theoption="--et"
    else
      theoption="--eg"
    fi
  fi
  if test "${theoption}" = "--eg"
  then
    savepath="${defaultdir}/"`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10`".gpg"
    savepath=`zenity --file-selection --save --confirm-overwrite --filename="${savepath}" --title="Save as..."`
    if test -n "${savepath}"
    then
      printf "%s\n" "${theText}" | gpg --homedir "${keydir}" --trust-model always --textmode -s -u "${senderID}" -e ${recipients} --no-emit-version --no-encrypt-to --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty - > "${savepath}"
    fi
    exit 0
  fi
  encryptedText=`printf "%s\n" "${theText}" | gpg --homedir "${keydir}" -a --trust-model always --textmode -s -u "${senderID}" -e ${recipients} --no-emit-version --no-encrypt-to --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty -`
  if test "${theoption}" = "--eo"
  then
    printf "%s\n" "${encryptedText}"
  elif test "${theoption}" = "--et"
  then
    savepath="${defaultdir}/"`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10`".asc"
    savepath=`zenity --file-selection --save --confirm-overwrite --filename="${savepath}" --title="Save as..."`
    if test -n "${savepath}"
    then
      printf "%s\n" "${encryptedText}" > "${savepath}"
    fi
  else
    addresslist=`printf "%s\n" "${keylist}" | grep -o "<[^>]*>" | sed "s/^<//" | sed "s/>$//" | uniq | tr "\n" "," | sed "s/,$//"`
    if test -n "${addresslist}"
    then
      encryptedText=`printf "%s\n\n%s" "${addresslist}" "${encryptedText}"`
    fi
    printf "%s\n" "${encryptedText}" | xclip -i -selection clipboard
  fi
# ==================
elif test "${1}" = "--da" # Encrypt a message with deniable authentication and copy it
then
  recipientKeys=`"${thisFile}" --pp`
  totalkeys=`printf "%s\n" "${recipientKeys}" | wc -l`
  if test "${totalkeys}" -lt 2
  then
    zenity --info --width=400 --text="You have no public keys in your keyrings.\nTo send encrypted messages to others,\nyou'll need to import their public keys." &
    exit 0
  elif test "${totalkeys}" -eq 2
  then
    recipientID=`printf "%s\n" "${recipientKeys}" | head -n 1`
  else
    recipientID=`printf "%s\n" "${recipientKeys}" | zenity --list --title="Select Recipient" --text="Select recipient's key to encrypt with." --width=600 --height=401 --column="Key ID                  " --column="Recipient" 2> /dev/null | grep -o "^[^|]*"`
  fi
  if test -z "${recipientID}"
  then
    exit 0
  fi
  nameandaddress=`gpg --homedir "${keydir}" -k --with-colons --fixed-list-mode "${recipientID}" | grep "^uid" | head -n 1 | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*" | sed "s/[\]x3a/:/ig"`
  justname=`printf "%s\n" "${nameandaddress}" | sed "s/ <[^>]*>$//"`
  theText=`zenity --text-info --editable --font="monospace" --title="Type a message for ${nameandaddress}" --filename=/dev/stdin --ok-label="Encrypt for ${justname}" --width=620 --height=620`
  if test -z "${theText}"
  then
    exit 0
  fi
  thesecret=`zenity --entry --title="Deniable authentication secret" --text="Enter the shared secret that you and ${justname} agreed upon."`
  if test -z "${thesecret}"
  then
    exit 0
  fi
  themac=`printf "%s\n%s\n" "${thesecret}" "${theText}" | sha512sum | grep -o "^[^ ]*"`
  encryptedText=`printf "MAC:%s\n%s\n" "${themac}" "${theText}" | gpg --homedir "${keydir}" -a --trust-model always --textmode -e -R "${recipientID}" --no-emit-version --no-encrypt-to --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty -`
  printf "%s\n" "${encryptedText}" | xclip -i -selection clipboard
# ==================
elif test "${1}" = "--sm" || test "${1}" = "--ms" # Sign a message and copy it or save it
then
  senderKeys=`"${thisFile}" --ps`
  totalkeys=`printf "%s\n" "${senderKeys}" | wc -l`
  if test "${totalkeys}" -lt 2
  then
    zenity --info --width=400 --text="You have no personal key pair.\nYou'll need to make or import one before\nyou can sign messages." &
    exit 0
  fi
  if ! zenity --question --width=400 --title="Sign but don't encrypt?" --icon-name="dialog-warning" --text="Please remember that this message will NOT be encrypted.\nAnyone with GPG or other PGP software will be able to read it." --ok-label="Proceed" --cancel-label="Cancel"
  then
    exit 0
  fi
  if test "${totalkeys}" -eq 2
  then
    senderID=`printf "%s\n" "${senderKeys}" | head -n 1`
  else
    senderID=`printf "%s\n" "${senderKeys}" | zenity --list --title="Select Sender" --text="Select key to sign message." --width=600 --height=401 --column="Key ID                  " --column="Sender" 2> /dev/null | grep -o "^[^|]*"`
  fi
  if test -z "${senderID}"
  then
    exit 0
  fi
  justname=`gpg --homedir "${keydir}" --with-colons --fixed-list-mode -k "${senderID}" | grep "^uid" | head -n 1 | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*" | sed "s/[\]x3a/:/ig" | sed "s/ <[^>]*>$//"`
  theText=`zenity --text-info --editable --font="monospace" --title="Type a message to sign" --ok-label="Sign as ${justname}" --width=620 --height=620`
  if test -z "${theText}"
  then
    exit 0
  fi
  encryptedtext=`printf "%s\n" "${theText}" | gpg --homedir "${keydir}" -a --trust-model always --textmode -s -u "${senderID}" --no-emit-version --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty -`
  if test "${1}" = "--sm"
  then
    printf "%s\n" "${encryptedtext}" | xclip -i -selection clipboard
  else
    savepath="${defaultdir}/"`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10`".asc"
    savepath=`zenity --file-selection --save --confirm-overwrite --filename="${savepath}" --title="Save as..."`
    if test -n "${savepath}"
    then
      printf "%s\n" "${encryptedtext}" > "${savepath}"
    fi
  fi
# ==================
elif test "${1}" = "--ff" # Choose file or folder -- ${2} is action to perform on file or folder
then
  if test "${thisrootdir}" = "/mnt" || test "${thisrootdir}" = "/media"
  then
    filename=`df --output=target "${thisFile}" | grep "^/"`
  elif test -n "${thisisram}"
  then
    filename=${rammountpoint}
  elif test -n "${gotramdisk}"
  then
    filename="${ramdiskpath}"
  else
    filename="${HOME}"
  fi
  if zenity --question --width=400 --title="File or Folder?" --text="${2} a file or a folder?" --ok-label="File" --cancel-label="Folder"
  then
    filename=`zenity --file-selection --filename="${filename}/" --title "Select a file"`
  else
    filename=`zenity --file-selection --directory --filename="${filename}/" --title "Select a folder"`
  fi
  if test -z "${filename}"
  then
    exit 0
  fi
  printf "%s\n" "${filename}"
# ================== Encrypt an item as a tar archive; ${2} is archive path name
elif test "${1}" = "--sx" || test "${1}" = "--sf" ||  test "${1}" = "--st" ||  test "${1}" = "--uf" ||  test "${1}" = "--ef" ||  test "${1}" = "--sfc" || printf "%s\n" "${1}" | grep "^\-\-sfc[a-zA-Z0-9]\{10\}$" > /dev/null
then
  theoption="${1}"
  if printf "%s\n" "${theoption}" | grep "^\-\-sfc[a-zA-Z0-9]\{10\}$" > /dev/null
  then
    custpath="${custom}/$(printf "%s\n" "${theoption}" | sed "s/^\-\-//")"
    if ! test -f "${custpath}"
    then
      rm "${actionsdir}/$(basename "${custpath}").desktop"
      rm -f "${menudir}/$(basename "${custpath}").desktop"
      rm -f "${HOME}/Desktop/$(basename "${custpath}").desktop"
      zenity --info --width=400 --text="Because \"$(basename "${custpath}")\" has disappeared from the \"egpgdesk\" folder in \"easygpgkeyrings\" folder, this Action can no longer be used. It has been removed." &
      exit 0
    fi
    senderID=`tail -n +3 "${custpath}" | head -n 1`
    recipientID=`tail -n +4 "${custpath}" | grep -o "^[A-F0-9]\+" | uniq`
    for akey in ${recipientID}
    do
      if test -z "$(gpg --homedir "${keydir}" -k "${akey}" 2> /dev/null)"
      then
        zenity --info --width=400 --text="Because the key ${akey} has disappeared from the keyring, this Action can no longer be used." &
        exit 0
      fi
    done
  else
    senderKeys=`"${thisFile}" --ps`
    totalkeys=`printf "%s\n" "${senderKeys}" | wc -l`
    if test "${totalkeys}" -lt 2
    then
      if test "${theoption}" != "--uf"
      then
        zenity --info --width=400 --text="You have no personal key pair.\nYou'll need to make or import one before\nyou can encrypt files." &
        exit 0
      fi
    fi
    if test "${theoption}" = "--sfc"
    then
      actionname=`zenity --entry --text="Enter a name for the new Encrypt files Action." --title="Name of new custom Action" --entry-text="Encrypt a File or Folder for "`
      if test -z "${actionname}"
      then
        exit 0
      fi
    fi
    if test "${theoption}" = "--uf"
    then
      if ! zenity --question --width=400 --title="Encrypt but don't sign?" --icon-name="dialog-warning" --text="This tar archive will be encrypted but not signed.\nNormally this is only done when creating an attachment for a message using deniable authentication." --ok-label="Proceed" --cancel-label="Cancel"
      then
        exit 0
      fi
    else
      if test "${totalkeys}" -eq 2
      then
        senderID=`printf "%s\n" "${senderKeys}" | head -n 1`
      else
        senderID=`printf "%s\n" "${senderKeys}" | zenity --list --title="Select Sender" --text="Select key to sign archive" --width=600 --height=401 --column="Key ID                  " --column="Sender" 2> /dev/null | grep -o "^[^|]*"`
      fi
      if test -z "${senderID}"
      then
        exit 0
      fi
    fi
    if test "${theoption}" = "--ef"
    then
      recipientID="${senderID}"
    else
      recipientKeys=`"${thisFile}" --pp`
      totalkeys=`printf "%s\n" "${recipientKeys}" | wc -l`
      if test "${totalkeys}" -lt 2
      then
        zenity --info --width=400 --text="You have no public keys to encrypt with.\nYou'll need to make or import one before\nyou can encrypt files." &
        exit 0
      fi
      if test "${totalkeys}" -eq 2
      then
        recipientID=`printf "%s\n" "${recipientKeys}" | head -n 1`
      else
        recipientID=`printf "%s\n" "${recipientKeys}" | zenity --list --multiple --title="Select Recipients" --text="Select recipients' keys to encrypt with." --width=600 --height=401 --column="Key ID                  " --column="Recipients" 2> /dev/null | tr "|" "\n" | uniq`
      fi
      if test -z "${recipientID}"
      then
        exit 0
      fi
    fi
    if test "${theoption}" = "--sfc"
    then
      custname="sfc$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10)"
      custpath="${custom}/${custname}"
      printf "%s\n" "${actionname}" > "${custpath}"
      printf "%s\n" "--${custname} %f" >> "${custpath}"
      printf "%s\n" "${senderID}" >> "${custpath}"
      printf "%s\n" "${recipientID}" >> "${custpath}"
      chmod 600 "${custpath}"
      "${thisFile}" --nn
      exit 0
    fi
  fi
  filename="${2}"
  if test -z "${filename}"
  then
    filename=`"${thisFile}" --ff "Encrypt"`
  else
    filename="$(readlink -e "${2}")"
  fi
  if test -z "${filename}"
  then
    exit 0
  fi
  if ! test -e "${filename}"
  then
    zenity --info --width=400 --text="\"${filename}\" does not exist." &
    exit 1
  fi
  if test "${filename}" = "${ramdiskpath}" && test -z "$(ls -A "${ramdiskpath}")"
  then
    zenity --info --width=400 --text="The RAM disk is empty." &
    exit 0
  fi
  if test "${theoption}" = "--sx"
  then
    if zenity --question --width=400 --title="Save as .gpg or .asc?" --text="Save \"$(basename "${2}")\" as an encrypted tar archive in text form (.asc) or binary form (.gpg)?\nThe binary form will be smaller." --ok-label=".gpg" --cancel-label=".asc"
    then
      theoption="--sf"
    else
      theoption="--st"
    fi
  fi
  if test "${theoption}" = "--ef" && test -n "${3}" && test -d "$(dirname "${3}")"
  then
    savepath="${3}"
  else
    if test "${theoption}" = "--st"
    then
      savepath="${defaultdir}/"`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10`".asc"
    else
      savepath="${defaultdir}/"`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10`".gpg"
    fi
    savepath=`zenity --file-selection --save --confirm-overwrite --filename="${savepath}" --title="Save as..."`
  fi
  if test -n "${savepath}"
  then
    cd "$(dirname "${filename}")"
    recipients=`printf "%s\n" "${recipientID}" | sed "s/^/-R /" | tr "\n" " " | sed "s/ $//"`
    if test "${theoption}" = "--uf"
    then
      (tar --numeric-owner -c "$(basename "${filename}")" | gpg --homedir "${keydir}" --trust-model always -e ${recipients} --no-emit-version --no-encrypt-to --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty --yes -o "${savepath}" -) | zenity --progress --text="Encrypting..." --pulsate --auto-close --no-cancel
    elif test "${theoption}" = "--st"
    then
      (tar --numeric-owner -c "$(basename "${filename}")" | gpg --homedir "${keydir}" --trust-model always -a -s -u "${senderID}" -e ${recipients} --no-emit-version --no-encrypt-to --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty --yes -o "${savepath}" -) | zenity --progress --text="Encrypting..." --pulsate --auto-close --no-cancel
    elif test "${filename}" = "${ramdiskpath}"
    then
      cd "${ramdiskpath}"
      (tar --numeric-owner -c * .[^.]* 2> /dev/null | gpg --homedir "${keydir}" --trust-model always -s -u "${senderID}" -e ${recipients} --no-emit-version --no-encrypt-to --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty --yes -o "${savepath}" -) | zenity --progress --text="Encrypting..." --pulsate --auto-close --no-cancel
    else
      (tar --numeric-owner -c "$(basename "${filename}")" | gpg --homedir "${keydir}" --trust-model always -s -u "${senderID}" -e ${recipients} --no-emit-version --no-encrypt-to --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty --yes -o "${savepath}" -) | zenity --progress --text="Encrypting..." --pulsate --auto-close --no-cancel
    fi
  fi
# ==================
elif test "${1}" = "--xs" ||  test "${1}" = "--ss" || test "${1}" = "--sg" # Save an item as a signed tar archive; ${2} file or folder pathname; ${3} is optional archive path
then
  theoption="${1}"
  senderKeys=`"${thisFile}" --ps`
  totalkeys=`printf "%s\n" "${senderKeys}" | wc -l`
  if test "${totalkeys}" -lt 2
  then
    zenity --info --width=400 --text="You have no personal key pair.\nYou'll need to make or import one before\nyou can sign files." &
    exit 0
  fi
  if test -z "${3}" && ! zenity --question --width=400 --title="Sign but don't encrypt?" --icon-name="dialog-warning" --text="Please remember that this file will NOT be encrypted.\nAnyone with GPG or other PGP software will be able to read it." --ok-label="Proceed" --cancel-label="Cancel"
  then
    exit 0
  fi
  filename="${2}"
  if test -z "${filename}"
  then
    filename=`"${thisFile}" --ff "Sign"`
  fi
  if test -z "${filename}"
  then
    exit 0
  fi
  if ! test -e "${filename}"
  then
    zenity --info --width=400 --text="\"${filename}\" does not exist." &
    exit 1
  fi
  if test "${totalkeys}" -eq 2
  then
    senderID=`printf "%s\n" "${senderKeys}" | head -n 1`
  else
    senderID=`printf "%s\n" "${senderKeys}" | zenity --list --title="Select Signer" --text="Select key to sign archive" --width=600 --height=401 --column="Key ID                  " --column="Signer" 2> /dev/null | grep -o "^[^|]*"`
  fi
  if test -z "${senderID}"
  then
    exit 0
  fi
  if test "${theoption}" = "--xs"
  then
    if zenity --question --width=400 --title="Save as .gpg or .asc?" --text="Save \"$(basename "${2}")\" as an signed tar archive in text form (.asc) or binary form (.gpg)?\nThe binary form will be smaller." --ok-label=".gpg" --cancel-label=".asc"
    then
      theoption="--sg"
    else
      theoption="--ss"
    fi
  fi
  if test -n "${3}" && test -d "$(dirname "${3}")"
  then
    savepath="${3}"
  else
    savepath="${defaultdir}/$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10)"
    if test "${theoption}" = "--sg"
    then
      savepath="${savepath}.gpg"
    else
      savepath="${savepath}.asc"
    fi
    savepath=`zenity --file-selection --save --confirm-overwrite --filename="${savepath}" --title="Save as..."`
  fi
  if test -n "${savepath}"
  then
    cd "$(dirname "${filename}")"
    if test "${theoption}" = "--sg"
    then
      tar --numeric-owner -c "$(basename "${filename}")" | gpg --homedir "${keydir}" --trust-model always -s -u "${senderID}" --no-emit-version --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty --yes -o "${savepath}" -
    else
      tar --numeric-owner -c "$(basename "${filename}")" | gpg --homedir "${keydir}" -a --trust-model always -s -u "${senderID}" --no-emit-version --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty --yes -o "${savepath}" -
    fi
  fi
# ==================
elif test "${1}" = "--gm" # Read messages from stdin -- ${2} is filename from --rf
then
  copiedText=`cat /dev/stdin | tr -d "\r" | sed "s/\-\-\-\-\-BEGIN PGP [^ ]* *MESSAGE\-\-\-\-\-/\n&/" | sed "s/\-\-\-\-\-END PGP .*\-\-\-\-\-/&\n/"`
  messageNumber="0"
  while true
  do
    messageNumber=`expr ${messageNumber} + 1`
    messageStart=`printf "%s\n" "${copiedText}" | grep -n "^\-\-\-\-\-BEGIN PGP [^ ]* *MESSAGE\-\-\-\-\-" | grep -o "^[^:]*" | tail -n +${messageNumber} | head -n 1`
    if test -z "${messageStart}"
    then
      break
    fi
    encryptedText=`printf "%s\n" "${copiedText}" | tail -n +${messageStart}`
    if ! printf "%s\n" "${encryptedText}" | grep "\-\-\-\-\-BEGIN PGP [^ ]* *MESSAGE\-\-\-\-\-$" > /dev/null
    then
      encryptedText=`printf "%s\n" "${encryptedText}" | sed "s/\-\-\-\-\-BEGIN PGP [^ ]* *MESSAGE\-\-\-\-\-/&\n/"`
    fi
    if ! printf "%s\n" "${encryptedText}" | grep "^\-\-\-\-\-END PGP .*\-\-\-\-\-" > /dev/null
    then
      encryptedText=`printf "%s\n" "${encryptedText}" | sed "s/\-\-\-\-\-END PGP .*\-\-\-\-\-/\n&/"`
    fi
    messageLength=`printf "%s\n" "${encryptedText}" | grep -n "^\-\-\-\-\-END PGP .*\-\-\-\-\-$" | grep -o "^[^:]*" | head -n 1`
    if test -n "${messageLength}"
    then
      encryptedText=`printf "%s\n" "${encryptedText}" | head -n ${messageLength}`
    fi
    while true
    do
      filetype=`printf "%s\n" "${encryptedText}" | gpg --homedir "${keydir}" --use-agent --no-tty -d - 2> /dev/null | file -b -i -`
      if printf "%s\n" "${filetype}" | grep -v "charset=" > /dev/null
      then
        filetype="" # decryption failed
      elif printf "%s\n" "${filetype}" | grep -v "charset=binary$" > /dev/null
      then
        filetype="text"
      else
        filetype=`printf "%s\n" "${filetype}" | grep -o "^[^;]*" | grep -o "[^/]*$" | sed "s/x-//"`
      fi
      temp="${tempfiledir}/.$(cat /dev/urandom | tr -dc "0-9a-z" | head -c 10)"
      if test "${filetype}" = "text"
      then
        decryptedText=`printf "%s\n" "${encryptedText}" | gpg --homedir "${keydir}" --use-agent --no-tty -d - 2> /dev/null`
        printf "%s\n" "${decryptedText}" | "${thisFile}" --gk # import any keys, then decrypt again to write status file
        printf "%s\n" "${encryptedText}" | gpg --homedir "${keydir}" --use-agent --no-tty --status-file "${temp}" -d - > /dev/null 2> /dev/null
      elif test "${filetype}" = "tar"
      then
        decryptedText=`printf "%s\n" "${encryptedText}" | gpg --homedir "${keydir}" --use-agent --no-tty --status-file "${temp}" -d - 2> /dev/null | tar -t`
        decryptedText=`printf "tar archive that contains the following:\n-------\n${decryptedText}"`
      else # need to try decryption even if certain to fail in order to get error into status file
        printf "%s\n" "${encryptedText}" | gpg --homedir "${keydir}" --use-agent --no-tty --status-file "${temp}" -d - > /dev/null 2> /dev/null
        decryptedText="Data of type ${filetype}"
      fi
      msginfo=`cat "${temp}"`
      rm "${temp}"
      nopgp=`printf "%s\n" "${msginfo}" | grep -o "NODATA"`
      noseckey=`printf "%s\n" "${msginfo}" | grep -o "NO_SECKEY"`
      plaintext=`printf "%s\n" "${msginfo}" | grep -o "PLAINTEXT"`
      badpassphrase=`printf "%s\n" "${msginfo}" | grep -o "BAD_PASSPHRASE"`
      if test -n "${nopgp}"
      then
        printf "This is not really a PGP message.\n---------------------------------\n\n%s\n" "${encryptedText}" | zenity --text-info --font="monospace" --title="Not a PGP Message" --filename=/dev/stdin --ok-label="Close" --cancel-label="Close" --width=620 --height=620 &
        break
      elif test -n "${badpassphrase}"
      then
        zenity --info --width=400 --text="At least one bad passphrase was given.\nTry again." --title="Bad Password" &
        exit 0
      elif test -n "${noseckey}" && test -z "${plaintext}"
      then
        if test -n "${2}"
        then
          printf "%s\n" "${encryptedText}" | "${thisFile}" --so "$(basename "${2}") - Text encrypted for someone else" &
        else
          printf "%s\n" "${encryptedText}" | "${thisFile}" --so "Text encrypted for someone else" &
        fi
        break
      elif test -z "${plaintext}"
      then
        zenity --info --width=400 --text="Message processing failed." --title="Processing Failed" &
        break
      fi
      filename=`printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*PLAINTEXT " | sed "s/^\[GNUPG:\][\t ]*PLAINTEXT [A-F0-9]* [0-9]* //"`
      if test "${filename}" = "-" || test -z "${filename}"
      then
        filename=""
      elif test "${filetype}" != "tar" &&  test "${filetype}" != "text"
      then
        decryptedText=`printf "%s\n(a file of type %s)" "${filename}" "${filetype}"`
      fi
      sigtime=`printf "%s\n" "${msginfo}" | grep "SIG_ID" | grep -o "[0-9T]*$"`
      if printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*BEGIN_DECRYPTION" > /dev/null
      then
        encrypted="yes"
        beginline=`printf "%s\n" "${msginfo}" | grep -n "BEGIN_DECRYPTION" | grep -o "^[^:]*"`
        recipientID=`printf "%s\n" "${msginfo}" | head -n "${beginline}" | grep "KEY_CONSIDERED" | tail -n 1 | grep -o "[A-F0-9]\{40\}" | head -n 1 | grep -o "[A-F0-9]\{16\}$"`
        if test -z "${recipientID}"
        then
          recipientID=`printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*USERID_HINT [A-F0-9]\+" | tail -n 1 | grep -o "[A-F0-9]\{16\}" | head -n 1`
        fi
      else
        encrypted=""
        recipientID=""
      fi
      if printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*GOODSIG" > /dev/null
      then
        signer=`printf "%s\n" "${msginfo}" | grep -o "^\[GNUPG:\][\t ]*GOODSIG [A-F0-9]\+" | grep -o "[A-F0-9]\{16\}"`
        if test -n "${signer}"
        then
          signer=`gpg --homedir "${keydir}" --with-colons --fixed-list-mode -k "${signer}" | grep "^uid" | head -n 1 | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*" | sed "s/[\]x3a/:/ig"`" ${signer}"
        fi
        if test -z "${signer}"
        then
          signer="*****Forged signature*****"
        fi
      elif printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*BADSIG" > /dev/null
      then
        signer=`printf "%s\n" "${msginfo}" | grep -o "^\[GNUPG:\][\t ]*BADSIG [A-F0-9]\+" | grep -o "[A-F0-9]\{16\}"`
        signer=`gpg --homedir "${keydir}" --with-colons --fixed-list-mode -k "${signer}" | grep "^uid" | head -n 1 | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*" | sed "s/[\]x3a/:/ig"`" ${signer}"
        signer=`printf "%s\n%s\n" "${signer}" "*****This is not a valid signature.*****"`
      elif printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*EXPKEYSIG" > /dev/null
      then
        signer=`printf "%s\n" "${msginfo}" | grep -o "^\[GNUPG:\][\t ]*EXPKEYSIG [A-F0-9]\+" | grep -o "[A-F0-9]\{16\}"`
        signer=`gpg --homedir "${keydir}" --with-colons --fixed-list-mode -k "${signer}" | grep "^uid" | head -n 1 | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*" | sed "s/[\]x3a/:/ig"`" ${signer}"
        signer=`printf "%s\n%s\n" "${signer}" "*****This key has expired.*****"`
      elif printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*REVKEYSIG" > /dev/null
      then
        signer=`printf "%s\n" "${msginfo}" | grep -o "^\[GNUPG:\][\t ]*REVKEYSIG [A-F0-9]\+" | grep -o "[A-F0-9]\{16\}"`
        signer=`gpg --homedir "${keydir}" --with-colons --fixed-list-mode -k "${signer}" | grep "^uid" | head -n 1 | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*" | sed "s/[\]x3a/:/ig"`" ${signer}"
        signer=`printf "%s\n%s\n" "${signer}" "*****This key has been revoked*****"`
      elif printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*ERRSIG" > /dev/null
      then
        signer="(apparently) "`printf "%s\n" "${msginfo}" | grep -o "^\[GNUPG:\][\t ]*ERRSIG [A-F0-9]\+" | grep -o "[A-F0-9]\{16\}"`
        signer=`printf "%s\n%s\n" "${signer}" "*****To verify this signature, you must import the public key of the signer.*****"`
        sigtime=`printf "%s\n" "${msginfo}" | grep -o "ERRSIG.*$" | grep -o "[0-9T]* [0-9]*$" | grep -o "^[0-9T]*"`
      elif test "${filetype}" = "text" && printf "%s\n" "${decryptedText}" | head -n 1 | grep "^MAC:" > /dev/null # no sig status messages, so unsigned
      then
        msgmac=`printf "%s\n" "${decryptedText}" | head -n 1 | sed "s/^MAC://"`
        decryptedText=`printf "%s\n" "${decryptedText}" | tail -n +2`
        signer="*****unauthenticated*****"
        while true
        do
          thesecret=`zenity --entry --title="Deniable authentication secret" --text="Enter the shared secret for deniable authentication."`
          if test -n "${thesecret}"
          then
            themac=`printf "%s\n%s\n" "${thesecret}" "${decryptedText}" | sha512sum | grep -o "^[^ ]*"`
            if test "${msgmac}" = "${themac}"
            then
              signer="\"${thesecret}\""
              break
            else
              if ! zenity --question --width=400 --title="Try again?" --text="Authentication with \"${thesecret}\" failed. Try again?"
              then
                break
              fi
            fi
          else
            break
          fi
        done
      elif test -z "${encrypted}"
      then
        signer="" # neither signed nor encrypted, so fake message
      else
        signer="*****unsigned*****"
      fi
      if test -n "${sigtime}"
      then
        if printf "%s\n" "${sigtime}" | grep "T" > /dev/null
        then
          sigtime=`date "+%c" -d "${sigtime}"`
        else
          sigtime=`date "+%c" -d @${sigtime}`
        fi
      fi
      if test -n "${recipientID}"
      then
        recipientID=`gpg --homedir "${keydir}" --with-colons --fixed-list-mode -K "${recipientID}" | grep -o "sec:[^:]*:[^:]*:[^:]*:[^:]*" | grep -o "[^:]*$"`
        recipientID=`gpg --homedir "${keydir}" --with-colons --fixed-list-mode -K "${recipientID}" | grep "^uid" | head -n 1 | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*" | sed "s/[\]x3a/:/ig"`" ${recipientID}"
      fi
      encLength=`printf "%s\n" "${encryptedText}" | wc -l`
      if test "${filetype}" = "text" && test -n "${2}" && test -z "${filename}" && test "${signer}" = "${recipientID}" && test -n "${encrypted}"
      then
        printf "%s\n" "${decryptedText}" | "${thisFile}" --gm &
        if test "${encLength}" = `cat "${2}" | wc -l`
        then
          printf "%s\n" "${decryptedText}" | "${thisFile}" --ed "${2}" "${signer}" &
        else
          printf "%s\n" "${decryptedText}" | "${thisFile}" --ed "${defaultdir}/$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10).asc" "${signer}" &
        fi
      else
        if test -z "${recipientID}"
        then
          thesig=`printf "From: %s\n%s" "${signer}" "${sigtime}"`
        else
          thesig=`printf "From: %s\nTo:   %s\n%s" "${signer}" "${recipientID}" "${sigtime}"`
        fi
        if test -n "${2}"
        then
          windowtitle=`basename "${2}"`" - "
        else
          windowtitle=""
        fi
        if test -n "${encrypted}"
        then
          windowtitle="${windowtitle}Message ${messageNumber} - Encrypted ${filetype}"
        elif test -n "${signer}"
        then
          windowtitle="${windowtitle}Message ${messageNumber} - Signed ${filetype}"
        else
          windowtitle="${windowtitle}Message ${messageNumber} - Forged Message"
          thesig="*****Forged message*****"
        fi
        if test "${filetype}" = "text"
        then
          if test -n "${encrypted}" && test -n "${recipientID}"
          then
            justname=`printf "%s\n" "${recipientID}" | sed "s/ [^ ]*$//" | grep -o "^[^<]*"`
            okbtnlabel="Save Encrypted for ${justname}"
          elif (test -n "${encrypted}" && test -z "${recipientID}") || (test -z "${encrypted}" && test -z "${sigtime}")
          then
            okbtnlabel="Save"
          elif printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*ERRSIG" > /dev/null
          then
            justname=`printf "%s\n" "${signer}" | grep -o "[A-F0-9]*$" | head -n 1`
            okbtnlabel="Save Signed by ${justname}"
          else
            justname=`printf "%s\n" "${signer}" | sed "s/ [^ ]*$//" | grep -o "^[^<]*" | head -n 1`
            okbtnlabel="Save Signed by ${justname}"
          fi
          printf "%s\n%s\n" "${encryptedText}" "${decryptedText}" | "${thisFile}" --se "${encLength}" "${thesig}" "${windowtitle}" "${okbtnlabel}" &
          printf "%s\n" "${decryptedText}" | "${thisFile}" --gm &
        else
          printf "%s\n%s\n" "${encryptedText}" "${decryptedText}" | "${thisFile}" --sd "${encLength}" "${thesig}" "${filename}" "${windowtitle}" "${filetype}" &
        fi
      fi
      break
    done
  done
# ==================
elif test "${1}" = "--so" # save text encrypted for someone else; ${2} is window title
  then
  theText=`cat /dev/stdin`
  if ! printf "%s\n" "${theText}" | zenity --text-info --font="monospace" --title="${2}" --filename=/dev/stdin --ok-label="Save Encrypted for Someone Else" --cancel-label="Close" --width=620 --height=620
  then
    exit 0
  fi
  filename="${defaultdir}/"`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10`".asc"
  savepath=`zenity --file-selection --save --confirm-overwrite --filename="${filename}" --title="Save encrypted text..."`
  if test -n "${savepath}"
  then
    printf "%s\n" "${theText}" > "${savepath}"
  fi
# ==================
elif test "${1}" = "--ed" # edit encrypted text file; ${2} is filename; ${3} is sender and recipient
  then
  nameandaddress=`printf "%s\n" "${3}" | sed "s/ [A-F0-9]\{16\}$//"`
  justname=`printf "%s\n" "${nameandaddress}" | sed "s/ <[^>]*>$//"`
  theText=`zenity --text-info --editable --font="monospace" --title="$(basename "${2}") - editable - encrypted for ${nameandaddress}" --ok-label="Save Encrypted for ${justname}" --cancel-label="Close" --width=620 --height=620`
  if test -z "${theText}"
  then
    exit 0
  fi
  savepath=`zenity --file-selection --save --confirm-overwrite --filename="${2}" --title="Encrypt for ${3}..."`
  senderID=`printf "%s\n" "${3}" | grep -o "[A-F0-9]\{16\}$"`
  if ! "${thisFile}" --ps | grep "${senderID}" > /dev/null
  then
    senderID=`"${thisFile}" --ps | head -n 1`
  fi
  if test -n "${savepath}"
  then
    printf "%s\n" "${theText}" | gpg --homedir "${keydir}" -a --trust-model always --textmode -s -u "${senderID}" -e -R "${senderID}" --no-emit-version --no-encrypt-to --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty - > "${savepath}"
  fi
# ==================
elif test "${1}" = "--se" # save encrypted or signed text; read encrypted and decrypted text from stdin; ${2} is length of encrypted part
  then
  allText=`cat /dev/stdin`
  encLength="${2}"
  encryptedText=`printf "%s\n" "${allText}" | head -n "${encLength}"`
  encLength=`expr ${encLength} + 1`
  decryptedText=`printf "%s\n" "${allText}" | tail -n "+${encLength}"`
  thesig="${3}"
  windowtitle="${4}"
  okbtnlabel="${5}"
  if printf "%s\n-----------------\n%s\n" "${thesig}" "${decryptedText}" | zenity --text-info --font="monospace" --title="${windowtitle}" --filename=/dev/stdin --ok-label="${okbtnlabel}" --cancel-label="Close" --width=620 --height=620
  then
    filename="${defaultdir}/"`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10`".asc"
    savepath=`zenity --file-selection --save --confirm-overwrite --filename="${filename}" --title="Save as..."`
    if test -n "${savepath}"
    then
      printf "%s\n" "${encryptedText}" > "${savepath}"
    fi
  fi
# ==================
elif test "${1}" = "--sd" # save data from a message; read encrypted and decrypted text from stdin; ${2} is length of encrypted part
then
  allText=`cat /dev/stdin`
  encLength="${2}"
  encryptedText=`printf "%s\n" "${allText}" | head -n "${encLength}"`
  encLength=`expr ${encLength} + 1`
  decryptedText=`printf "%s\n" "${allText}" | tail -n "+${encLength}"`
  thesig="${3}"
  filename="${4}"
  windowtitle="${5}"
  filetype="${6}"
  if test "${filetype}" = "tar"
  then
    okbtnlabel="Save tar archive contents in new folder"
  elif test -n "${filename}"
  then
    okbtnlabel="Save ${filename}"
  else
    okbtnlabel="Save as a .${filetype} file"
  fi
  if printf "%s\n-----------------\n%s\n" "${thesig}" "${decryptedText}" | zenity --text-info --font="monospace" --title="${windowtitle}" --filename=/dev/stdin --ok-label="${okbtnlabel}" --cancel-label="Close" --width=620 --height=620
  then
    if test "${filetype}" = "tar"
    then
      filename="decrypted-$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 7)"
      filename="${defaultdir}/${filename}"
      savepath=`zenity --file-selection --save --confirm-overwrite --filename="${filename}" --title="Save new folder as..."`
      if test -n "${savepath}"
      then
        rm -rf "${savepath}"
        mkdir "${savepath}"
        cd "${savepath}"
        (printf "%s\n" "${encryptedText}" | gpg --homedir "${keydir}" --use-agent --no-tty -d - 2> /dev/null | tar -x) | zenity --progress --text="Decrypting..." --pulsate --auto-close --no-cancel
      fi
    else
      if test -z "${filename}"
      then
        filename=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10`".${filetype}"
      fi
      filename="${defaultdir}/${filename}"
      savepath=`zenity --file-selection --save --confirm-overwrite --filename="${filename}" --title="Save as..."`
      if test -n "${savepath}"
      then
        printf "%s\n" "${encryptedText}" | gpg --homedir "${keydir}" --yes -d -o "${savepath}" - 2> /dev/null
      fi
    fi
  fi
# ==================
elif test "${1}" = "--rm" # Read copied text
then
  copiedText=`xclip -o -selection clipboard 2> /dev/null`
  if test -z "${copiedText}"
  then
    zenity --info --width=400 --text="No text was copied." &
    exit 0
  fi
  printf "%s\n" "${copiedText}" | "${thisFile}" --rs "copied text"
# ==================
elif test "${1}" = "--mg" # parse gopher menu
then
  output="$(printf "Gopher menu\n===========\nRead gopher:// URLs with \"Read a file from the Internet.\"\n===========")"
  while read item
  do
    item="$(printf "%s\n" "${item}" | tr -d "\r")"
    if test "${item}" = "."
    then
      break
    fi
    gtype="$(printf "%s\n" "${item}" | head -c 1)"
    item="$(printf "%s\n" "${item}" | tail -c +2)"
    gtitle="$(printf "%s\n" "${item}" | grep -o "^[[:print:]]*")"
    if test "${gtype}" = "i"
    then
      if test -n "${gtitle}"
      then
        output="$(printf "%s\n%s\n" "${output}" "${gtitle}")"
      else
        output="$(printf "%s\n \n" "${output}")"
      fi
      continue
    fi
    item="$(printf "%s\n" "${item}" | sed "s/^[[:print:]]*\t//")"
    gselector="$(printf "%s\n" "${item}" | grep -o "^[[:print:]]*")"
    item="$(printf "%s\n" "${item}" | sed "s/^[[:print:]]*\t//")"
    gdomain="$(printf "%s\n" "${item}" | grep -o "^[[:print:]]*")"
    item="$(printf "%s\n" "${item}" | sed "s/^[[:print:]]*\t//")"
    gport="$(printf "%s\n" "${item}" | grep -o "^[0-9]*")"
    if test "${gport}" = "70"
    then
      output="$(printf "%s\n=>%s\ngopher://%s/%s%s" "${output}" "${gtitle}" "${gdomain}" "${gtype}" "${gselector}")"
    else
      output="$(printf "%s\n=>%s\ngopher://%s:%s/%s%s" "${output}" "${gtitle}" "${gdomain}" "${gport}" "${gtype}" "${gselector}")"
    fi
  done
  printf "%s\n" "${output}" | "${thisFile}" --wt "${2} - Gopher menu" &
# ==================
elif test "${1}" = "--ru" # Read a file from the Internet
then
  theURL="${2}"
  if test -e "${theURL}"
  then
    exit 0
  fi
  if test -z "${theURL}"
  then
    copiedText=`xclip -o -selection clipboard 2> /dev/null | sed "s/^[ \t]*//"`
    theURL="$(printf "%s\n" "${copiedText}" | grep -m 1 -o "[a-zA-Z0-9]\+://[a-zA-Z0-9\.\-]\+\.[a-zA-Z0-9]\+[^ ]*")"
    theURL=`zenity --entry --text="Enter the URL to read." --title="URL to Read" --entry-text="${theURL}" --width=300 --ok-label="Read" | sed "s/^[ \t]*//"`
    if test -z "${theURL}"
    then
      exit 0
    fi
  fi
  theProtocol=`printf "%s\n" "${theURL}" | grep -o "^.*://" | tr "A-Z" "a-z"`
  theDomain=`printf "%s\n" "${theURL}" | sed "s/^.*:\/\///" | grep -o "^[^/]*"`
  theSpec=`printf "%s\n" "${theURL}" | sed "s/^.*:\/\///" | sed "s/^[^/]*//"`
  notlocal=`printf "${theDomain}" | grep -v "^127\.\|^10\.\|^192\.\|^172\.\|^localhost"`
  title="$(printf "%s\n" "${theSpec}" | grep -o "[^/]*$")"
  if test "${theProtocol}" = "minus://"
  then
    if test -z "${theSpec}" || test "${theSpec}" = "/"
    then
      theURL="gopher://${theDomain}:1990/9/index.minus"
    else
      theURL="gopher://${theDomain}:1990/9${theSpec}"
    fi
  fi
  if test -z "${title}"
  then
    title="${theDomain}"
  fi
  if ! test -n "${gotinet}" && test -n "${notlocal}"
  then
    zenity --info --width=400 --text="There is no Internet connection." &
    exit 0
  fi
  theTLD=`printf "%s\n" "${theDomain}" | grep -o "[^\.]*$"`
  if test "${theTLD}" = "onion" && (test -z "${torproxy}" || ! which "curl" > /dev/null) && ! test -x "${tailstest}"
  then
    zenity --info --width=400 --text="\"${title}\" could not be downloaded because Tor is not running and/or curl is not installed." &
    exit 0
  fi
  if test "${theTLD}" = "i2p"
  then
    if (test -z "${theProtocol}" || test "${theProtocol}" = "http://") && lsof -ni | grep "TCP 127.0.0.1:4444 (LISTEN)" > /dev/null
    then
      dlcom="wget -q -T 20 -t 4 -e use_proxy=yes -e http_proxy=127.0.0.1:4444 -O"
    elif test "${theProtocol}" = "https://" && lsof -ni | grep "TCP 127.0.0.1:4445 (LISTEN)" > /dev/null
    then
      dlcom="wget -q -T 20 -t 4 -e use_proxy=yes -e http_proxy=127.0.0.1:4445 -O"
    else
      zenity --info --width=400 --text="\"${title}\" could not be downloaded because I2P is not running or the protocol is neither HTTP nor HTTPS." &
      exit 0
    fi
  elif which "curl" > /dev/null
  then
    if test -n "${torproxy}" && test -n "${notlocal}"
    then
      dlcom="curl -s -L --socks5-hostname "${torproxy}" -o"
    else
      dlcom="curl -s -L -o"
    fi
  else
    dlcom="wget -q -T 20 -t 4 -O"
  fi
  theText=`${dlcom} - "${theURL}" | grep -I ""`
  if test -n "${theText}"
  then
    if test "${theProtocol}" = "gopher://" && test -n "$(printf "%s\n" "${theText}" | grep "^[[:print:]]*[[:cntrl:]][[:print:]]*[[:cntrl:]][[:print:]]*[[:cntrl:]][0-9]*")"
    then
      printf "%s\n" "${theText}" | "${thisFile}" --mg "${title}" &
    else
      printf "%s\n" "${theText}" | "${thisFile}" --rs "${title}"
    fi
    exit 0
  fi
  if ! zenity --question --width=400 --title="Try to save as file?" --text="\"${title}\" could not be downloaded, or it is not text.\nTry to download and save it as a file?" --ok-label="Download and Save" --cancel-label="Cancel"
  then
    exit 0
  fi
  title="$(printf "%s\n" "${theSpec}" | grep -o "[^/]*$")"
  savepath=`zenity --file-selection --save --confirm-overwrite --filename="${defaultdir}/${title}" --title="Save as..."`
  if test -n "${savepath}"
  then
    ${dlcom} "${savepath}" "${theURL}" | zenity --progress --text="Dowloading and saving..." --pulsate --auto-close --no-cancel
    if ! test -f "${savepath}"
    then
      zenity --info --width=400 --text="\"$(basename "${savepath}")\" was not successfully downloaded." &
    fi
  fi
# ==================
elif test "${1}" = "--rs" # Read text from stdin
then
  copiedText=`cat /dev/stdin | grep -I ""`
  if test -z "${copiedText}"
  then
    zenity --info --width=400 --text="There is no data from stdin, or it is not text." &
    exit 0
  fi
  if ! printf "%s\n" "${copiedText}" | grep "\-\-\-\-\-BEGIN PGP [^ ]* *MESSAGE\-\-\-\-\-" > /dev/null && ! printf "%s\n" "${copiedText}" | grep "\-\-\-\-\-BEGIN PGP [A-Z]* KEY BLOCK\-\-\-\-\-" > /dev/null
  then
    if test -n "${2}"
    then
      title="${2} - Unencrypted text"
    else
      title="Unencrypted text"
    fi
    printf "%s\n" "${copiedText}" | "${thisFile}" --wt "${title}" &
    exit 0
  fi
  printf "%s\n" "${copiedText}" | "${thisFile}" --gk
  printf "%s\n" "${copiedText}" | "${thisFile}" --gm &
# ==================
elif test "${1}" = "--wf" # Write new encrypted file
then
  "${thisFile}" --wt
# ==================
elif test "${1}" = "--wt" # If ${2} is missing or "", write new encrypted file, otherwise read or encrypt text from stdin
then
  senderKeys=`"${thisFile}" --ps`
  totalkeys=`printf "%s\n" "${senderKeys}" | wc -l`
  if test -z "${2}"
  then
    if test "${totalkeys}" -lt 2
    then
      zenity --info --width=400 --text="You have no personal key pair.\nYou can't encrypt for yourself without one." &
      exit 0
    fi
    theText=`zenity --text-info --editable --font="monospace" --title="New encrypted file" --ok-label="Save Encrypted for Me" --width=620 --height=620`
  else
    if test "${totalkeys}" -lt 2
    then
      zenity --text-info --font="monospace" --title="${2}" --filename=/dev/stdin --cancel-label="Close" --ok-label="Close" --width=720 --height=620
      exit 0
    else
      theText=`zenity --text-info --editable --font="monospace" --title="${2} - editable" --filename=/dev/stdin --cancel-label="Close" --ok-label="Save Encrypted for Me" --width=720 --height=620`
    fi
  fi
  if test -z "${theText}"
  then
    exit 0
  fi
  if test "${totalkeys}" -eq 2
  then
    senderID=`printf "%s\n" "${senderKeys}" | head -n 1`
  else
    senderID=`printf "%s\n" "${senderKeys}" | zenity --list --title="Select Key" --text="Select key to encrypt with." --width=600 --height=401 --column="Key ID                  " --column="Name" 2> /dev/null | grep -o "^[^|]*"`
  fi
  if test -z "${senderID}"
  then
    exit 0
  fi
  filename="${defaultdir}/"`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10`".asc"
  savepath=`zenity --file-selection --save --confirm-overwrite --filename="${filename}" --title="Save as..."`
  if test -n "${savepath}"
  then
    printf "%s\n" "${theText}" | gpg --homedir "${keydir}" -a --trust-model always --textmode -s -u "${senderID}" -e -R "${senderID}" --no-emit-version --no-encrypt-to --personal-digest-preferences "SHA512 SHA384 SHA256" --personal-compress-preferences "ZLIB BZIP2 ZIP" --personal-cipher-preferences "AES256 TWOFISH CAMELLIA256 AES192 AES" --use-agent --no-tty - > "${savepath}"
  fi
# ==================
elif test "${1}" = "--rf" # Read files
then
  shift
  if test $# -gt 0
  then
    linkname="${1}"
    shift
    if test $# -gt 0
    then
      "${thisFile}" --rf "$@" &
    fi
    if ! printf "%s\n" "${linkname}" | grep "^-" > /dev/null
    then
      filename=`readlink -e "${linkname}"`
    else
      filename=""
    fi
    if test -z "${filename}"
    then
      filename="${linkname}"
    fi
    if test -d "${filename}"
    then
      if test "${filename}" = "${ramdiskpath}"
      then
        "${thisFile}" --sf "${filename}" &
      elif zenity --question --width=400 --title="Save \"$(basename "${filename}")\" as an encrypted tar archive?" --text="\"$(basename "${filename}")\" is a folder. Save it as an encrypted tar archive?" --ok-label="Encrypt" --cancel-label="Cancel"
      then
        "${thisFile}" --sx "${filename}" &
      fi
      exit 0
    fi
    if ! test -f "${filename}"
    then
      zenity --info --width=400 --title="${filename}" --text="\"${filename}\" does not exist." &
      exit 0
    fi
  else
    filename="${defaultdir}"
    filename=`zenity --file-selection --filename="${filename}/" --title "Select a file to read"`
    if test -z "${filename}"
    then
      exit 0
    fi
  fi # if the file is text, encrypt or read
  filetype=`file -b -i "${filename}"`
  if printf "%s\n" "${filetype}" | grep -v "charset=binary$" > /dev/null
  then
    copiedText=`cat "${filename}"`
    if ! printf "%s\n" "${copiedText}" | grep "\-\-\-\-\-BEGIN PGP [^ ]* *MESSAGE\-\-\-\-\-" > /dev/null && ! printf "%s\n" "${copiedText}" | grep "\-\-\-\-\-BEGIN PGP [A-Z]* KEY BLOCK\-\-\-\-\-" > /dev/null
    then
      if zenity --question --width=400 --title="Encrypt or Read?" --text="\"$(basename "${filename}")\" is a text file.\nDo you want to read it or save it as an encrypted tar archive?" --ok-label="Read" --cancel-label="Encrypt"
      then
        printf "%s\n" "${copiedText}" | "${thisFile}" --wt "$(basename "${filename}") - Unencrypted text" &
      else
        "${thisFile}" --sx "${filename}" &
      fi
      exit 0
    fi
    printf "%s\n" "${copiedText}" | "${thisFile}" --gk
    printf "%s\n" "${copiedText}" | "${thisFile}" --gm "${filename}" &
    exit 0
  fi # not text, so try to import as key
  temp="${tempfiledir}/.$(cat /dev/urandom | tr -dc "0-9a-z" | head -c 10)"
  gpg --homedir "${keydir}" --import --import-options import-minimal --status-file "${temp}" "${filename}" 2> /dev/null
  importinfo=`cat "${temp}"`
  rm -f "${temp}"
  if ! printf "%s\n" "${importinfo}" | grep -o "NODATA" > /dev/null
  then
    importedkeys=`printf "%s\n" "${importinfo}" | grep -o "IMPORT_OK .*$" | sed "s/IMPORT_OK //" | tr " " "_" | tr "\n" " "`
    if test -n "${importedkeys}"
    then
      printf "%s" "${importedkeys}" | "${thisFile}" --li &
      exit 0
    fi
  fi # not key, so try to read as message
  filetype=`gpg --homedir "${keydir}" --use-agent --no-tty -d -o - "${filename}" 2> /dev/null | file -b -i -`
  if ! printf "%s\n" "${filetype}" | grep "charset=.*$" > /dev/null
  then
    filetype=""
  elif printf "%s\n" "${filetype}" | grep -v "charset=binary$" > /dev/null
  then
    filetype="text"
  else
    filetype=`printf "%s\n" "${filetype}" | grep -o "^[^;]*" | grep -o "[^/]*$" | sed "s/x-//"`
  fi
  temp="${tempfiledir}/.$(cat /dev/urandom | tr -dc "0-9a-z" | head -c 10)"
  if test "${filetype}" = "text"
  then
    decryptedText=`gpg --homedir "${keydir}" --use-agent --no-tty --status-file "${temp}" -d -o - "${filename}" 2> /dev/null`
  else
    if test "${filetype}" = "tar"
    then
      decryptedText=`gpg --homedir "${keydir}" --use-agent --no-tty --status-file "${temp}" -d "${filename}" 2> /dev/null | tar -t`
      decryptedText=`printf "tar archive that contains the following:\n-------\n${decryptedText}"`
    else
      gpg --homedir "${keydir}" --use-agent --no-tty --status-file "${temp}" -d -o - "${filename}" > /dev/null 2> /dev/null
      decryptedText="Data of type ${filetype}"
    fi
  fi
  msginfo=`cat "${temp}"`
  rm -f "${temp}"
  if test -z "${msginfo}"
  then
    nopgp="NODATA"
  else
    nopgp=`printf "%s\n" "${msginfo}" | grep -o "NODATA"`
  fi
  if test -n "${nopgp}"
  then
    if zenity --question --width=400 --title="Save \"$(basename "${filename}")\" as an encrypted tar archive?" --text="\"$(basename "${filename}")\" is not a PGP key or message. Save it as an encrypted tar archive?" --ok-label="Encrypt" --cancel-label="Cancel"
    then
      "${thisFile}" --sx "${filename}" &
    fi
    exit 0
  fi
  noseckey=`printf "%s\n" "${msginfo}" | grep -o "NO_SECKEY"`
  plaintext=`printf "%s\n" "${msginfo}" | grep -o "PLAINTEXT"`
  badpassphrase=`printf "%s\n" "${msginfo}" | grep -o "BAD_PASSPHRASE"`
  if test -n "${badpassphrase}"
  then
    zenity --info --width=400 --title="${filename}" --text="At least one bad passphrase was given.\nTry again." &
    exit 0
  elif test -n "${noseckey}" && test -z "${plaintext}"
  then
    zenity --info --width=400 --title="${filename}" --text="\"$(basename "${filename}")\" was encrypted for someone else." &
    exit 0
  elif test -z "${plaintext}"
  then
    zenity --info --width=400 --title="${filename}" --text="Message processing failed." &
    exit 0
  fi
  if printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*BEGIN_DECRYPTION" > /dev/null
  then
    windowtitle="$(basename "${filename}"): Encrypted ${filetype}"
  else
    windowtitle="$(basename "${filename}"): Signed ${filetype}"
  fi
  originalfilename=`printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*PLAINTEXT [A-F0-9]* [0-9]* .*$" | sed "s/^\[GNUPG:\][\t ]*PLAINTEXT [A-F0-9]* [0-9]* //"`
  if test "${originalfilename}" = "-" || test -z "${originalfilename}"
  then
    originalfilename=""
  elif test "${filetype}" != "tar" &&  test "${filetype}" != "text"
  then
    decryptedText=`printf "%s\n(a file of type %s)" "${originalfilename}" "${filetype}"`
  fi
  sigtime=`printf "%s\n" "${msginfo}" | grep "SIG_ID" | grep -o "[0-9T]*$"`
  if printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*ERRSIG" > /dev/null
  then
    signer="(apparently) "`printf "%s\n" "${msginfo}" | grep -o "^\[GNUPG:\][\t ]*ERRSIG [A-F0-9]\+" | grep -o "[A-F0-9]\{16\}"`
    signer=`printf "%s\n%s\n" "${signer}" "*****To verify this signature, you must import the public key of the signer.*****"`
    sigtime=`printf "%s\n" "${msginfo}" | grep -o "ERRSIG.*$" | grep -o "[0-9T]* [0-9]*$" | grep -o "^[0-9T]*"`
  elif printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*GOODSIG" > /dev/null
  then
    signer=`printf "%s\n" "${msginfo}" | grep -o "^\[GNUPG:\][\t ]*GOODSIG.*$" | sed "s/^\[GNUPG:\][\t ]*GOODSIG [^ ]* //"`
    signer="${signer} "`printf "%s\n" "${msginfo}" | grep -o "GOODSIG [A-F0-9]\+" | grep -o "[A-F0-9]\{16\}"`
  elif printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*EXPKEYSIG" > /dev/null
  then
    signer=`printf "%s\n" "${msginfo}" | grep -o "^\[GNUPG:\][\t ]*EXPKEYSIG.*$" | sed "s/^\[GNUPG:\][\t ]*EXPKEYSIG [^ ]* //"`
    signer="${signer} "`printf "%s\n" "${msginfo}" | grep -o "EXPKEYSIG [A-F0-9]\+" | grep -o "[A-F0-9]\{16\}"`
    signer=`printf "%s\n%s\n" "${signer}" "*****This key has expired.*****"`
  elif printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*REVKEYSIG" > /dev/null
  then
    signer=`printf "%s\n" "${msginfo}" | grep -o "^\[GNUPG:\][\t ]*REVKEYSIG.*$" | sed "s/^\[GNUPG:\][\t ]*REVKEYSIG [^ ]* //"`
    signer="${signer} "`printf "%s\n" "${msginfo}" | grep -o "REVKEYSIG [A-F0-9]\+" | grep -o "[A-F0-9]\{16\}"`
    signer=`printf "%s\n%s\n" "${signer}" "*****This key has been revoked*****"`
  elif printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*BADSIG" > /dev/null
  then
    signer=`printf "%s\n" "${msginfo}" | grep -o "^\[GNUPG:\][\t ]*BADSIG.*$" | sed "s/^\[GNUPG:\][\t ]*BADSIG [^ ]* //"`
    signer="${signer} "`printf "%s\n" "${msginfo}" | grep -o "BADSIG [A-F0-9]\+" | grep -o "[A-F0-9]\{16\}"`
    signer=`printf "%s\n%s\n" "${signer}" "*****This is not a valid signature.*****"`
  fi
  if printf "%s\n" "${msginfo}" | grep -o "BEGIN_DECRYPTION" > /dev/null
  then
    beginline=`printf "%s\n" "${msginfo}" | grep -n "BEGIN_DECRYPTION" | grep -o "^[^:]*"`
    recipientID=`printf "%s\n" "${msginfo}" | head -n "${beginline}" | grep "KEY_CONSIDERED" | tail -n 1 | grep -o "[A-F0-9]\{40\}" | head -n 1 | grep -o "[A-F0-9]\{16\}$"`
    if test -z "${recipientID}"
    then
      recipientID=`printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*USERID_HINT [A-F0-9]\+" | tail -n 1 | grep -o "[A-F0-9]\{16\}" | head -n 1`
    fi
  else
    recipientID=""
  fi
  if test -n "${recipientID}"
  then
    recipientID=`gpg --homedir "${keydir}" --with-colons --fixed-list-mode -K "${recipientID}" | grep -o "sec:[^:]*:[^:]*:[^:]*:[^:]*" | grep -o "[^:]*$"`
    recipientID=`gpg --homedir "${keydir}" --with-colons --fixed-list-mode -K "${recipientID}" | grep "^uid" | head -n 1 | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*" | sed "s/[\]x3a/:/ig"`" ${recipientID}"
  fi
  if test -n "${sigtime}"
  then
    if printf "%s\n" "${sigtime}" | grep "T" > /dev/null
    then
      sigtime=`date "+%c" -d "${sigtime}"`
    else
      sigtime=`date "+%c" -d @${sigtime}`
    fi
  else
    signer="*****unsigned*****"
  fi
  if printf "%s\n" "${msginfo}" | grep "^\[GNUPG:\][\t ]*BEGIN_DECRYPTION" > /dev/null
  then
    if test -n "${recipientID}"
    then
      thesig=`printf "From: %s\nTo:   %s\n%s" "${signer}" "${recipientID}" "${sigtime}"`
    else
      thesig=`printf "From: %s\n%s" "${signer}" "${sigtime}"`
    fi
  elif test -n "${sigtime}"
  then
    thesig=`printf "From: %s\n%s" "${signer}" "${sigtime}"`
  else
    thesig="*****Forged message*****"
  fi
  if test "${filetype}" = "text"
  then
    if test -n "${originalfilename}"
    then
      printf "%s\n-----------------\nFilename: %s\n-----------------\n%s\n" "${thesig}" "${originalfilename}" "${decryptedText}" | zenity --text-info --font="monospace" --title="${windowtitle}" --filename=/dev/stdin --ok-label="Close" --cancel-label="Close" --width=620 --height=620 &
    else
      printf "%s\n-----------------\n%s\n" "${thesig}" "${decryptedText}" | zenity --text-info --font="monospace" --title="${windowtitle}" --filename=/dev/stdin --ok-label="Close" --cancel-label="Close" --width=620 --height=620 &
    fi
    printf "%s\n" "${decryptedText}" | "${thisFile}" --gk
    printf "%s\n" "${decryptedText}" | "${thisFile}" --gm "${originalfilename}" &
  else
    if test "${filetype}" = "tar"
    then
      okbtnlabel="Save tar archive contents in new folder"
    elif test -n "${originalfilename}"
    then
      okbtnlabel="Save ${originalfilename}"
    else
      okbtnlabel="Save as a .${filetype} file"
    fi
    if printf "%s\n-----------------\n%s\n" "${thesig}" "${decryptedText}" | zenity --text-info --font="monospace" --title="${windowtitle}" --filename=/dev/stdin --ok-label="${okbtnlabel}" --cancel-label="Close" --width=620 --height=620
    then
      if test "${filetype}" = "tar"
      then
        originalfilename="decrypted-$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 7)"
        originalfilename="${defaultdir}/${originalfilename}"
        savepath=`zenity --file-selection --save --confirm-overwrite --filename="${originalfilename}" --title="Save new folder as..."`
        if test -n "${savepath}"
        then
          rm -rf "${savepath}"
          mkdir "${savepath}"
          cd "${savepath}"
          (gpg --homedir "${keydir}" --use-agent --no-tty -d "${filename}" 2> /dev/null | tar -x) | zenity --progress --text="Decrypting..." --pulsate --auto-close --no-cancel
        fi
      else
        if test -z "${originalfilename}"
        then
          originalfilename=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10`".${filetype}"
        fi
        originalfilename="${defaultdir}/${originalfilename}"
        savepath=`zenity --file-selection --save --confirm-overwrite --filename="${originalfilename}" --title="Save as..."`
        if test -n "${savepath}"
        then
          gpg --homedir "${keydir}" --use-agent --yes --no-tty -d -o "${savepath}" "${filename}" 2> /dev/null
        fi
      fi
    fi
  fi
# ==================
elif test "${1}" = "--gk" # Import keys, in ASCII form, from stdin
then
  copiedText=`cat /dev/stdin | sed "s/^[\t ]*//" | tr -d "\r" | sed "s/\-\-\-\-\-BEGIN PGP [A-Z]* KEY BLOCK\-\-\-\-\-/\n&/" | sed "s/\-\-\-\-\-END PGP [A-Z]* KEY BLOCK\-\-\-\-\-/&\n/"`
  if test -z "${copiedText}"
  then
    exit 0
  fi
  importedkeys=""
  keynum="0"
  while true
  do
    keynum=`expr ${keynum} + 1`
    keyStart=`printf "%s\n" "${copiedText}" | grep -n "^\-\-\-\-\-BEGIN PGP [A-Z]* KEY BLOCK\-\-\-\-\-" | grep -o "^[^:]*" | tail -n +${keynum} | head -n 1`
    if test -z "${keyStart}"
    then
      break
    fi
    theKey=`printf "%s\n" "${copiedText}" | tail -n +${keyStart}`
    if ! printf "%s\n" "${theKey}" | grep "\-\-\-\-\-BEGIN PGP [A-Z]* KEY BLOCK\-\-\-\-\-$" > /dev/null
    then
      theKey=`printf "%s\n" "${theKey}" | sed "s/\-\-\-\-\-BEGIN PGP [A-Z]* KEY BLOCK\-\-\-\-\-/&\n/"`
    fi
    if ! printf "%s\n" "${theKey}" | grep "^\-\-\-\-\-END PGP [A-Z]* KEY BLOCK\-\-\-\-\-" > /dev/null
    then
      theKey=`printf "%s\n" "${theKey}" | sed "s/\-\-\-\-\-END PGP [A-Z]* KEY BLOCK\-\-\-\-\-/\n&/"`
    fi
    theKey=`printf "%s\n" "${theKey}" | fold -w 64`
    keylength=`printf "%s\n" "${theKey}" | grep -n "^\-\-\-\-\-END PGP [A-Z]* KEY BLOCK\-\-\-\-\-$" | grep -o "^[^:]*" | head -n 1`
    if test -n "${keylength}"
    then
      theKey=`printf "%s\n" "${theKey}" | head -n ${keylength}`
    else
      printf "This is not really a PGP key.\n---------------------------------\n\n%s\n" "${theKey}" | zenity --text-info --font="monospace" --title="Not a PGP Key" --filename=/dev/stdin --ok-label="Close" --cancel-label="Close" --width=620 --height=620 &
      break
    fi
    temp="${tempfiledir}/.$(cat /dev/urandom | tr -dc "0-9a-z" | head -c 10)"
    (printf "%s\n" "${theKey}" | gpg --homedir "${keydir}" --import --import-options import-minimal --status-file "${temp}" - 2> /dev/null) | zenity --progress --text="Importing keys..." --pulsate --auto-close --no-cancel
    importinfo=`cat "${temp}"`
    rm -f "${temp}"
    if printf "%s\n" "${importinfo}" | grep -o "NODATA" > /dev/null
    then
      printf "This is not really a PGP key.\n---------------------------------\n\n%s\n" "${theKey}" | zenity --text-info --font="monospace" --title="Not a PGP Key" --filename=/dev/stdin --ok-label="Close" --cancel-label="Close" --width=620 --height=620 &
      continue
    fi
    if ! printf "%s\n" "${importinfo}" | grep "^\[GNUPG:\][\t ]*IMPORT_OK" > /dev/null
    then
      printf "This is not really a PGP key.\n---------------------------------\n\n%s\n" "${theKey}" | zenity --text-info --font="monospace" --title="Not a PGP Key" --filename=/dev/stdin --ok-label="Close" --cancel-label="Close" --width=620 --height=620 &
    fi
    importedkeys=`printf "${importedkeys}%s\n" "$(printf "%s\n" "${importinfo}" | grep -o "IMPORT_OK .*$" | sed "s/IMPORT_OK //" | tr " " "_" | tr "\n" " ")"`
  done
  if test -z "${importedkeys}"
  then
    exit 0
  fi
  printf "%s" "${importedkeys}" | "${thisFile}" --li
# ==================
elif test "${1}" = "--li" # list imported keys
then
  newsecretkey=""
  importedkeys=`cat /dev/stdin`
  importedinfo=""
  for akey in ${importedkeys}
  do
    thecode=`printf "%s\n" "${akey}" | grep -o "^[^_]*"`
    thefingerprint=`printf "%s\n" "${akey}" | grep -o "[^_]*$"`
    thekeyid=`gpg --homedir "${keydir}" -k --with-colons --fixed-list-mode --with-fingerprint "${thefingerprint}" 2> /dev/null | grep "^uid" | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*" | head -n 1 | sed "s/[\]x3a/:/ig"`
    if test -z "${importedinfo}"
    then
      importedinfo="A key for ${thekeyid} was processed."
    else
      importedinfo="${importedinfo}\n----\nA key for ${thekeyid} was processed."
    fi
    importedinfo="${importedinfo}\nFingerprint: "`printf "${thefingerprint}\n" | fold -w 4 | tr "\n" " " | fold -w 10 | tr "\n" " " | sed "s/ *$//"`
    keyinfo=`gpg --homedir "${keydir}" -k --with-colons --fixed-list-mode --with-fingerprint "${thefingerprint}" 2> /dev/null`
    cipherbits=`printf "${keyinfo}" | grep -o "^pub:[^:]*:[^:]*" | grep -o "[^:]*$"`
    ciphertype=`printf "${keyinfo}" | grep -o "^pub:[^:]*:[^:]*:[^:]*" | grep -o "[^:]*$" | sed "s/16/Elgamal/" | sed "s/17/DSA/" | sed "s/18/ECC/" | sed "s/19/ECDSA/" | sed "s/1/RSA/" | sed "s/20/Elgamal/"`
    creationdate=`printf "${keyinfo}" | grep -o "^pub:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*" | grep -o "[^:]*$"`
    if printf "%s\n" "${creationdate}" | grep "T" > /dev/null
    then
      creationdate=`date -d "${creationdate}"`
    else
      creationdate=`date -d @${creationdate}`
    fi
    expirationdate=`printf "${keyinfo}" | grep -o "^pub:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*" | grep -o "[^:]*$"`
    if test -z "${expirationdate}"
    then
      expirationdate="never"
    elif printf "%s\n" "${expirationdate}" | grep "T" > /dev/null
    then
      expirationdate=`date -d "${expirationdate}"`
    else
      expirationdate=`date -d @${expirationdate}`
    fi
    capabilities=`printf "${keyinfo}" | grep -o "^pub:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*" | grep -o "[^:]*$" | tr -dc "ESCA" | sed "s/E/ encrypt/" | sed "s/S/ sign/" | sed "s/C/ certify/" | sed "s/A/ authenticate/"`
    importedinfo="${importedinfo}\n${cipherbits}-bit ${ciphertype}\nCreated: ${creationdate}\nExpires: ${expirationdate}\nThis key can:${capabilities}"
    if test ${thecode} -ge 16
    then
      importedinfo="${importedinfo}\nIt's a secret key"
    else
      importedinfo="${importedinfo}\nIt's a public key"
    fi
    if test ${thecode} -eq 0 || test ${thecode} -eq 16
    then
      importedinfo="${importedinfo} that was unchanged."
    elif test ${thecode} -eq 1 || test ${thecode} -eq 17
    then
      importedinfo="${importedinfo} that was added to the keyring."
    else
      importedinfo="${importedinfo} that was changed."
    fi
    if test ${thecode} -gt 16
    then
      newsecretkey="true"
    fi
  done
  gpg --homedir "${keydir}" -k --no-tty > /dev/null 2> /dev/null # causes trust database to be updated
  printf "${importedinfo}\n" | zenity  --text-info --font="monospace" --ok-label="Close" --cancel-label="Close" --title="Key Processed" --width=620 --height=620 &
  "${thisFile}" --bp
  if test -n "${newsecretkey}"
  then
    "${thisFile}" --bs
  fi
# ==================
elif test "${1}" = "--ck" ||  test "${1}" = "--pk" ||  test "${1}" = "--sk" # Copy my public key, print it to stdout, or save it to a text file
then
  senderKeys=`"${thisFile}" --ps`
  totalkeys=`printf "%s\n" "${senderKeys}" | wc -l`
  if test "${totalkeys}" -lt 2
  then
    zenity --info --width=400 --text="You have no personal key pair.\nYou'll need to make or import one before\nyou can send your public key to others." &
    exit 0
  elif test "${totalkeys}" -eq 2
  then
    senderID=`printf "%s\n" "${senderKeys}" | head -n 1`
  else
    senderID=`printf "%s\n" "${senderKeys}" | zenity --list --title="Select Key" --text="Select a public key." --width=600 --height=401 --column="Key ID                  " --column="Public key" 2> /dev/null | grep -o "^[^|]*"`
  fi
  if test -z "${senderID}"
  then
    exit 0
  fi
  if test "${1}" = "--ck"
  then
    gpg --homedir "${keydir}" --export -a --no-emit-version "${senderID}" | xclip -i -selection clipboard
    zenity --info --width=200 --text="Copied." &
  elif test "${1}" = "--pk"
  then
    gpg --homedir "${keydir}" --export -a --no-emit-version "${senderID}"
  else
    savepath="${defaultdir}/$(gpg --homedir "${keydir}" -k --with-colons --fixed-list-mode "$(printf "%s\n" "${senderID}" | head -n 1)" | grep "^uid" | head -n 1 | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*" | sed "s/[\]x3a/:/ig" | sed "s/ <[^>]*>$//" | tr " " "-").asc"
    savepath=`zenity --file-selection --save --confirm-overwrite --filename="${savepath}" --title="Save as..."`
    if test -n "${savepath}"
    then
      gpg --homedir "${keydir}" --export -a --no-emit-version "${senderID}" > "${savepath}"
    fi
  fi
# ==================
elif test "${1}" = "--bp" # Backup public keys
then
  if gpg --homedir "${keydir}" --with-colons --fixed-list-mode -k | grep "pub:" > /dev/null
  then
    gpg --homedir "${keydir}" --export --yes -o "${pubkeybu}" --no-emit-version 2> /dev/null
    chmod 600 "${pubkeybu}"
  else
    rm -f "${pubkeybu}"
  fi
# ==================
elif test "${1}" = "--bs" # Backup personal key pairs
then
  if gpg --homedir "${keydir}" --with-colons --fixed-list-mode -K | grep "sec:" > /dev/null
  then
    if test "${gpgmvers}" != "1"
    then
      zenity --info --width=400 --text="Whenever a key is added or deleted, EasyGPG backs up all your keys.\nBacking up your personal key pairs requires your passphrases/passwords. Click OK, then enter them."
    fi
    gpg --homedir "${keydir}" --export-secret-keys --yes -o "${seckeybu}" --no-emit-version 2> /dev/null
    chmod 600 "${seckeybu}"
  else
    rm -f "${seckeybu}"
  fi
# ==================
elif test "${1}" = "--rk" # Restore all keys
then
  if test -f "${pubkeybu}"
  then
    (gpg --homedir "${keydir}" --import --import-options import-minimal "${pubkeybu}" 2> /dev/null) | zenity --progress --text="Restoring key rings..." --pulsate --auto-close --no-cancel
  fi
  if test -f "${seckeybu}"
  then
    if test "${gpgmvers}" != "1"
    then
      zenity --info --width=400 --text="Your keys need to be restored from the backups.\nRestoring your personal key pairs from the backup\nrequires your passphrases/passwords.\nClick OK, then enter them."
    fi
    gpg --homedir "${keydir}" --import --import-options import-minimal "${seckeybu}" 2> /dev/null
  fi
# ==================
elif test "${1}" = "--ca" # Copy all public keys
then
  if ! gpg --homedir "${keydir}" --with-colons --fixed-list-mode -k | grep "pub:" > /dev/null
  then
    zenity --info --width=400 --text="There are no public keys to copy." &
    exit 0
  fi
  gpg --homedir "${keydir}" --export -a --no-emit-version 2> /dev/null | xclip -i -selection clipboard
  zenity --info --width=200 --text="Copied all the public keys." &
# ==================
elif test "${1}" = "--lk" # list and copy keys
then
  theKeys=`"${thisFile}" --pp`
  if test -n "${theKeys}"
  then
    theIDs=`printf "%s\n" "${theKeys}" | zenity --list --multiple --print-column=1 --separator="\n" --title="Public Keys" --text="All public keys" --ok-label="Copy Selected Public Keys" --cancel-label="Close" --width=600 --height=401 --column="Key ID                  " --column="Name and address" 2> /dev/null  | uniq`
    if test -n "${theIDs}"
    then
      theIDs=`printf "%s\n" "${theIDs}" | tr "\n" " " | sed "s/ $//"`
      gpg --homedir "${keydir}" -a --no-emit-version --export ${theIDs} | xclip -i -selection clipboard
      zenity --info --width=200 --text="Copied." &
    fi
  else
    zenity --info --width=400 --text="There are no keys in the keyrings." &
  fi
# ==================
elif test "${1}" = "--ip" # Import all the public keys from the main GPG keyring
then
  if ! gpg --with-colons --fixed-list-mode -k | grep "pub:" > /dev/null
  then
    zenity --info --width=400 --text="There are no public keys to import." &
    exit 0
  fi
  gpg -a --no-emit-version --export 2> /dev/null | "${thisFile}" --gk
# ==================
elif test "${1}" = "--is" # Import all the personal key pairs from the main GPG keyring
then
  if ! gpg --with-colons --fixed-list-mode -K | grep "sec:" > /dev/null
  then
    zenity --info --width=400 --text="There are no personal key pairs to import." &
    exit 0
  fi
  gpg -a --no-emit-version --export-secret-keys 2> /dev/null | "${thisFile}" --gk
# ==================
elif test "${1}" = "--ep" # Export all the public keys to the main GPG keyring
then
  if test "${thisrootdir}" = "/mnt" || test "${thisrootdir}" = "/media" || test -n "${thisisram}"
  then
    if ! zenity --question --width=400 --title="Export Keys?" --icon-name="dialog-warning" --text="One reason to run EasyGPG from a flash drive or RAM disk is not to leave evidence of its use behind.\nExported keys could be evidence.\nExport anyway?" --ok-label="Proceed" --cancel-label="Cancel"
    then
      exit 0
    fi
  fi
  if gpg --homedir "${keydir}" --with-colons --fixed-list-mode -k | grep "pub:" > /dev/null
  then
    (gpg --homedir "${keydir}" --export -a --no-emit-version 2> /dev/null | gpg --import 2> /dev/null) | zenity --progress --text="Exporting public keys..." --pulsate --auto-close --no-cancel
    zenity --info --width=200 --text="Export complete." &
  else
    zenity --info --width=400 --text="There are no public keys to export." &
  fi
# ==================
elif test "${1}" = "--es" # Export all the personal key pairs to the main GPG keyring
then
  if test "${thisrootdir}" = "/mnt" || test "${thisrootdir}" = "/media" || test -n "${thisisram}"
  then
    if ! zenity --question --width=400 --title="Export Keys?" --icon-name="dialog-warning" --text="One reason to run EasyGPG from a flash drive or RAM disk is not to leave evidence of its use behind.\nExported keys could be evidence.\nExport anyway?" --ok-label="Proceed" --cancel-label="Cancel"
    then
      exit 0
    fi
  fi
  if gpg --homedir "${keydir}" --with-colons --fixed-list-mode -K | grep "sec:" > /dev/null
  then
    (gpg --homedir "${keydir}" -a --no-emit-version --export-secret-keys 2> /dev/null | gpg --import 2> /dev/null) | zenity --progress --text="Exporting personal key pairs..." --pulsate --auto-close --no-cancel
    zenity --info --width=200 --text="Export complete." &
  else
    zenity --info --width=400 --text="There are no personal key pairs to export." &
  fi
# ==================
elif test "${1}" = "--dk" # Delete a key
then
  theKeys=`"${thisFile}" --pp "all"`
  if test -n "${theKeys}"
  then
    theKey=`printf "%s\n" "${theKeys}" | zenity --list --title="Select Key to Delete" --text="Select key to delete" --ok-label="Delete" --cancel-label="Cancel" --width=600 --height=401 --column="Key ID                  " --column="Name" --print-column=ALL 2> /dev/null`
    keyID=`printf "%s\n" "${theKey}" | grep -o "^[^|]*"`
    if test -z "${keyID}"
    then
      exit 0
    fi
    theKey=`printf "%s\n" "${theKey}" | sed "s/^.*|//" | grep -o "^[^|]*" | sed "s/</\&lt;/" | sed "s/>/\&gt;/"`
    thefingerprint=`gpg --homedir "${keydir}" --with-colons --fixed-list-mode --fingerprint "${keyID}" | grep "^fpr" | tr -dc "A-F0-9"`
    thefingerprint=`printf "%s\n" "${thefingerprint}" | grep -o "^[A-F0-9]\{40\}"`
    if gpg --homedir "${keydir}" -K "${keyID}" > /dev/null 2> /dev/null
    then
      if zenity --question --width=400 --title="Delete secret and public keys?" --text="If you delete this key pair, you will never be able to read anything encrypted with it ever again.\nPermanently delete the both the secret and public keys for \"${theKey}\" from the keyring?" --ok-label="Delete" --cancel-label="Cancel"
      then
        gpg --homedir "${keydir}" --batch --yes --delete-secret-and-public-key "${thefingerprint}"
        "${thisFile}" --bp
        "${thisFile}" --bs
      fi
    else
      if zenity --question --width=400 --title="Delete public key?" --text="Permanently delete the public key for \"${theKey}\" from the keyring?" --ok-label="Delete" --cancel-label="Cancel"
      then
        gpg --homedir "${keydir}" --batch --delete-key "${thefingerprint}"
        "${thisFile}" --bp
      fi
    fi
  else
    zenity --info --width=400 --text="There are no keys in the keyrings." &
  fi
# ==================
elif test "${1}" = "--mk" # Make a new personal key pair
then
  keyinfo=`zenity --forms --width=400 --title="New Personal Key Pair" --text="Enter information for the new key pair" --add-entry="Name" --add-entry="Email address (optional)"`
  name=`printf "%s\n" "${keyinfo}" | grep -o "^[^|]*"`
  if test -z "${name}"
  then
    exit 0
  fi
  if printf "%s\n" "${name}" | grep "^[0-9]" > /dev/null
  then
    zenity --info --width=400 --text="Names may not begin with digits." &
    exit 0
  fi
  address=`printf "%s\n" "${keyinfo}" | grep -o "[^|]*$"`
  expirationdate=`zenity --calendar --width=400 --title="Key expiration date" --cancel-label="Never" --year="$(expr "$(date -u +%Y)" + 1)" --day="$(date -u +%d | sed "s/^0//")" --text="Select the UTC date that the key will expire. The key will expire at the beginning of the selected date, UTC. Note: Today, UTC, began at $(date -d 'TZ="UTC" 00:00 today')."`
  if test -z "${expirationdate}"
  then
    expirationdate="0"
    displaydate="never"
  else
    expirationdate=`date --iso-8601 -u -d "${expirationdate}"`
    displaydate=`date -d @$(date -u -d ${expirationdate} +%s)`
  fi
  if zenity --question --width=400 --title="Make the New Key?" --text="Name: ${name}\nEmail address: ${address}\nExpiration Time: ${displaydate}\n\nIf this is all correct, click \"Make Key\" to create the new key.\nMaking a new key can take a long time." --ok-label="Make Key" --cancel-label="Cancel" --width=400 --height=401
  then
    if test "${gpgmvers}" = "1"
    then
      passphrase1=`zenity --entry --hide-text --text="Choose a password to unlock the secret key" --title="Enter the password"`
      if test -z "${passphrase1}"
      then
        exit 0
      fi
      passphrase2=`zenity --entry --hide-text --text="Reenter the password" --title="Reenter the password"`
      if test -z "${passphrase2}"
      then
        exit 0
      fi
      if test -n "${passphrase1}" && test "${passphrase1}" = "${passphrase2}"
      then
        if test -z "${address}"
        then
          newkeyattr="Key-Type: RSA\nKey-Length: 4096\nKey-Usage: sign auth\nSubkey-Type: RSA\nSubkey-Length: 4096\nSubkey-Usage: encrypt\nName-Real: ${name}\nPassphrase: ${passphrase1}\nPreferences: S9 S8 S10 S13 S12 S7 S11 S3 S2 H10 H9 H8 H11 H3 H2 Z2 Z3 Z1 Z0\nExpire-Date: ${expirationdate}\n%%commit\n"
        else
          newkeyattr="Key-Type: RSA\nKey-Length: 4096\nKey-Usage: sign auth\nSubkey-Type: RSA\nSubkey-Length: 4096\nSubkey-Usage: encrypt\nName-Real: ${name}\nName-Email: ${address}\nPassphrase: ${passphrase1}\nPreferences: S9 S8 S10 S13 S12 S7 S11 S3 S2 H10 H9 H8 H11 H3 H2 Z2 Z3 Z1 Z0\nExpire-Date: ${expirationdate}\n%%commit\n"
        fi
      else
        zenity --info --width=400 --text="The passwords don't match. The new personal key pair was not created." &
        exit 0
      fi
    elif test -z "${address}"
    then
      newkeyattr="Key-Type: RSA\nKey-Length: 4096\nKey-Usage: sign auth\nSubkey-Type: RSA\nSubkey-Length: 4096\nSubkey-Usage: encrypt\nName-Real: ${name}\nPreferences: S9 S8 S10 S13 S12 S7 S11 S3 S2 H10 H9 H8 H11 H3 H2 Z2 Z3 Z1 Z0\nExpire-Date: ${expirationdate}\n%%ask-passphrase\n%%commit\n"
    else
      newkeyattr="Key-Type: RSA\nKey-Length: 4096\nKey-Usage: sign auth\nSubkey-Type: RSA\nSubkey-Length: 4096\nSubkey-Usage: encrypt\nName-Real: ${name}\nName-Email: ${address}\nPreferences: S9 S8 S10 S13 S12 S7 S11 S3 S2 H10 H9 H8 H11 H3 H2 Z2 Z3 Z1 Z0\nExpire-Date: ${expirationdate}\n%%ask-passphrase\n%%commit\n"
    fi
    temp="${tempfiledir}/.$(cat /dev/urandom | tr -dc "0-9a-z" | head -c 10)"
    (printf "${newkeyattr}" | env TZ=UTC gpg --homedir "${keydir}" --batch --use-agent --cert-digest-algo "SHA512" --s2k-cipher-algo "AES256" --s2k-digest-algo "SHA512" --s2k-mode 3 --s2k-count 32000000 --status-file "${temp}" --gen-key 2> /dev/null) | zenity --progress --text="Making new personal key pair..." --pulsate --auto-close --no-cancel
    thefingerprint=`cat "${temp}" | grep -o "KEY_CREATED.*$" | grep -o "[A-F0-9]*$"`
    rm -f "${temp}"
    gpg --homedir "${keydir}" -k --no-tty > /dev/null 2> /dev/null # causes trust database to be updated
    if test -n "${thefingerprint}"
    then
      "${thisFile}" --bp
      "${thisFile}" --bs
      thekeyid=`gpg --homedir "${keydir}" -k --with-colons --fixed-list-mode --with-fingerprint "${thefingerprint}" 2> /dev/null | grep "^uid" | sed "s/^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*://" | grep -o "^[^:]*" | head -n 1 | sed "s/[\]x3a/:/ig"`
      keyinfo=`gpg --homedir "${keydir}" -k --with-colons --fixed-list-mode --with-fingerprint "${thefingerprint}" 2> /dev/null`
      cipherbits=`printf "${keyinfo}" | grep -o "^pub:[^:]*:[^:]*" | grep -o "[^:]*$"`
      ciphertype=`printf "${keyinfo}" | grep -o "^pub:[^:]*:[^:]*:[^:]*" | grep -o "[^:]*$" | sed "s/1/RSA/" | sed "s/16/Elgamal/" | sed "s/17/DSA/" | sed "s/20/Elgamal/"`
      creationdate=`printf "${keyinfo}" | grep -o "^pub:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*" | grep -o "[^:]*$"`
      if printf "%s\n" "${creationdate}" | grep "T" > /dev/null
      then
        creationdate=`date -d "${creationdate}"`
      else
        creationdate=`date -d @${creationdate}`
      fi
      expirationdate=`printf "${keyinfo}" | grep -o "^pub:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*" | grep -o "[^:]*$"`
      if test -z "${expirationdate}"
      then
        expirationdate="never"
      elif printf "%s\n" "${expirationdate}" | grep "T" > /dev/null
      then
        expirationdate=`date -d "${expirationdate}"`
      else
        expirationdate=`date -d @${expirationdate}`
      fi
      capabilities=`printf "${keyinfo}" | grep -o "^pub:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:[^:]*" | grep -o "[^:]*$" | tr -dc "ESCA" | sed "s/E/ encrypt/" | sed "s/S/ sign/" | sed "s/C/ certify/" | sed "s/A/ authenticate/"`
      thefingerprint=`printf "${thefingerprint}\n" | fold -w 4 | tr "\n" " " | fold -w 10 | tr "\n" " " | sed "s/ *$//"`
      printf "A new personal key pair was created for ${thekeyid}\nFingerprint: ${thefingerprint}\n${cipherbits}-bit ${ciphertype}\nCreated: ${creationdate}\nExpires: ${expirationdate}\nThis key can:${capabilities}\n" | zenity  --text-info --font="monospace" --ok-label="Close" --cancel-label="Close" --title="Key Created" --width=620 --height=620
    else
      zenity --info --width=400 --text="A new personal key pair could not be created." &
    fi
  fi
# ==================
else # not recognized option, so should be file to open
  "${thisFile}" --rf "$@" &
fi
exit 0
# This is the public key of the key pair used to sign updates and the messages below.
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBFODh34BEADq5HEVpnqY7uqRkUK7H6lMxg7r1tFGEXBIygOPx6kxMQeA3uVb
tigR/cjSz7CfLlYyYocZeWUtvqw610vhaDfZe18aHt9PH9obwTBsmGcTJkNFEewL
FuyW+fbPj88NuoZx0DRar8BrFG9qeJhnltlNvkR/d+useV2/jDbDkueAKT91J9CJ
SpcOuZRayeI/lI9jzNXfv5B3Oqb/Wa2mrCI1zRG3D1vZVva/BJbSZ4HGdXnBrSAt
YHFZJ9iLyZm95n7lnI1EE03BCoC1pT7ms9x9JclIltL+h/iOloQj5eMjoB+NcEpW
2B0r3+FsiRMfK/WFoidpp/FKFSp5c7W48OH34aOluIX1hA//CD5YrMr8gzrHVasD
FFG1WMdYzUTO1rJir8ZD8Rv/7QRM/1G5+x2Tb34h+cEVj70Ym/1FlEOql/1P1qx0
uXoWTMoMF14Um+7kKcnH/fQcsUYO9A3h5R8yFJItD6Wmurw4DTHh9Bm+m31HsGAw
vAqMVXsaGK/Emo4gjcD/IdnoV8hmr9fXmIgJ/DB0S314CEzEb6Q1CyT93sn+Yfk/
hdc1aHu1bhVGgigjFlUcivjaFZ63Y+THybypluzY+aIJXIR2g+D+2Mvkm8mW1Koq
RVAOBoaWHyp8Dqk75tqoKcFYzff/pC+cktLbTdZGIIER+Ta9FR5i7fyIxwARAQAB
tB4qKioqKioqKiA8NTdCNzMxRTlAbmVyZHBvbC5jaD6JAjgEEwECACIFAlSvAOQC
GwMGCwkIBwMCBhUIAgkKCwQWAgMBAh4BAheAAAoJEKa9v1dXtzHp+t8QAMGVOcgk
mJEsj5zjyh0MQQIlK6Owyid+vPACMbKj2GUbgkyaJR2eXqMA5JPBIWeceYLrm0fg
h5UIZRDYqXP1kZyj6KwTlmpAv2Y9xXRDgi8VaAj9Uj0Tr7wP8SEOQC2gp0QrAP4O
h70oOqhwPlcMq62TgGHyxe/wYxOnk5FmAT3QAZI9eAYiefQPx+JQXJJzJKbnsNwe
gT4D2DmHtHkxrK6xt1fQfQFdQInJX/e1m5VImpqF1QglK9lB5vu7R8JySY4yjik1
tRSilcwTnvZ0sdci8g35ddM2OM8kF5sFErESNNL8Bk1LF3ouo73B1r5OjOduE4A5
WMW8qSCSHSrWjeLrg9RydkybLy2RImXWWyHsZoLyLLHssNqThwroCWzMgDe9QQz1
mWWid7FMtpXssp3fcg9jj3P26uJXqzXSauB7vGTZZvkZs0YfiIi6kjubKh04JQSL
lBsS+dzYwtXszchMJ7hIAMCscNMuCxd7J1CO7ePczB/7v5mnGx2nTBLkDutUJl9+
m0ZvRXn7BSMfsuTSjNvPvEZOoP3M94ujIS36LRqcqa++gnIzVtIyaHH++H0lJzyS
dr1gSS4KGONM7FqPY0Iy9EhvEAV4vOP3HWZT3d5KVPxrSHALVEOw7IRVteZQL4Ts
Ngya5ZS2AsdqmU2K9qHlc3GJePI+gkgblmO2tAgqKioqKioqKokCNwQTAQoAIQUC
U4OHfgIbAwULCQgHAwUVCgkICwUWAgMBAAIeAQIXgAAKCRCmvb9XV7cx6Ue5D/95
AJrWOoYg9w2Lg7VEHOqIfmPvzS1slyPCwwaDj3vyhwOR+mYTeuar7y2q/4zGDxTN
3IOX9MQSJO2jVclRS00z/r4Hr17axsLterw3oq7Sr2VeHBk1xuHH5wZ0jgNWo+cz
mNWcVK+G7cD3/h2XPiU2RwJQb/59adGxVYcUem3dgfh7TpbBkkrFjBjFwb+JCKZC
SEs2ShJI7ZG7Ziz07ZkcPPKTHaFR5Gixq1ASh2haKOwFu6VWoECWqZC0jqlsjbve
IAn7eUORrdf/NTHlrl0J5188XZ8rUsRGRDftczLuzSi8q1GRHz7Kct/kPcutUzXy
1XGWsnnAw53tNtC5Snaljz7cfoNpf5/RGwt4UQg1SF5J6ci+Fp9pMUvkPWRRuMTN
akpyh+U+Zr7R7DsF53Xy1XzhfvJQI59tMjRcu7G2T1CFf5bg2S962JWg+SDUWH78
0HUNQgqNyq9eiErS00zjsCPz8Qfg7qLuFTK2ZIY9KN47afpG01Dva+e5ed0eYF8K
m5zRfqAPDBEO1EHpYpzdti9Gb2Sshobv/tHK4lzjV4UuyK0DGTRV9rg0zTtZGGk5
mvjZ8ERsEnOptFMNq2BCp7VzwA/NUWIFAWVzcTbxgfmgIAZDJJu6NuMs6HhJ4WpQ
ZllejM75SRyuaNBtIjtwwSw5hGYPK8NEmuUemvFrCbkCDQRTg4d+ARAAom3VI/kn
vN2ULHUseNi+e8enJhePmVTotCUEojuTpvRO15Zl9RuUZtdr5J0wxPZRfuhKeVaO
xFStsshiGEyumPG8tE2h5rnANQUHvdAi5J+swbXC97yDmzp1q+qGoQdHsqoVy8pt
XKnSdj6Y3vrod+NgbAEOigs1kSkp2mti/NqdInJPCZUgYF6oImmhyxZjHQ464ZQH
Tt3L7ile+AbdF1mTVVF4uo0vKq+jJ3wW3QNPmqMk6f6ReknCec3AMIsaesN9TLlQ
Q+XnVHYe5IKGaNyc00WW5YDoV31obJpRpmyrpepguHBHJFw1gxgQFzQ1My1gkB3B
P1CNqKBf7PGJE4lcoQI4c+yhEyCbGY08bHKMMPIKSeXzY/4KiwS/9kQ0FWTrWrie
BwLDzlKCL8/ZrVrfHqyOwFojeDZ/kwDKqR4Pyezti84Qxg5UWe2krOuImZUJauPG
ngbFLNjaaY+fD2pVNuqqzmCABDyzYsLM47jBI6nDMfRcN0FrN3F2uOjLzEb9Vzwy
28pXVBh8AAFA+eKtQzbLLEZQxqEdMjmMjmQwcsmDqXJ5PF2dHzW9uzlaXsMWZxOt
YfxQkjgiLiRxlD4BUBwJfMn796IK0STKPsTHG1hAFxfsKg+ZF+HcQoLFMLHdMrMa
BQVIgRwubTW+iQup4KiwHJW7i870T1K33isAEQEAAYkCHwQYAQoACQUCU4OHfgIb
DAAKCRCmvb9XV7cx6Q/tD/9Pq8tb87bOxqULJ6txfyhmgrZLasF0mzjIStnBW4Pt
DaLm+Ajz18bCCzIIHSPHlmqXpFStecjLAKIfbl/ewhWXUBoGVfrRvLvRQavLix/z
ORZvBzEiUfGYTwBUwh8U2fePH8I83Dlb110RNMMoQ7jX2zudjulJ3s1in9HdNSvX
PVZTU3e6YqXtqwaOfPpPHOxpJ5jVqH8om0n3d+PKx+YSps8RbJBW92ubHlrmKyAV
T5wpAuy8bX8YahXG1M3g/gqo1h64uFuCU7GcUwqxNlR29HZxHCn1DhheysmSBDHu
nCoRoRutRmFJJEtTiaLTjtWj2v85KvhV2jMVFTjUozfFu9rEZh+2NX4hGkRZVqsl
+sjQertl7vVJj0fi9jARfkPleQFgKEfWvP4A2pr4DiBBg2whNWPswo8sk3M3tUyh
PmpxYcTtN0MsFmGv9jLfRYA36O5YY6AT71to0jgi/QjX6F4SDG5UuU5aFQp74gIS
DZ8B5FmeuE8Kjg1RKmVHzRE7J4b1c1ZzUwJqgVU/t8u4SkMuCu1EZh0BIGoCQ9LW
FIe3qNA5sF+m8Q5TujNm95qDV7HuMK1zZFO2t9uBC7T97WaZLnCMIB7SAJI7Wthw
0ly+ZihYjQ0gvDdmp8c9fI3BvhYayZqnkN3v+5hd079PXJg6pU2h73KZ5tlHZ3hl
ww==
=yzOv
-----END PGP PUBLIC KEY BLOCK-----
# This is the copyright notice and license for EasyGPG.
-----BEGIN PGP MESSAGE-----

owJ4nMV9f5BkeV0YB4JFk9EjJBUqhOR5p7czoWd25/bu9m532dg707vbYXZm7JnZ
ZQGF1/2+3f3c1+/1vfd6evtERY1WTqJYlRMSFEVisEg0RSyQHynExEIhKVBMJVLK
T5PCnOFKEwKUWIF8fn1/vX49R/FPLKzbmXnv+/18P9/P71/vZ1aedVfjrrd/4IM3
b75n88m7/tNT5V3r4eM/+dmLo3KcXFppXBypMIL/BsHFsSrDYFSWk3X16DQ+fuk9
/SwtVVqul/OJuieQn156T6nulKfx9QtBfxTmhSpfOi0H6w/fw8uUcZmoS61eNi2D
dljMr+5fvXiafwnbnZb9LvayaI7/nVy62LvUbh3cgudutLsHnb3di6d7ly6envBf
t7LJPI+HozK4rw//vBBs7e13b7Vb3YOgHKkgnJajLA/iNIr7YamioKeSbGbeFgCC
uAimRZwOz5u/yHYXe3lwGn4DP7autncP3d89ptK4nAevaO92Dm+5f3j51k5nn36h
Vzu0oGQDfexgHEbw6zTYm6h0/+p+M+getILbah5MwjjfCPClAQCl8kkepyW+WY4A
Uv0EQn25dfaBK2fPtLYe2Ty3dfnMuc0HHn74gdbZ7dZDl7cvX3nw3IPnLp87u9l+
hEG72M8idUnB9sPJcKMYXTxNv6C7C+O0IJRNpr0k7tMuekfaLUwj+ClXKgBYg7Eq
inCoimZQxMMUEDuLyxG9X6h+rsqF9zfqkB7FRZnHvSnezDSNVE4rlCofF/yyCq7u
HgVXVaryMAn2GbSduK/SQjWDY5UXcZYGZ5tww/1kGukL3giWbBKntOgom8A2o7AM
4hIgTxJ4DShADaZJM4Ang5udw2t7R4dBa/dWcLPV7bZ2D29doDMi3apjxevE40kS
4+HDPA/TEk+80rje7m5dgxdalzs7QBkBXPqVzuFu++AguLLXDVrBfqt72Nk62ml1
g/2j7v7eQXsjCA6UeprzBgNYaZzlKoiAFeOk0CgdMdkxr+zn8XHYn3tMcnME8BKB
B0U2KAFaQB5uNswVcEVR0sUCNsosmPD7dN1wldMcaTxm0ugAi+epKi16CXdpVgZh
WarxhFboZ+PxNCV+s1ShX4VrToB0gnk2hXuGl2N6B3YH6gLJEQzybOy/Aafuj1T/
Np0/DFI10xdv4YC7KwCO4L4kenSaXYCVFcirfL5xX06/CDoM6ji8rfA5B1wgj34G
CwazUWYEQTCeO6iajZT8J20iOAjIDKlnMs0nWaEqFwFSbnQ2KMp5olgcrocJMMn5
AK4RznThnkt0ye3ddre1AyRweaezFcD/t3cP2iABz9Klnfj+DUv49z8S/MNpqoL7
z5w5x2DUy0X8e3AF2fdAzhVcyYDnwhIWagK2+xsA+X1JeeEiiPlcDV56Dwr786dP
D4rBRpYPT99zqfKLi6fDS/cN4QXY9iLuHLQBj/MMoEGmBy6OS2Q6ookJk5RlRrzE
Huw+xj/GqoDXtbxIhOCjrD8dw5mZJ0GbpEO8GyCZuGCqS4DZVbRhDg7IA/DTcAyY
mwBRjXuJuucSArovPwmCjVg+gd9gkxDoUcHVI/yJGpQGNCCBlYamEDpZBkSbB7dB
1ZDsmmX57WLDuRHcTN4uhJWB8WqWmORhvwT2SXiNICSOZzG70gBklkjE4SycIxfl
BGGETJMFxUgvRchiicKQBMHlOQn6PCzKJqzztGePUaFHfH/DaYgCDqinsiUgobon
XIpmUMJECCfKhnk4Xl+HlYgDiym8AteYqzHpHVzPQScxOskGZEdYCqC/KSJrOQnD
k/gEKOunE6EFKVM8iN7yAkITTlCaA8KTIqOjhelc7gRxCNAmoDsRIUikiP/enGBk
1Y5Q3gKx1geVjkvNWbatNAhlggNQmGWWuXRBwnkGOJmo8DbChchwhY8iCgCOVHmO
5A9IFPw3kQlWGiCy+wo234Nt6s/tE1HgXQOpQQBxpTEKj5liHIpy+JXZdAHCYFXu
Ph8KW8AS4yAekIyfxcVorWk2gWP0VXyMb0/zPq4JJhAKeMDZUJE65hdXGjOgN/jZ
eRcfcujaAADv490DdH2GD1dJUVGsNAhUi3s2YmS922k2MwtHGS5KJARI9jkXtWJW
qn7J1E9ytaCbSRWjE2QN2AQlU0vBSixS6Rzvi07Da/ObCHBY3Ja/Ed9O81wZ+4ef
ggs9RL0DKAUqwAfxflYafZWjsQaYLCbAYXEvTuISL0YwXntjLr5IhcUDoktghige
IKWeX1yQ9DKQZb/0qAJZh47p4ugKrKnuhGAPwfonQVJM+yMrEkip4mIrDfixjAut
XkHwKjn2eAr8OgnBaACNV2aMItWPYcW0ZMOkAIHPdw0AFgvUFgln0koVyofX58ST
Tf30SsMhSEacoVVYqAUkZMAqRkBCRO9CPGS6FgTkfKVBFAb/jDXRuCjbBpJJwBDN
BWCRXyyV93fqaE7sqRlQTKkmxfmVxurmGtASyMiSdBSr/CytXDiS/er9a4B/ECJM
dCjCRDoA7uNjTY2JGoL4IOVdkI0h2rvpXiesd5okqRBPlRJw98gc7xTtL0LylD4X
yWw6L5y1D5I1B4mp7kwS1AjIMHw5uWJlb23sAW0QF74YgpvBnXtAmqwzaFeQ4Xrb
Au7dbpijE5srQT0dBA15o7fAHxiH+W34VQjAsNSJmnyzDFhMMh0sinFhzWB4DUxL
7W2AwAZjKJsWcDLSJwwLsgEKjBj+YnZ0UXiQjQl/cb9GcqNY4RMGYb+PtjT8ElBW
osYEDOTTdKWxeKCKEMA34ogIDpgwTNCxGbKpPg7T6QBMEGARYEuRjUVG8gjNAkA8
Kl00zGDLObpe2XgCzAu4sAZ/GI/JG9K3DeQl1+JolxppzmIvKOZA4WNYtA9LoLWe
p1Z69JBVsj44J4X26ODNkPGaRdN+yRYWuv3HcTQFhU74nxZkxcewBhqoKEQKBSeY
MZmRWUnWwTRF1E7KEI7ky+GZEj1prwSRoj1R7bQChZFWyEYgTlmKiFmnhH4z5DsN
LezRkcMZmgpzAA5+14ObTctYUC32COAP2YThwQcicqGQzICH0G4TzzvPjmPNxrSp
fhXXGkzxkg2NgOAz4ANNFKTdtH5jWVTRA2ycucwfpwhoE13kfK6lPPuP5GsixsAK
pTMlZEAZipzgnwEXK42DEr1SFK7TJLKGvn5CFBOwGUAkYgapkcQMkcbAMSYzFORk
FK2Lu4Ye6gQYNC/YsSASygpRCFFGmh0NF2ba4yyOmERBFcI9BBESbc5Pa5DYdiRc
sc9gzt7HQwBHotoBUlAggoEOjpHu4BGw3cCZB0cVqMyaEXh3VlaBfJ9qUUXcQDsC
Y4rEmRa8sWNC6M3TLF1HaKqOiNC+6Csy4DJglpJEBRIoinzgWUfqIxGRRGXZ0ier
G57Fm6l3wGhx9r4O293rB0FrdzvY2tvd7hx29nYPrCc2esC+VLBqOMPvndkIttUg
Thky2AYelWOIq3/oKDLt7JOtTGRiIkRPH05yUSRrGydaL0yewViFgCSjbdeTGG42
CWeiS9j0h719hxAMWvTDmiLCgLrVOEakA/+jKgWDUHNS4J9PBft8m4vHQwfFABKS
/EVHRdxMG1OLgbf1MYOgHQIE8gx7uVEE9FWQstMhFLAFZD94Q34nS6jC4APpQf5o
TTL91zF6SMqTwhgAzYdhGj8WllWtd2jCN2xXmE34UIx2HUsgCxvtxCickHjAHyZh
XuqLxndWGsDawI5hMUIiYJ2PmsiaStbQacqFwSWmogfJREcPFjQqGLd9trFEQwHG
pgnpNQIvRo5MEsShdwxHCesDMYSwJJg8sXYuyealf8nbPXI17VsIkvuGi7qWfoki
WRiQhAfMNRDiVCynA3mRGsiEspzdaVvQBIwD+Xv9NaH4CocgjpbdVEQUSg4VGwak
CkPw6SSSCtLMwf+MxD0JSnYfgKRQSQBwIBWAVUAlwo9JTJROrmacDvBCFYl/pnaS
xH16xF4zsCfopDuo0+E/qj8tJZCEQg2MHFEIgTFq8c8hB0PZfcFr35cDIzFJwLkw
knKl4YnKVTowBgolaOyKTfCIhbrC4zBOCFj2bAAjJJXYZgfyLdAWBB2SwtLoQdEt
oiVxzE4acOxMJUn9DQHSjlU9I6HoQNkkRpM5F8kwlSJIshuqnFw7hHQ7aGWKH8+O
HeDmOplQGJ8MSYDzhYdkIqDyR/MSlKnRvWBclAgO0wIa+CWY9wX6JxQzYp9D4nx8
DsCXR/Sp3e9Y8Ub0CzBeFeotcCXmRp61Jqxt8Tp3yMfZzdAc09KKI2vIY2g+lSY7
YO45ZCjSWFsasN44ThXZMWhkAcbABwUjx/iW6JwZODgqZGCwtJkSHMZJA0DgPtmv
FeSd7AmJ0F8V8tZ05J5D3ojFnSCrECw7HZjRuqAgkc3I5kVoZatFbOoFgSXfF3Rs
rGZyYSaCa5VNZ8CxRnszYHsUbDPBxhyKo3Ni3iAkZQkkMCH1YLUlMCnw+LTJoQPG
O1wPBppYUtFSY6UoIoDCOIe/5pgksLbJopmxyWbG5kZwwE7+Fjr5rpmBol5IyIkD
aJYaWMEpUo+NK4zVgXiFP499hUQBSGZ9VyCwE1daVbvX+35F+sbuZfkWrToGhhYc
M8csVQoH6B+EecQpFbyD6pLO7TD/s7KI6W/ZAKAEu1ccDVwoQpsMk21z2BdEQTYE
jQ4/6wfAf86iOQaamvp++qEY5margg1q0kOMR5Iz/WkSmnDpGBGVgL09DYcYuEqV
WMEYgAUSTuZs/YbjDB60IQdCBol4kWh6jaohrFFE7iaQbS8PUc46qhowwBrD2lci
E4xya2pBaWwHegzJFrNKiRI+Ww3XOLAuuUpBTQpXCPJIbhLFbf92OGQNdD38fkDN
FgjNLDWZEOPComy0Rg/sQI+vNJznSar01kCD5sco0VM2TlnCi7dkQZ49xf47onhh
a5Q2GeU8Wd+GwSJlSW4M4UOryTwsSrNYUHg630yqzsaEERkYbDPmZQUauSAhL+R3
rD+4A1AKTQOX4Qto1pL3TIqHXl1prN7GzGKCqieNQIhxsIFxBQY+6GcTLmEK7QdI
VSEZDfwwLBIjbczX0ITgA7P28Ekln6IUI+pGAOJE5dpf0+6/zdLwg8BxlvmZM0Gi
lPZFXLQSdHNIeSvLOZIbIbQs1mrEVexvQ8QnqEsSFtnufdhQADvTJYZ5OegkNDZg
2C0CWHusEbS4nLsdxeEzHUC052fWIDSDAJ+UHLehtFWWiJdujR8gkGvZDEMNTVTV
UaaYJTR/6nVPgftT5XDCdTUwUGYZOynyB+AVS6+cpNK5BE3kufjfjlENqAVqQYxS
uqYCNHIwoMa87fMwAyzBTBNQr7tTlKRimjhWj3GWg0GckHlYZP2YSl/IzpI75T/q
bI0gn5MjqsqHnFWMQMMK6jiROwdFilnKZL7SSOIUQ6XFtGfQo60W4x1pViKsugFN
CcWCAa2VPabVgGnHaCdFYRk6JQUUZMsDTREDDAv1wJJUKtVBNRcKJ6sKKC48HFfY
pw7HnN9xScr4MzpknxccI82VZgyMdGcUuqSjsqe6CIO3H9AnbfgNAeVzcVVqcrAN
BThmRSpHrTGB7mcT6P6N4HJYgKTbNx6ZH29pgY8tWYgh5YKjGsOQKFr/WZunGAci
xbaQodjXWSBKEKCBC8c9zth701Yq0ySGBCMSBTpMhc+PsRglcGNAlEIAewWt8hAs
F4xvUQZlmibxOKZF/OyGlk+LbrF4+uC9gcvCFwkPp6SZERTra5P7Lz/35j5KSOXG
fOm8VDMYxlhCFJcFCzdSrxQWjctpKc6GXb16RLAP0myWqGio+HArDZ1ZHAQDLNYi
vZ4L2VG9XpiwOVBYvPbmvofsEh+ly8AZGFPWBFEkLg97/B54TsINPH5KSbMPYfx8
N7oICjZBYy2Ue9EVFQTrDIOSUhVA0RugIcpta3jEOalsn5nEq5BckSEBsVSnsPYo
PGauBR1APqxvfYPvlEwLjsriGgAZKQjBlWSkUYCCmNX50QFnW1Ir5SXE51CuTl2D
7sdEA6zAEtepcXMuFg5sXF0KaqEIzGM2DkXjMJpXGuIXkxg010iUwqHsaWEiWC6Y
latjj8TkMynT42EDeISuqadGYTJoCs/TrzhAA/jDSolIA9Mk5qbjcYTcSYKMmYl0
7IOjl5wYljIJcxIV2cMDBelkFWZSVcK3NoonrNTgzQ2v9kmjUCJCpoyjH+f96Ri9
FHQ/vCIlpBj0JvANviKXZktbPIq1emSywo2Rh+GVIl0IRKwGm2c46F+gaQLox+KE
AiP9JwnjsyyMz26gCNJ5syPOm3HUosuMfgXx2AIlub5FZ8K0Ae66Q2xspfZu5t03
6nKudowU2OWRsTzQctOZCbi9/ijNkmyIKgwd75Dy5RabTowN5EYwmCZgUCREa4CZ
oTCVPI8+HVqDm5ta9d3s7O85oqfEzBAsGoHXTyHP4P4zwTbga9yD9zcfeeQh5EXQ
ZyC/0TekWLsmK03gkg+i6K6HEckammMUC8U3zKAkV3y5y/UHsxBxgseW3DjcM3lC
wDm9GHVSdT8Pf4HeN/ADTGTseK+SV8u3wNIZbOm8HxOViXyv0bjEAaZOI9O0654i
lJIFLHVMMDGLh6HyrlJ0IKlG7eSQiVXJ87hOIvm17CvAr1WKMpq8YFAN6BN4djeZ
SE0WGJy4z5nqgO1PCUblcAalCzeIMbE6lJ7ISw8wLz2wEViBcEPXHG5x3NJllYqK
qZQnagyIaXCq8AwvVmaghiUsinVLmHsAPAOXxdNxvVJIi0ncn3KBAFcr2Agh5gdR
zhQj5AiFORupojwxjnhhpXFbqQleLyYsQi5MoEoClGLGdPWtNjS/0jkqlXRdW0fH
Jj0YSbAi7PezXDsSIubO2YQXE150EgiCxLAHXnlfseCZm4AmJZTINmIn1KnyqY8v
wmOZzi0sSVeYW+UCNdyO6uWQGNNM/o060GLYvSC0Yli9zWUhrqEpppNJllNxsgnH
2sIWW8J0IoU+yBT6oEuh17UNKsb+DVsfspRU3dTNgmktFlE12mijD7FTdG3ektCU
ji66tG4K9ZErxYrR1PBAHZ1L1lRJno5IoHD163mDpCk1nSTxpXCNTG+OrKJl0gcs
z53Yby1Rm4q2GM38AP4vlhCYVDiFVMR5jKWF4Fji7cBetDP8t/dN7UllI7yZKRCt
c4yYxVyrgvmKny20JYXrnNMGv7jGFG2Tc3GI2f0D+oAa/XSxuIQEg5bwoc7sesfv
r9kiOW2Uk74CKZybGIET03Szy7inPionpjFWiyX1YPpyiAKYLSsKVehCmdBmU6sr
UD1VqatuWMI0XWavGCFGGPEygFhCMWgKotCmlk4EuKvLxPOUWmPyz5v6LodhHlGv
AroPXMc35/QJxWepqtBzy1BwoTHIC/hOpotQ7bk75cXhXEpRbBRL6DcFzy1GWpXy
ILuqVFlSNVKhAHZWGrrmkSOEzv1Ga1hkZAAYhcUJ+TPAGQlE9go4h8UQLU2kXUAs
STDO05XVrfTJTB5A6m7Ijpa9lm/TFI8X16CDmBgN+XXkZXDBmpz+4mmSKpIT4Sis
MWp8g4+Ji+9Eo1QSsZGaYHkNho/Fy/NDeOT1o3uScgKQrDyv9M6zyVin+EsAaL3Y
9FLFpUl+sWE0xoQZKjGTHWmSi4x+PpY5HGfJdMw6FSRUlgOJ4t+8rLQ2RpxqhdSE
2MPhEKne5vNjDbhFGSGjLJzqB2uAyDnI5ousVUkKnysVARzP4ssWNjhV6KBaT4EI
ibjPhhOIuoxEvH723TDFmJK7WnehXAEC/9NHs0HifsgFtw4Do+ByjRkn/W1NF73S
iZr9IdbsD7mafRdsK1HqV+A2n0aj+9GlmmB9pV+OohyyewGKAC/qwaXa2M3xjoHP
gezWsYCQpGlt0LG2Pc/JCDMppsrqdxBqSzT7ltm9kuogawXcPlB1ZHFSunc0L8jo
l2JJ5vxVmylwHqmh9bUmma3jSZjGOjgnEqg+uBrfYSMqDKJpzoFIvTyvyC/3QTBm
Y65IIXqnSLmtrgUscXXrgo3x//f0oUjOHMMeKVuzzYCUDFutoPzBiOGWQBXMVZhz
GN15RLS1E8bTVvGE9WPOTQ6MI8da5vAcB4XsacCywWQc5pbE09a2gxgMYvS42JIM
NZW/y4UY/+CksDnbFe5FWXoQoMSsWxrQbdZTh5yF0P6NU0dTp77JD9G2A3V5UjKZ
i//hQrJUSpq4YkLvil6im2vSlpQNIxrrncgLuwBs2bi4OyfyA7oGoa7W4TQyUUma
iU+1YEUKgTsC23eh3XuUKiHn8mroUyqQjyUGWAuoZ0eGCRaMh2TXxIWOzXEkPuv3
w4KMQ3a6sZoC80sYauHaZfLEcRkduXdbSepPwJrasJPxleUw/EhPm6kP9RYssyVC
oSf+JnE6X5tcCGfQOClCxJtg/m+12kXDV7TGdi4j06YDHEowiH+qlgrEHeSMUohw
5qZfgH7J2xNZ8DqDac4hV6YRjuAbU038F6+P5xsix4rD7+CKa+ipooBg0Z6QXtQX
tsUCTTeXkpbwJFezMu/HiEmK4jE/rHLojIUFyUNEvg12zdd4ERItIg0L9yakitBJ
Lzi6m0MPMTXpy6GwX8YG4LFU/05pzBbnoEVIy3KHA9qRMSd5l6MY8Nj1fB+ywOSc
IxA/xcnvN4VdEGAdDmZzEISnKY1zvGYnw07KxegVW3NQIFFzoUDhOb+Fz0hqKSNx
V/dEqXy9zNbxv1xvaIpPNbIZJjxCnHL0g1O3iqqKGI01xQyVbC6uIRTrRU7h7Z5i
qTwg3SJXJvUGuhjG4SOJW0mUwJEhUb1/w24LKSagMSd060CMvgsmidyoTixJMEKB
iQfV8yAyj1dPAULTsHbPViZEfkJrQXIuK1jDhAc6j6iNjS9SeDrfqXAtpmN2iZzH
tXdmauZWGiV2jxNe4CYpNoAOpgLedOupsBLL1dn6YdDT4Ri0eZO6A0cZPDDKkkjn
GAurXHWBgClxIM2fRNKRBFoBLiaUeH9K/SIRVvBidSv6MjjdAT2bVPiWzhwa4ySW
SlTv1Fh8nE175WCaUKFdYfM8cH1ZcsxXMQiPeQIJWTbhUDfNuZV3pinJajyq8nNq
89BBa+oL8FBW06EAiJ9PyDzNuMQzS23VGdB5PwmLwuncalYiLzr5P7XtSRVQAj4T
MVlIPVK2HKvyKHjd/XKqweVLU3cwc0Iqk3hgwukXgJ3axbigk0DDejRrudZeRAV4
fX3OIhQMcbq6qPhcjA80HKIpGvGMLozGmx0YYBmfQfYGDb3IldTTcuKHTBYkPAr2
cthQSYmtxpI+zmLDS4crutjV75D8o3/79WMuezpVqWM4axZh/Aw26KsIEzRNafuU
fg+cCsNYZ6ka2y20VI+cdkYOknBFmapp0lyM3+hyTw9ElGZYel5dgFvFi+XmpPIA
xFAYVhdhOayq6jNJGJdxOkXJMU1JSov9bePvKA5IAuK1RzJmgUxpLI+V9i+WGRwf
46Nx5RYlqHuKAheV9BwSVQ9roHCmkHuxnYGX3kwXBLAbq9a6RVxR3FYyr27t1kD6
7dlBdTFtS8gc94PbNNFnNLlmVr2h3szhV6kOGrihYtvVx4aHd7dY7CR9A446NWal
1OJNVDnFOTbaLAYNR24+VSat1oZ6fRgL0sPwE5jij0nhvMI62xr1yGf3swAasxRO
7SnXKV9pcEwiWMZ8OLpjKgk7N+5vIlkUswKBNAAlzCoSrzzNOFfvGKHwup3zFFKf
/9xjtwqRyngGtv09tFO1pylPdGPKGLjD0iZekvVOd+/6WrWyzT2J494tQ8JicWeI
+UFvDc1/7no68oD2K3VZ6GwdUfh0gpF1KYKRLBqxs+Ung5HcOZPcmCEzPRWIZbBP
oJq846dbFdWL8cfgdNo9EccjUhTCwSFECwk9FGMqGZiKGZNOjlDUKS6FIy1HOsKm
+Vk26Z0AmuM4S6j5ls43Tbjak9u4sz5Wxw5Ej9t6zLCfZ0XhruRX45zAIywxll66
Nso5uuimn2u5irv96G0TxGGLGvlDTxMCJNLIEkkkBZUa9eUF6thU7Ndail9N+2uX
FoS5bhTGoRUzBBlQhvPQiEKmKeaPqF4CI65SvyL+34lB5XMcVD63EbRsgutQVUPJ
uuHJPmOTRrZzM1dukRayiVT1L4R4Tesokr2UbnGvEXcLU31rqgI9nUy0qk10bqw0
6oHhvUPJ8EkuT1fJcNZRp5DInkXZwtVC3KpqhxaADc/9Z25nhRuy82pv7HQJTuhx
aHOhfRBLIimoENZCT9UaVCHHGQO3iNvk0aVJPC81G5NnYdN1SA+sJdAhc1bnfGAN
JvTMpCHaPdyEg+epVARh0SXrNX30+kM8XTEUh+XqyqLwPKFMJ+F2LRTI40xqpeq3
05UGYSkNfig3KaCFdRmMQOB1NLVXl9CM4FEHCG0BueTfspkGBF5E/1Pm5bBfNNNn
rLQebKzZGlOKH6HwrN0f5Y2I2aak8CXmQ75cJcvn121SrlZPl6FYd20lj93OvZ7d
rMSbpR4rXT6pJ4bpKQ/VrAwPvNJ1lFSiBCDXQGqulNpaxGa3ms7Chl6vyrlik2en
yOprJwoRv1yN/lSfG9qWGjXyfs1YxZxzidQNFmvLxUThdJW9iUtV61KKYPNBktSb
D1UBugCLVzM0XdMoTr5Vfmy0pe1Yc2LynNI0JU6Sm5Z5n3ocDEGh3RNb2Zrr0OpC
vptXkaS3TovzjXD+U6Z2ciPzwjH6aygxTIkk0JPxFD3dD/c8jFPjjlvS1uewXfNL
RuXosSzmUHZ2jg5XOsiaUS9s4YRQTbCJQQnN9LiFM0VrcGtCBvjCVCa6sYZGktDh
GAQKdSf9g2svaPCOdzcmvqAhX9hQrWEdasJIxvwUkmOlFjMHjwuPy5Ww4htR0mWs
qREfYXCa9nn2g8UuJYQVC/sPXFrEGoRx6pVk2pM5E5b0Xeqwtb6VuV+VgyK+8I4f
rOr298r96lKqNeZeniJIgRSaTTIWS4AActyKiok8MNhP5+6Dooq5Iqx2YTMJAEy1
jDoqdDQdf6pFQm1wFKvPSGBWyhyr9UJkFGDgBWxPlJBiUUlKA7WLFBs7BhViRlgZ
QyI682aKwW16QivySs1oRLVq4rRpWyLmvhD22kKRMbX1Z445ECwrkQzZ4dWmcRjU
nciqAtHmfEGK+jHRGFoYz2kgxCCLzvs4K5Ilw9Ua6MHa0nlu5PJq9T2D0zE06pSW
JVz/8G5FhNvf7swT9csi8JU6uNHn5EaLYgpseiw1WMuOUBN3IbjZzF6A/gSPpsmU
wZPv0BoxBZmmfNFt5GtSxQ8gg25Dx0kWqNofT8PMI++T+yvUJcOyKY+xkDfyLHVx
PWwAf6HuzmGyrMp2TW3KSXeFTsXbjnanqk2be4nJwef6vbBw3JEL7IKHPaRcN1Ek
B5cACWihE12th9nVeniD/Ks45UBMXcEOtW+atig7FK9y2TKcgWBFNVyg6W+ocLFe
lAYNzs2cYtscxbENs53EbOYyvQxnVbFPSiWUfidiKUdR3khB0/HiyAy3voSkEXUs
mOm11f4/xUEmivCF8MfJyJN9m17w55pTJEjOBpbM8tBYij3UmrGlmO059prKKFfO
gzsR/qqRGlDAjOIr7PSvCcp1+t+E0ynECG5WUmvsei2FGAwZ8JQzi1C/lc023SOx
hzwipGmr6WT1lYYsP8Bh3nixyIADyR/zwxYlNCltrFxTikLvNP6VO/MfOhNEZF0N
SrkWajkqK37EdXDSM7oBr/9O4/OpExGKqsJg1Dnewun0K3SmWBXOqVYaT3+sJhOA
DB4fxDlWJMVj5QxCNXpT5BYsvpSCdHs6285r1g11m1IZYtsu059Kstcua3B91sW1
9K4RQBMTA2CwvLinlSX40AIX+lEtE/G0fEvIM2yIYXKZzIMmHTmXBim65MbsQEfG
Y9UwPGt2Fx5ajGKVZvdIqmVK995demg6LaDBo2DEkYOdmVlBOLndm4Ctq0ewBlE0
uVcNj2bTSVL6EZbSj2xQ4HRCbX3oMYnRLCnca9z6uaTjRxffuuknHv7oExAV2kZc
i6QPhOKXWmO94jPbPNxK+yCFQ+4hMBOWaopUKUmS83x+yvKEOrkIUOlOmacpYaAB
RhoygQhH05HWMLSkYyWhQZgzRIFsHUpee6PQ3BJ3lPzMy359e61O4n6HSh+zkmkI
7A/z+C5HbOjZwdLhWnMV/kBOjEmaiV3cscuYXmzSbkpZB38LhMnd4mFBZPAAsyI7
kQQ3ZVAhGvgtrW1lCfEBtrMZMAiOnwe61OVOHh3SQD4j3Za0Ivp5LU+zG1lYONb5
olttHKWmtL03jbEiIX65Qp4ZRbsWU84EkcHo3YHPPDI6OmH7zTYFcudzTAjuucko
klrG7q2ZwdhKtd+FAVB0FG1CzikK8ZOJJhfIW3N3tEzwcwcPsv3ozVZFCwQnKXOp
N/jJ+hmqYmRTqGaRscqHTHruWEOSp8uYXsZqcQm9LuhLg8VjSg8G5+7KTBSWe16U
+87du2KIC46w2Ns8gJVcyOiODtGNMZz54oKJ+SkajhtRxzJHqCgZjR8KGYUR+zw0
1JRimNYKNEONjR0IHkAyRcik07faA7Q8i+qewpDyEqiaNM68+gB1nZSV6fLSLWuM
DTUYYCleXccgJRs54ICCrMZBLHRuVBp1bY66MigDbQ+aRbHM1vemvYjTywJQQ2D5
Guei59k8TCSJmTl1ltzJaKFZgGTZLLe5e2icIINyAAsRh1Kq5JWcU8JvnXuJmRqo
opl+piQctmhPMWCEmc2hjlXwFxXEm5CnrRKIbH6pycoOxA8XRjVtFSx/eCNMZIz9
mKrcJB7oTrPEjWwBnDstqU6K6zlwmxvBvowh9mafOOVmHM7Nck8GLZi9yJgmjE49
LXUBjorJ4Izh9IZK7dsxytTjyRpQxAhNejATX21jj65IEXBPFTVnqEwm9Yvxal+2
87Tca/TSiihIvT+C1JxJIZyIZCeH7uzStOVuCU+zDvtilOGlo85n10b/uqk1Fc4N
pfyvQ0nkVoCZmqJJb+Y8SLWFV80/qFIdxWV5noFkMqsIA1MJg1pi6+qSBD7uUqAo
UcizDCumnZYtdS33NbuzwMBRoTbaTWey45+a3m1niaFWPTPKVtvouLYeLY06z+gH
4mbGIkdEC3rEFGB7IRbOCdWr8za3V9uzOKZkSJEhM3UEJ8HmSYTz/YykW5fpW14k
wlE9PrEuodImFYXzSBsq7sNbFtnCHRokV1io2NlRPJTmJHOJ9xccLKMZLzyo26ux
M4HFF80M13qTQnp6zK53ZlfspHMdTQLJOsyVxP+4PiMuOdQpTZlYP5KJI9dktzIT
M01RGIUT46tm4Gaq116w9GWOvX6JNzxWacgt0vxVoKmkZvgRd07vGs86l9MRGehD
6Y+D+LfM5TBsBZnZwvIZD267WHL4hWMaAnInVdDCJ9TMVexwHL0ER6FO24S9jnQB
ZunvXFpC7Vo3eoCMXwZPqRry6ThRTjPlsXRXzzFY8sU8P2rGXRqhnirszbOrtPQY
S4KqUvBJKkaKbUiGTKzUDgty6rF1M+WS48IpMM5L0/Rle1swjTneIbtbCgcus2dG
9UyCpp5KQbDZyHaFPswHQZxYoBmnuHrWbNF0pRnL4KcRZ4tFJObjCsqwTOL6i8bV
s50tdqjry6qk489htaEtyYGZKV8BT81GlaPjJhV6C2R6klNrX51QxnEKHCTNdqOO
WTGY3Hhb16ssU9TNq6zZjOvulv7E2CANGoz7D+RDBZL0RnBohcC0okrECo1xJpEk
VsfKluToefiBsGUT87vFNORyP/YB4Mip8iZOow5P/PpNVJZCBCwUnekebtCAfFSs
Xp5qBxKekOBAczGcQIMlKCVcJ7PIAHFL1lWhHfW6UVzGXDRzynQJuoFOKyD+QpEW
T2ZGqev9LcQO0hqq4U9A8QniopItYEqXgBjO/rOtWv4eYodS0oEKE8wcEn9GaauK
o9hYlTiYKI9JO2X53Gk/r5vqyZlWGVHaxy95Gp3LXQ1NMxyqqHpn7DQUzhxBOzGF
bRDryFVq2ozBZMvWKoXRy30sZ4CdHWe+EBCQ0BfZ0NbpR/VmSdckcp2qXsnnEp+S
uEQzVqqZbU+w9/FPBtEWJpE2nYTzMVfHZTapI3t4s2VkgpUOYcts0zl3logEqg4W
dXesLs6mYFN/0cKKeRvcZqGjw5sLvKOD201qznOJaUFb0BDmRbnht6j68s9UdEuh
1yoXbMY0MD0yoTb+tAz+eo1VD2aAABJqA5Ya4zSq29ywsPl8EdsxeipCYSQoJdpr
ONz5rCp9JJTyqRIycumH+5ecT9cYvMxCEyVo2jzH/Q8H18Mc7gy/H1qpRhvFesC2
EzU1jUc0zjKfmgStBA+cUi4OB+hv6Jo5kNoOAWfGxKi87zxIoRKKQWOeY029W7hr
Eh1uwlqf2B11WRcNkJGom/dvBLtZcGA+zgfEsYc7FqfoM5RRNvaiBJ2BU/TK40wp
eBPJEMVgVfu3NHlzSuOjOOPk2K/OqdYCqZTEspgo7ps2E71HXZ50boZwAtJRpePO
JoK2/GWTA9JfFtJSqmJHFJmMJNFNmUU8niZlqD90xgWhC2MA/biHnoakmysxhkPH
t++J1lrIhHjRMYERP95JA46qoTQtUhG/FOu0hRC6NVW+z4h2dpJgqDvUPifZWqaP
2ZhWDrfDWyCcxo45wWLPKf+Vliz5WisHTg3q6Gt1spT5EpOPKRNHoOzOIEf254pg
XfXoN1gujECrI3AZM7l5dgObFaz5i19HaqE3nC3/SJI7W/KbKEi1Rm51QFJ6W6Qe
DsFZyB1pxed94EhXT9d+5unEgwTSesnGIjXhyewdOwrbHZNS+dyPtIctqZOn2gu3
G8WbKkO1XabldEGUc2SJjBtpI1h0Sr6BA+I4bZ1FPUtFZH2Vc7Go85EW4yEab3AW
fwETIQ7AGjnSHsHNhyfTl4xe3HxgI+gqoAQ42w3lfojQixH5H9tY9oFfLs2WiYi5
rCofonS+Ca6jUCd/SZ1YGBNueET4Lw11hQvxFsJrpJYDPXgUJ61N4jw2nfZSUGui
guShIZxc3Yov8Lfa+Uty/Jkt2sR85o8tebybYiFcpskasMUDo8m4QSKcAhLwLvUT
6RRnpZoaw5WG6ZuQgmVttJpyeH7Db4asYA1W8dEm1rqE17FPqBpBNt9Qps+ZWG5n
oLhNgAKeTuBryVfoJLqga/g0vCsNAzB/KagKiaYRG2L2KGqlYUlqsSbTOBuMN7TR
fRQb43Q5ddmqxf4o09kjvQqF4gyEpvdvGdFXwlIuqAs3CyR1Z87fMoa/oKNEMoY/
8mjQVtRcdJU9cA1pVdEdLLg2jnOVZiiv2tDPoVuMueVBTi7E6FvGDq2vX9HRjZpB
njt0ydpKNKdBPA8laOQW8ma5LTF3dY330Yg0815xjJaK9UYTBKSiP6spjCIrhVWE
cYvofFI4zvSOtpRHqydLURkPuvnghumKYAK8KX0Rvuy81u62g85BsLsX3Gx1u63d
w1vBlb1uAH8I9rt7V7ut683gcI9+br/8sL17GOy3u9c7h4ft7eDyrZVGa39/p7PV
urzTDnZaN/FDgS/fau8fBjevtXeDPVz/ZuegHRwctvCNzm5ws9s57OxepRW39vZv
dTtXrx2uNK7t7Wy3u/S5x9OwP70Z7Le6h532AUJyo7PddqEyn+g6APC1ILnZOby2
d3RojrLS2LsCK94KXtbZ3W4G7Q6t2n75frd9cADgwEad6wB/G/7Y2d3aOdoGyJrB
ZVhid+8w2OnAQeGxwz1QjLi3PKzXR9Bgg+vt7tY1+LF1ubPTAfzhFyuvdA53YQ/C
ZYvPsXW00+qCeDzq7u8dtNEYQJzCKnAD3c7BywI4imD6e45aZiVANyxyvbW71cbN
HBSsNODi8MjBrb0jVEZw9p1tD0mIuHaw3b7S3jrs3Gg38UnY5+Doelsu4OAQVoVr
3NkJdttbAHKreys4aHdvdLYIF932fqvTRUxt7XW7uMze7skUKGPsNh/a4MYLk5ja
0eX7vgO0i8TXvoGkdbS7g0jrtr/nCLCCBBb49IVwtK5223QpDjWtNG524AR41Yam
AiapJr0Df7AkdQuocy+4vrfduYJXKCS3tbd7o33rgG9aIxDuxNJ76/Ie4vAygNIh
iAAGRChe8nbreutq+8ChI9wURH57t91t7TSDg/32Vgf/AQ8ALQO57DBWdw/guEgH
8AtZJWgBQeASSMx86SuNI2AjJNhdTWewO/7OBXfV7r5IxMHO3sEB3fZ267AVENDw
38ttfLzb3gVkEYu2traOusCu+AS+AvAcHAEDd3b5SvDIJCI63W2gZ+FRIvQrrc7O
UbdKqLj3HqAR1yR6da6DnzhYAw5DGgg6V2CzrWtyeYEnCm4F1+A+Lrfhsdb2jQ5x
sGwEYHYELXhAWkKQeTK1Sn/s5rkN/h4VfjXJUOzBQidY1XdHvRN5ctb0n+ELiccA
tn/FDg7ionP7XV622ZIMh59wjxgPd5eyf1ENJXUtcr08xofxc3ocfJ7ijCiOp7A9
LkuFMwmB4Cy4fpJxgzf2j92hb+ZgNjDFGdtZgnMzaH4920rolsTHceKAXxONcg1I
WyLtdePZ7hwfGXaugaS5F8oicUO4l2leOyQbDN9dpoTaDwbXfGR4lM3KjC/+Gn+P
sEW44kLDQ917cQs18i4Y3gJGYb8+bBJ+8vk4coZmZsaqKY+ZUZi8lGSWnGpI3cg4
GAQLm2L9TTWvdbwpSayi5NlpWGA6okyHqXGWfGZc4uwl56v2YsPR17QxHs1fGjJB
XQ6kyFfUTTpQRxMq3wSlYsYmthSEEoO1VrhuXjR+jS5h7VBmoAgHdEYE3bw+1k+D
McjdSlSt5jSm8Ce+Cu+j1/QVQUmaS8aficgfAk9L0RryFXDyIvRwSnbwTIVD5SvN
CccP8Eu8k4y8XQ7U6Nldg6mZhE1fs0dr28FVri7xBO3yAqKcVtOzKR3knCqIAmWj
Xh6rAea/QjMyTXIXG/cNywt6XJo2G1e31mgDHK+Jf+ftaL1M99/ye/zmoXzMXZcB
eURyXkftfNKIS+0e66wRd/HV540dh+B/LnoEYeG5Vbr9brnj0tT+20JoxtbdSDvg
qt9Fvrbo0m0sQ4M9rfmY4ggTQroxjgIx7MnApctkb/TRtXWJ2ktbmBdM4zxO4aHF
dGzaij7phazaiYDkZWZiYK3EAyWOMC5xUnhCp504eFCY82MSyeUHU+3vl34uX1qP
x3SGKFqMsjsMjIGFMYooclSWk/OnT89ms41hOt3I8uFpXRp0GulzAzsskWNMi2VB
LpU3OAnnE7FopvxFn5O/+H0TjLfmWYoT7/DrUuEEa6DgxDVu78T10KUnIXFjWU0t
QvX3ukJEVV5SoBhZnb7TTuelIQA4X7ukSbU8ItqdhY5Ds87XSgQBg3n2m+HnBTLm
+fh0Ga3LB3s7R4ftnVuuE8djt4QUgnIO9P2aAhE6O7XhrFeVClZxkR5RCW7EUWRP
SPASLBPMrAQThrngbtg/5YJSvXsM3o3mE4wAUw45MB/01eASRGYpIeRCD1xyZiFU
Zm4viUUHwd6AzCRT/2BFs957pTGmC8IBOjocoOeYXT3q2Cn08q0fAmlKQRutX8Cs
A2rqZXdqqiQdhqRSa6wyJkgUSI5sjtUxkoKwX87Rn7ZV+RpVGmKcAGUTf0+UsqA4
wM2dcajJzJqn7kQ2hz0o42E/NYXBLl2a4TMk8oH3FWe2MPEXEqswgmR/h+QBjokv
L4DdNcrVAOyuEyTDPZdO+iuaaiQ7KhOhThCIJizHdZzuGEUQZO7Vy0xFpiSgU+69
IYvPaXJyVEiI5Y55hglyJZ+4nOv2W57HTl3gKC4Ij6xKqBCOYcHEBY+KsFtKzrGU
MLL+6Jssr8OOzLQzXQ0zk7qWKGsGupoKcbKD1Yz50qAs1rGpMKpJ5+HUMOqcwyp8
6rLE0q6nv8fJeBSDV5FNRvPTs9F8HRC/ngwnycaoHCe1dzs54YXKdV88jZ+Gpn/g
X+Efjz/z73zLXXc1nvGcZz8zfPwnP/uMxnPvfvsHPnjz5ns2n3ziNd/+l3edf+e/
fGzw1vd+6M799z5v6y371993d//Fbx6+4ic+/eonXvPMwfrOD714a/WdvWf+9hPf
9anv/t6tl7zrj7/jY4PW+772uXf9ysq7/9WZ9z07f84nfupj3/ahK//1N8685clX
nPva8079t//y4V9+0anv+NVv+en3P/l4c//3ej/8uU985llveM6FN3zt9R/91rf+
bveJu598PfDX2dfuv+6T395/4W/8zgde9ZV3/vkTn7r9h8/7x93nf+XtP/Pav/rU
K1/Z+5EX/v1/9vGN7/v8m4c/+mMfvvnVr//FX/3i9977oce/7eef9z9++YX//T9f
Se569d8M3vvrb3vNl7/8F//k3mcdv+fMjRc1v3L8z3/r/g//4ONf/OTv/Nrnfm3+
jhvhL24/PPzAx554wRe+70vf/YwPvHr/q6971fve9UOP/tSftPZf98p/Mftie/3/
/tuN3id+4Y1P/sfnPPtXf/1T7/jQn37in/7eR4J/fee92Y/96cqVy1+efGj1+bNT
/674y+/8wXZr5+e2PvPh7Me/8tzfPPXo+Wffev63/uHBx3d//w96n/kbb9n40l/7
g+zj7/7fL/3cz7/hlR991vk3f/axF/3sS376nh/9/PuTdz926/Ib3/SK39w788WL
d//Zxz//+IV/tP4fipf8rS90jl74Ez93730/svFLT33k33/mDafGv/XZp95291c/
3Th63Qv//NF7P/MD3/2lD87f/vrHXvzHX/8HN1/wfw4/PXimWn3b3770b353EHzi
BaOf/e0f+JUff+Mv3LsTv3bwkU/9vc9+/HU//Nw3DcN3/P7f/frD1778nd/1R3/9
gVf+r7PDc5//ZPnRt+bzN92++/CX/uyJP3rVzT95R+/9py6+5v8BdGMRYg==
=hE7I
-----END PGP MESSAGE-----
# This is the help for users of EasyGPG.
-----BEGIN PGP MESSAGE-----

owJ4nLVce3BcV3nHBBgwUFoCJm2huXGA2rBaWQ9LtmMrkSXZXj1sWQ/LMkwmd+89
u3ul+9I9d7VaDTi0lJR0oJNCoU2nw/sxQIAOFFJIgaEECkmgBAoDLVBnyMAkqYEJ
LWlnQun3fedxz727ks1M+CeO7p57zne+5+977N727Ct27Nzxvs98bmnpzr6Hdtx7
Md3R41zxwH2Hrxk/NbawPDthNdLAt2YXj05XxqzdPb29SwNjvb3jC+PWiYWZaWuw
vK/PWkjskHupF4W239s7cXL3yLN3Hsb36F9mu/hvwFIbNkvjHrbW9NaP7B6LwpSF
ac9CO2a7LUf8dWR3yjbSXnz5Ostp2Aln6ZHFhWM9B2jT1Et9NjJh8/bx2ePWCebH
h3vFM/iwVx1Vjdw2Hd1XWAoP4HFMi+m/jcTqpX/7R5Yadmp53JJvXA+r+8VqtUdg
rzJueanF4IGVRpYbWSx0knaMVy+rLQf0Vi+99kB//77rsjX0oO863HtA7q0/wxfg
BC+s0xGcAzc82/fblhfEEede1WdWLUosO2xHIbOqzdRKG8zykG8uc62EOV7swVtI
WgKssGwrgH3sOitbS17aUFcrWe2oaTl2aFWZxZsJg32A4CiEs7bYERfTlvi53LQE
lIgH+XWdm9JxUdN3rYa9zqxW4qVwAlxT8SweGbXiKG76dmK1bOKtZJrFAtvz1ZGc
jmyxqgXqxuAVnnLkG6xvcmbB3cr4H3wEf7tW0HQalg/vIjF4oMUbRAfQWGWOjS/x
KICdWBQjez3YHlaB3rly07KVFz/+H8jfY4laoO+Ql6WLQmoh21dZm5fRSFxpI367
RHzjNhwNn8KdJcEo30xd6LYu0xpmLTRgYwcEkTKUbpxEoBTBIetE1CLWI6M5EM9o
e9yZMwdk4bevt0ZrKdAMh5csr0YrSRg2SCYImqHn2HQk2FwYMp+kh+raN3wdl7sA
ZVGz3sB7p2jxgZeqc0pWq9G2wii1Vpo8JWGQ+OmGessINgpzOnS9Zh5dTV4IKOMg
H1ARHvnrcFViY8ukyIGLwAfSwuJm1fccurBhkNLahO53XVIi2VjIMMVVL7Ri20tA
YKdCwcKoRhTjU61YmYJmVhDBf3IrpORQbGInYol8j1uHeZpEYX0EBCevlR7ulc9Q
QTu2WTIZEEQJK0lzBa0JhPRC5iBjEzIh4JEDJgWGEsmr1JIoyNOaNpKoBYqGVmeT
hijNUfeVjCnnJRUwUABxIeVM6h6oE/yRwDEeylmduge3zElKSQY+3Uv8W2UsNggr
st3cAXQxYWl+B0s8RBaxMEdDyw5TrjUPSbUz/8UbLBMHKDOpGb2daYuQL75IRJA/
i1nCgdmtRkSais+dKEkYj6PQRQcuyBHESfHl988WlK1KjXwQcov5YDh1BtSgWcbE
hA56cKNIqwxaNFh/Q/ijoo/mmTNH1wJ7gT9bBx4Ra7NrtJIoZeaLuJkUrD5J3cRY
lmlFJczCi5IbR/rJwmgrUiRhbDnrVYYg6LF9rX5c24OyZRVjB0dmJJ3zXj0ElkNE
HRSEzEBQyK6vzLplxD8igcN7oNXCpXKxSd5tdwjqqIwYenMyJ9IpvWHXV0tFdqOY
SG8Caw9FNKlJW0p9L1kYZ5cRVU3pC9fT7WCDQ7Cx7fOIdm9J+8kUIih3QqXjLE2R
XfOpnQBzFVICTFMJeQqyFQJREGedgT9SvK9Fvgu62vJ8XwagFCK0DTJLW5H8lEuf
mjAMyT6Tzlh4BM5yq6LcB7jY2DuE8MPBxMkoSTIAQdsxXnJPz17cB2wHhFYVwAeX
K7+Tp7dsHcPIvGEHABNUAE2Kl/J0XDrsRC4bQZxYj4EV9BfBLmlM+vJEcv4tuWlP
/m26rK1O6vrCqIMxgBdezEx02+2B9m4B1lZGOs74ahrFRK9yvsAS+em03Qwd8EE6
5i7LoBBEcEuPIClyVe2yR+BYULdEej1P+u0GwjFxy71lazwCE2A9DpiBQsYk3SiW
mi7vrPhC1pH5YvL8ADPRx2JekQELsDOPuSUw944DtEFoZyoW0w74BikTHMTWhczt
GHBLnHjwBFQjdPwm6ZsCbvC/OfiKcD6hxwIYjid2vY5/EtqDnWL8Q6gysEkpO+RH
kb4/ZB6XQYECOmG9ZFBT6koFOgpxpteVs+jiiCumIsj31FLivu2gf+qkn+imHUB4
iUAlchXSo5YRA5grrmv6n3ikqAxCn+dYtemBE5R6rc3FB0jDQYb0Kc9pn3FPIDhv
xCb/wARMVpRJq2UG4SJ1ZDQsJDGg14SHGMrhA3hb6CJGjG6nIyTBv1pRgpcpW6OK
4Boob4PeOL5YEf4hsENQH/SaoQvBXgA1PFBeL+ORsjsZbBElitwH/WTRp+HiwIbw
VDOdGzkmXK5iUfYarLIhoxI5me1A9AklALZDgdzMBEZrsSlf4HqUuAKtAANEqhLo
PKVsFW4QslaBbi5yDPDZtIQERcuUpgo8TjbezvRE+rsyb5gedVsd8qQOGrt3uqRt
dyClV8BHapNkVAdnUJ4hEykIuUI8Fe+Q8QQTT4j8JJgMHEgg0k1kQL+SDPCryYVe
Mkn0tAd8RBLoNdxJhyqxYAyfK7qNA/Uyk4W2azpKxQFlO1mWLElDrMUkaAIyMbs2
aytxM7NMzIStmm+DWbgJZhl4LCIxWK9SmoRJLAcKDWjYj4mfMcIYJ5VwCt61nbYw
ClarwQewGepchco1mL66Xq3mQdZEEUvkVYWzZS4agQ64Hl8103V8pZkQt7sd3AVJ
LZJEtNIUK04kc1kI6VAY7dPFRyXp0g2NkM8pjhkJgXqOe4kYh6FRYacU/JvcBGzZ
QJJU74KgncEt8nOwEdmjC2FMhzBSmO4hTN5NAw3TLRpJpgy/hdCbmd72+4A39cUd
ZDQz9hFGCQm24BeiAPJgvlFmUkskh830l4AgQlrh5Qm8EGK2ReUNQ3Ou2Ebc0YCl
ANaU09oDMbFhxxzNtIhJisFjryA4xw3C9zIZzyiRdT8qcUlcK8/NaoB5uXWXWfai
WQZACScI3EW4o6uLYhyksFieKuZzWQQp+CkCaJRI+Sj1tgLHHTtkZEtlBw/gGVol
WJ95uIpYpV1YR4aZlUQgpoVU3YPPEkpgTAdmiexZYFp0VSLk6JsVzp3JlnScmd+4
bJ2w15F/Qk3CLkRuyTahpglzm45I3u0AIzKJ2guEWvKYSm5tApWxzTkADhfMeZ6J
V1TyPaZiqfBYnUSryhRngiFVBrlTDnhkuQ2FMaPoYqay+JwiEqTdpFntgjwN5662
0HYpqxLac4lEG4KInQJ8EAgtI4PQpqJFHlCIZMgrQY3WQVReWZDRzgXplp4EgwK+
JVK3bZ3fpR1eCfUqUxsKomSCRSWRtJECqAodlp5+Pb+Ip4naLLussJ2JVxXWdDgy
ymjKLmy3q7DNqhKZCSo5KCrQQRTLqp0GeR7mbbZiuXEO6kYzjbDU6ejGSEKlOxn7
PYG0qT4PUsGiZk1cWdSiDFiky+SeSS8vZstb8ceNGN8qBcucgfGiABmSYFF1lkrM
tXqXMLCEoghDkIayrgDQiIfdiVwWaQY3qrLrQ2iP/MZGxicPr4GjcNX7OsqJ+AiM
0mG/A7ZtC0mEL5Yf6/pEamufZydOA0CUyBu5uB9Ip9b0UZtlXpGm4OsCFqY8Q77d
rSy4tIVdnoEZdXbhf8LYBlvK55/aBiiY2xlsUqUlpeUZA6pt7a9k/VWYi8IXNjb1
kGUu9VI8nm+ucVuV1LGHIh2glnh2TOZKIhl1wMbM3FwB+omi9RL0ZtlhXqqxPdvw
uKidywRGiNv3Vgn5aRlKLcgkiTflIqjJHJEsUBTj8CQdhkoo9RbzSfoiTQF/j63k
9VxfI486xFXmjfqZxisWFVFB4ZSmmUwiOFuQe0dPV971MnS4WIpFLJV1fIQ3Mjii
k9UqaFYzLjRPuF2DTETIgNIdJwogBUKGQCgSdTLE5ZGw2EoIH4VM9pZEUyKxHXKM
2u2hS0ZbWhXZdiq6QkrNOfZGcYlkVIvKGslF7M4nrKMUXQQE4mS9P72dtQrIBxn6
7TKgN/BCEXoq4wKNejzDOFX09gBWnFRB3kwEkkSj2SCioSBZKGRE4Uc4XZok4CJI
Z0ZKtqHNS/kz8NrrnouKCTgCCyCqAS0LPGVrsnuAzzYuQmQhfSzscOkZ8nUkIfkq
q4P7pc9lEbXDODvrrNjtMNr6aoHkR55IW1e0UBckD+RkAqa6Bm9K+KAhiuRUmo7B
wxkDDVMCEvOsqL+MSiGRsjB+XVRrNTxHtkB0d5UqPW7ZWgypG+9ldeYUaCRy0qhk
aUwrKkOeiFbiDBmr9jR5UwT/cN2joYi90kTL9bCZ1bNVSb6zrJx19GWNEJuPXBOs
T+tebJdlpFXNEVUqyvxy5oq2qDxtNY6hyzCibuNunZyo87a+edkCoVmZeIQEZM1S
aDjlMdoOABugtVJ/UCW3CfOpe2YkWB7vclddepHOlOYusimJwKtTwdeoRmn3RtU6
Mb6hESDeOld0MVTxknmK0tDCvIWc8KDEMhajM+B1YDPfygKTBCCum+D8jbqp1+Uc
TBco8V0VLEUEaNWjCDCVy2w1YSFAZhETu+BfnRRUWONEwRNxSNZwVqmZG3UZEGjG
fmS7MkkitAwhFrGo6KyoLX3boa4eVrxQrlQktEM16aITC/B2cseOfnMAmYa77sE+
0nHI6hwt1AyUCXKS5yFyEE+ZwaJl6EDMsogWj1OEKhTG4pGjbUmGljCNx+ieanZc
h7jEUSXdj00Z9SGza0u1Bb/su1nwJZQjxorko44rSBiqOyDoeUgFmyGWWpgiRDdP
deioCH0DcTMP51go5tJiI5aJTQ0hY2rtMUJAujMtctUwkzPXrWsIWrIUTlGMCig9
ge0yiIw8LcmuTNjJAzXUVcJIKe0Dm8SrYdSiUYGMRklaoCbUbPAXnqw0IBUkVDVR
VdEVKFXUdXxmi+ZcqVAcE+zKrk78MhWsQ8bd7d7aY8SUKBZ2vzezpW3PyLRKZF3M
w3874Ias+wTAVfy/TCtFGSyrUZmHAasTo4IvQ47LfEBEqtqhGgQ55J5k1Ycw0pNR
dCi9LcXQwQg12ZWRQF4I2MBSw48iepsbnbHGPb6aOc0KOC9yly2mGl2iuyUCMmiR
CAxkuKmEqnGjzQl4BpDGNIMS2IXGbnLchEwjSxTE9WV8I6PAV+2SzEblh6JKjW7C
TgScw3ysRHxUKKBK1hUBiWaNtpgQ4kWpWE9uWpCZw0L6CpHCL0SVoFgNNUoMQ2an
dxRjGzJJCOKaRgSEt3ibp0weJWCGhkmwg1FskG59Fkch8EoBC6rMqLrRbA8101Mp
ODq9ympRIgt9DVBnF4e5RMIoA6zp4eGmRvqpBhByIEfizMQOcH+FL5XZFeYdTA4m
pDiwn6JNtxeVwVCS0sQSHwu0t6YQKhF/W9CFu1W7q1ZnOULD9xrSp4vb0s8RbCr2
d3WSV5IsFV4C0zIIPjn+lhQ82aYZCVkLEJAbZpW1BiUk8bLSC+WpzXMy/yDdea6i
J2K8chIhuMQ4bRs0iryfvBNJSR9Mc3FRCA6eWl6ix1rqaAOPZm9sOaUDLkgvUmmM
8vdUdBbuqIsdZLYnGKUQ4HZLFY+k1zPKUKM+Jr62aNxt1cToqApQhratVDIG5+xL
I7TChrKw54VyvkSXhgqEhJdRSLg0daWOvDafH2+5c0cbyyhuKfJzeriVCgtDUduz
xFZDDTiknhgME0VNXMzJviE+BTaEA4S7OmxhXqdug2N1IMKE6iRAjjJDHWrMjhaN
iRo+TwGtrcgWpRZFNv7FiyYukpLLYKaavobFmBQlxFcc3ZFczQ2uXErXAMyo2Qdq
KLDE8AULUVdnJIA+KSKOAeEkkxAjroskW0pdKhWCC0atYjsula1TaJ4tj6NfJI8n
31fK1DEQ3NHyKo7Fhbl7FB3hNk417yMrYjbL8WlUFnG70TxXr5atMfhcJK/gPrA1
xlXHRGbDiLiawP9AezKsFIXZlipSVClaCWMnFGaMbuRLkh2+EVEOFVnWAb5SFYwg
kJjAWIBnQOgxggI05g0soocKrcu3sRzoikgoPHluEiEi1s7CpYFuRpM4lKKIFioV
BQXOEGGM5JezR49joQe/c4G0EQEYzjWSEDU+yvPhcZ2FYPgY5NOkyXTjTEEGH0hS
eAFZwc2kPcfuDG7mnyugFWd1TlnJ1wF9NI59+U0FLJWHzZLkQs5QVE/d6zbKKSoY
MmbvNZE5j7KpLupTJZCG4ix9MeDl2q8C/MvGHMhOFpbFNK95v3wjgVB1YYWeJm8h
6LM8B6sBlzEQlu3chaFyZgi9lVDvpscbCnsXrygkLgoiIOCCmcjkhebqi8O0pV9n
mnarcbUiP3Il8hwmMr4hZOe6i1gkb4jmA3WPfZzCkYOW5gZywKlwrt5TDUWprQVB
WW8kpNqVuGNNlT5oYMu8gOziCCKkEzKaWkXUUigpCxyfI6zH7slTRJ1tGlhkXUFC
tqXC8LoCTi01m2v+kki3ahNphugGUSbEaW+ViXhRmILYirsFMJXjbVYMLbxs6/tS
k6RjUAX7h5du2pSMRZdQl62k1A1bktPoEL+ppQiGunI8Y9c2HS0xJ2cSb/S4turZ
qwugP4V/JE1beVKaPOo6iuylJmwYLyJc5buwciKrKjSVIxv/YbNDYboTlnf5TzA5
+c0vk6It4spvhLICOpJoU74iZt4xoIty5TaBrRvFRahWoKwYhCR9st6gDV7j+uIL
ukuh6MnPtpWyBV3Ct/5Mh/pc48OsNGx55T3y+7Ht3OX3GgBkMXZtEy0a304x5hqa
uIraTsyvZTRkzdZJ9eVG6d8azFkVrVYyfizLkibXsnNy7aHuw395KKmI4KplA+8O
ikI+P6S0ZQHOPBXiWfMsodr8jBc28YuaWBsuqWVYccIvyu6Bvyv9s3vpA6SoypJ6
OUrkdwEi3QXGZxIDGPfRaSRgQizOM0NA21OCfgwOVoTQN2fUl5mNTEN/r6HLIXly
o8QkVRgCqEZNIOpKTUrGaSa+8c0WT3wtSWZqSDI8TJoh5Ql7jMOlYeGKo0nU4sxc
CezDD9Ra/f1MSXAnuQVq83I2GWzcWX4bTX2TLGh3KfGi80iwxY4RuhrVm1xpDZFQ
ZaJDKO/cZey4WB9dYlVrHuSD0PyR/oxGqp+RBqGSGy1A7BAcBvgH4PTIbvxSPz/U
22tctddlKYqkV/Y+d49cxqLDvbbhkis0UOYJGJWqrpENHKjnyQHxd5BiSqG37p3d
XD3OnV75Rm9Gzfbr8gQtGO7H0GmkKnc+bCsv1F/2+mN5WvEp7U2WVsWK70C/0W/q
2K/a2kja+21/c2O/16gn7kDdtxuDLEpdt+pvNmx/HWB4HIetwU3HXtlci6LELlcH
9PnAPDwv+64yDsiDbvvYv7wU/dfDfyRt4rUjcf+ZqQNuT7Q6d/Tc9ARfX5qaPZa4
a8vN0daJo44969qVRb+xsrLqrLbcen3TDlYjVtmYOL7WN33MWw5m90PWeTJkiydO
T861T3jDPcnBtj21UYvWhgZm4373FB9Nj80eHLftYb50Zr5RXe1ZnDw9PeHys8cG
FsIz7aHK+njt+IGVyTW/dao2PJ3O7Rs8EwTtwaD/RD2J1kfn16ZnNqdOnz3jHz97
cnkzOsl8d9/5VX9gZu58f19fMj08X1+dOT1+xmHLs87JZv/cwlg8OWlPnNt0xoLR
806tLxyan1wZbEwsVJZb8xPDQ+PTx0+cG51ZO5NOLW4c6+GbU8fGz52bT+f8vpnJ
6YW+nsp6dXGxWeN91bP9Y+l6rba4tnywMdNeGRvsOd/w+mfZ4prTPzo1NblYPz3T
1xOtByc31tb7z06cPF4dXmo3p5snJ4c2wqmejfmj4WajHi8OzbHNOpvcdyapD01v
7m8eW58dWBsNllv7VsaGTs6eXuUHzy4eODd5zDlTHx3y58NTK7XV2Xjf8am5pbPx
Wtx3Llw/PnzyII9bRwd485Tff7Ax1zM5NrG5MeCsnR8cP7B/brOSjB4EgZ2bWK1O
punZZv/C0il2oM5mp5OhE40kTU5XlkYX7PPLM15lpRYv20dPj06Mjjqjo6NHjmTa
1d1SOuOD/vmGLoo33IBkZqjp1NdbVWdlJWKDg35jc3OjvRKn1QHXHko3/YGBiA2z
Tb9/rd7yk9UaG2q75QiPMFRdfacD0vxQTEXJUqe02sslth7FWOPAr3vk6BXPn2CK
5ayVPJNHtbQlvi0dUjUIgQdQWGzhwMXWPQgmWeGi6N6N2UeZXnRgm61yCJM7IrD7
XjXBioHJjQA/eWKZoRD5E321kjnJX6UYr7+aTxeUcb9klO4uM7zQ6z2zSZRGTuR3
GoYRgPEHO0BuY/qnN2Q7eYZlXwLStZHiooCaY+OezWP0853khSxx48gvO43e1K4b
QXhBkSQ7huq3UNSYHn4F3H4Ic40aIQ/6fRW7GmXfxeqcCXCtxKY6YAMrifor8QaF
JUvNLyYIzCF5INCj2qqiO003U7MaVHrVv1MixmyoA9/kadb0UT8Nk0MA+CyNDq34
bnAw2T98Q9pMseNkl4GNu0e6Pt5CRkVVQzlA/OzxvVD8Rk5SA0yeyWtB8k8O++WW
i4kELO+2YyYnhsQIMU5F4sxmoZAnB7uU6Mq8YfX0NOhHjOgTY3ykEaktySeon+Gg
QSQlvYbQN6RajGJpLKcLTdg5jpNohTkvTs2OivifXvWbSr3yd51uffILn7Jjx84n
Pe2pT8bfi3rSzmf8tvoVqeR/n/PLq/548KF3nvjI59/wnbd8IV18/MLvpj+//fZf
vG4Ht945eO9Pztx9x203Lu/64gPXXnz50sNXBo998ju7Hx6486PffsUVp+9//NTE
+951232e/Z67//DR+ZuH06/seP5Nr97x/We99UW7b1268OX3Hb3pyqs+WTt835m7
z7t3fe+u/37J4qf/6rlnXvtbP77zta95W+nvP/+yq77//jfd8+DUx888+cP/9/v1
O371quPjj49t9O+fesOnN06/+e/+fe8PP3HPjV+7I7rnJ3/yiSs/eO97X3fxU7ff
9uieF3/74bcOP/fP0ukvfPX+533kpqdddfML0mb5jx759ou/3qx+7DkX3jH5ovkf
PP6tDwxatS9OPfKCZzZe/s0//bddjy5dfcOpvrf+w9984rtf+uHdLwzuGP7r6bf/
uHnuw09/952VQ9Wf3n5h9rWjb3v34vPveEbPtW9ffezBn77jc+f/+TUPveTU2MfX
N5J7/2XuuU/5yG7r0MXX3LTyT9+463mPfWjkOz+3f3nr9/5g7NNXH+rp/72nvbzy
lI9eCB7Yee0H+u+/+PadJ27+2blXhAfee+s1n/neLeXNv7jzrp++bLHxprvSW771
n/9x5AePXizveuM3f3HP6Vff/NTHRirv/9iuK6/52K4nveXDr3zRgzdc1/vw4Mc/
9I/ejc8Zihq7vvsub/nrN9745r/8ysrXd177rF0/uvtLX26PvfG2C6+6cMV/Xf3O
bxw59dIbrh29+ur5mz47/KwHHrvwqnrwo6/9zysfHnzsqT/76us/9TtzH/zGVdf8
69/++dnX/yp65v2fve+Wxa+e/9xbnv7Iex4c/H9r0MbN
=Brvv
-----END PGP MESSAGE-----
# This is easygpg.png.
-----BEGIN PGP MESSAGE-----

owEBTQay+ZANAwACAaa9v1dXtzHpAa0EHGILZWFzeWdwZy5wbmdejoAliVBORw0K
GgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAACXBIWXMAAA7EAAAOxAGVKw4b
AAADvUlEQVRoge2aQUwiVxjH/8OMgM8M0XEjRLawNSUSL8TMwd2EWFcPjva8R9Ob
Sa97lGTTHvTSQw+ce2jirQ1Hwx7UXS9dMHjQqFEm6GATMIaZMAYME0h7WGnNKg4P
tCMJv4SEDHxv/n8+8r3vvTfAbd4B+PuJvn78Uix34/0zAG8kSZoKBAJ3+LKedDr9
bTwe/wHAHwAugP8MPOvv738pCMLbiYmJbxYWFiwTeR+rq6tTx8fHXxWLxb8KhcIn
ABfs9WffDw8P/7S0tPQ8HA5zhBArdTbE6/UiGAySZDL5StM0HcA2C+CdJEnfzc/P
i+FwmBscHLRaZ0MIIeB5niOECL29vX2yLPtZAB/m5ua+XlxcxFP95W9CCEEoFIIs
yy8SicRrm9WC2qVrwGo63gBn/pXbnJ+fI5PJ4ODgAKVSqS0BfX19GBsbw8jICNxu
N3U8lYFarYZyuYzDw0Osr68jHo9D0zTqm95kYGAAkiRhenoahBAQQsCyrHngNVQG
yuUyUqkU1tbWsLm5CV3XqQV/ia7riMfjqFQqYBgGoiiC5/mm46kMXF5eYmNjAzs7
O6hUKhgfH2859cDnv+LJyQn29/eRSqXgdDoRCAQez0CpVEIymcTp6SncbjdmZmYw
OTmJ0dFRavEAcHR0hK2tLZydnUFRFPT09IC2D2u5ChFCIIoi/H5/q0PA7/dDFMW2
OoCWDbAsC57n4XQ6W7650+kEz/Ow2Vqv5h0/D3QNWI1pFTIMA7lcDoZhIJvNwjCM
f69ns9m2BTQa0263w+PxwOFw3BtvaiCXy2FlZQWKosAwDOTzeQBAPp/H8vIy7HZ7
WwYajenz+RCJREyrXFMZUBQFsizfuv4QGbhvzHpm7oNqImNZFi6XCxzXUg9oSrVa
ha7rqNVqTcdQKXG5XJidnYUgCNTimkHTNOoGkcoAx3EQBAFDQ0PU4pqBYRjq7HZ8
Ge0asJquAavpGrCargGroZr2qtUqNE0DwzCPIkZVVVSrVaoYKgP1PZzHbuZooN6Z
a3cn7qExNVBfXPzf+Hy+phZLpgY8Hg8ikUhTi4uHpL6kNMPUgMPhaGvz6rHp+DLa
8QZYAIwgCFBV9YXX633yJ5WqqiIWi2F3d/ejLMu/sQA+yLJMstlsMBgMkutzWKt1
3omqqtjb26tEo1Fle3v7VwA/149CFJvNtp9MJl8RQoRQKGSlzobEYjFEo1GlWCy+
vbq6eg+gXK9CF4VC4VOhUPglkUi80TRtykKdDUmn0x8zmczvAP7E9cMed9FRj9v8
AylftT3C984HAAAAAElFTkSuQmCCiQIcBAABAgAGBQJejoAlAAoJEKa9v1dXtzHp
EEgP/iN0tATQlSlajhO64cowQVqk1EhVt0Hq+qfAPzZs9m+NDxQ5HczZSyKggnYt
xlP2+gHlNvJhaSZHyqBnQTOQbsbS1jRaVv7Jgb6eu+qqQqIByVlfEfN2VQKCPCw6
Va6KFa+U1rusAGBq44pA4dblRJdosF01bDQubyb2QOnWLoX+zcCHIgM9vZZ4QPnU
6Xj+YhHS0nOaoMrJwRVGzi4Xwc7x5HWavlSVkNUE8YnF9HVO2KLhEeaTlLJdVvYV
HkLB8awpSFORM89szXZIJuFoT6DL0s35ad/2jxuykQalX8vn1z3MnTQZJe915u/A
/YtntB1jd0/uj7H4ojlduYELRToy58g/oWyFHjPrQFkYmB4xS5ZfvgciYBjzG4Aj
tL48fsneBGlw+7PK732cBJ0EChLL0luJQYOVTWMuBXLPVV5iQsj7mstNG7HFLVr3
9SDYsGQjrDno4hjYSSYNLms703aGkIj4+pqjiYRxvPFiJPhExKjXdgBZ5HCUdNgf
1AnPLbCHh81YOTysbthdVIqwe3IlrIKY5WarIpg3mHvE2VmT/24ChDwG37JnENka
hxiZLm+OX89pTc3/b8BsRbkH7ZtAvKl0wx+T8rBjdq+7b8d1b0qxFlsOJuefgWH0
LFZ6fYVPnlZBjut6QxzefOJf72ydXwgPUIqf7kuFb9LBrg27
=aHP9
-----END PGP MESSAGE-----
