# EasyGPG

EasyGPG is an easy-to-use GUI for GPG that uses Zenity and XClip.

It is a single shell script, securely distributed as a signed (but not encrypted) PGP message in text form that contains a `tar` archive.

If you are using the Tor Browser, you may want to use [EasyGPG's Tor Onion Service website](http://7hinc6ucgvwbcjjoe44lhzzxyjptb3da6tzl33oe7ezl2qgwlrkfe6yd.onion/) instead of this page.

If there is a Tor SOCKS5 proxy listening at `localhost` on port 9050 or 9150, and `curl` is installed, EasyGPG and its installer will use that SOCKS5 proxy to install or update from EasyGPG's Tor Onion Minus server. If Tor is not running, but I2P is, EasyGPG's I2P site will be used. Otherwise, EasyGPG and its installer will fall back on the EasyGPG items on Codeberg or the Internet Archive.

These 4 alternatives are  
minus://7hinc6ucgvwbcjjoe44lhzzxyjptb3da6tzl33oe7ezl2qgwlrkfe6yd.onion/  
http://easygpg2.i2p/  
https://archive.org/download/easygpg  
and  
https://codeberg.org/giXzkGsc/EasyGPG

(Minus is a simple protocol similar to Gopher, but even simpler. See https://nerdpol.ch/tags/minus-protocol)

To install EasyGPG, download https://codeberg.org/giXzkGsc/EasyGPG/raw/branch/main/EasyGPG-Installer.tar.gz and unpack it. Then open the resulting folder and double-click `Install EasyGPG`. The installer will download the latest version of EasyGPG, and then prompt for a location to which to save your new EasyGPG folder. By default, the new EasyGPG folder will be named `easygpg`, but you can use any name you like. When installation is complete, some windows will pop up. Read these, and you are on your way to using EasyGPG.

`EasyGPG-Docs.tar.gz` contains more documentation for EasyGPG. Unpack it to find out more about installing and using EasyGPG.

`versions.tar.xz` is a collection of past versions of EasyGPG and its installer.

If, for any reason, updating or installing from Codeberg fails, EasyGPG will fall back to the Internet Archive. To update, it needs only a tiny text file called `version.txt` and a PGP message in text form called `easygpg.asc`.

Using a signed `tar` archive in text form has advantages. It is signed, so verification can always occur, and it is text, so it can be saved with a web browser using "Save page as," stored on pastebins, and backed up to the Wayback Machine and similar sites.

For as long as Codeberg and the Internet Archive survive, I will prefer them to any alternative (other than Tor and I2P). The Archive's and Codeberg's values are my values.

By the way, in the spirit of freedom, the place for release notes, questions, issues, etc. about EasyGPG is https://nerdpol.ch/tags/easygpg By using Diaspora, we can share ideas with a worldwide community interested in free software, privacy, and freedom. By using the Internet Archive, Codeberg, and Diaspora we can also avoid large, profit-making businesses.
